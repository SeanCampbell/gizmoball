package observer;

import model.IShape;
import model.Pair;

/**
 * A declaration of the functionality of a message which is to be
 * transmitted from an Observable to an Observer.
 */
public interface IMessage {
	/**
	 * The total number of Pairs present in the message
	 * @return the total number of pairs
	 */
	public int size();
	/**
	 * A Pair matching a String, usually a reference name, to an
	 * instance of {@link IShape}.
	 * @param index the Pair entry to take from the message, denoted via a List
	 * @return the Pair matching name to IShape
	 */
	public Pair<String, IShape> get(int index);
}
