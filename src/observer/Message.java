package observer;

import java.util.List;

import model.IShape;
import model.Pair;

public final class Message implements IMessage {

	private final List<Pair<String, IShape>> gizmos;
	
	public Message(List<Pair<String, IShape>> gizmos) {
		this.gizmos = gizmos;
	}
	
	@Override
	public Pair<String, IShape> get(int i) {
		return gizmos.get(i);
	}

	@Override
	public int size() {
		return gizmos.size();
	}
}
