package observer;
/**
 * Interface illustrating usage of Observer Pattern.
 * Designed to avoid any casting from occurring through using
 * the default provided implementation.  Instead, the contents
 * of the messages are known through {@link IMessage}.
 */
public interface IObservable {
	/**
	 * Add an observer to this
	 * @param obs the observer to add
	 */
	void addObserver(IObserver obs);
	/**
	 * Remove an observer from this
	 * @param obs the observer to remove
	 */
	void removeObserver(IObserver obs);
	/**
	 * Notify all observers that an update has occurred in this
	 * @param msg notify observer with a {@link IMessage}
	 */
	void notifyObservers(IMessage msg);
}
