package shapes;

import org.jbox2d.common.Vec2;

public final class LogicalPoint {
	private static double rotations[][] = new double[][]{{1,0},{0,1},{-1,0},{0,-1}};
	private final double x;
	private final double y;

	public LogicalPoint(final double x, final double y) {
		this.x = x;
		this.y = y;
	}

	// The point to rotate around by a certain number of radians
	public LogicalPoint rotate(LogicalPoint centre, double cos, double sin) {
		LogicalPoint translated = new LogicalPoint(this.x - centre.getX(), this.y - centre.getY());
		return new LogicalPoint(translated.getX() * cos - translated.getY() * sin + centre.x,
						translated.getX() * sin + translated.getY() * cos + centre.getY());
	}

	// Rotate by a multiple of pi
	public LogicalPoint rotate(LogicalPoint centre, int orientation) {
		return rotate(centre, LogicalPoint.rotations[orientation][0], 
							LogicalPoint.rotations[orientation][1]);
	}

	// Rotate by a specific angle
	public LogicalPoint rotate(LogicalPoint centre, double theta) {
		return rotate(centre, Math.cos(theta), Math.sin(theta));
	}
	
	public double getX(){
		return this.x;
	}
	
	public double getY(){
		return this.y;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		LogicalPoint other = (LogicalPoint) obj;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) {
			return false;
		}
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(String.valueOf(Math.round(this.getX())) + " " 
						+ String.valueOf(Math.round(this.getY())));
		return builder.toString();
	}

	public Vec2 getVec() {
		// Handy convience to get a vectory
		return new Vec2((float)x, (float)y);
	}
}
