package shapes;

public class GeometryCircle implements ICircle {
    private final LogicalPoint centre;
    private final double radius;

    public GeometryCircle(final LogicalPoint centre, final double radius){
        this.radius = radius;
        this.centre = centre;
    }

    @Override
    public ICircle getCircle() {
        return this;
    }

    @Override
    public IPolygon getPolygon() {
        return null;
    }

    @Override
    public LogicalPoint getCentre() {
        return this.centre;
    }
    
    public double getRadius(){
        return this.radius;
    }
    
    @Override
    public String toString() {
    	return centre.toString();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((centre == null) ? 0 : centre.hashCode());

		long temp = Double.doubleToLongBits(radius);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GeometryCircle other = (GeometryCircle) obj;
		if (centre == null) {
			if (other.centre != null) {
				return false;
			}
		} else if (!centre.equals(other.centre)) {
			return false;
		}
		if (Double.doubleToLongBits(radius) != Double
				.doubleToLongBits(other.radius)) {
			return false;
		}
		return true;
	}
    
    
}
