package shapes;


/**
 * An interface extension of {@link IGeometry.class}.
 */
public interface ICircle extends IGeometry {
	/**
	 * Fetch a {@link LogicalPoint.class} which contains information
	 * on the centre co-ordinates of the circle.
	 * @return a LogicalPoint
	 */
    public LogicalPoint getCentre();
    /**
     * Fetch a double value which represents the radius of the circle.
     * @return a double value
     */
    public double getRadius();
}
