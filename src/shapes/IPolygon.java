package shapes;

import java.util.List;


/**
 * An interface extension of {@link IGeometry.class}.
 */
public interface IPolygon extends IGeometry {
	/**
	 * Fetch a list of {@link LogicalPoint.class} which contains
	 * all necessary information on the co-ordinates of the polygon.
	 * @return a list of LogicalPoint
	 * (Invariant: Order dependent list whereby the order of points
	 * specifies the order of the lines to be drawn)
	 */
	public List<LogicalPoint> getPoints();
}
