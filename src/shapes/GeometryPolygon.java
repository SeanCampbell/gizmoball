package shapes;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GeometryPolygon implements IPolygon {
	
    private final List<LogicalPoint> points;
    
    public GeometryPolygon() {
    	this.points = new ArrayList<>();
    }
    
    public GeometryPolygon(final Collection<LogicalPoint> points){
        this.points = new ArrayList<>(points);
    }
	
    @Override
    public GeometryCircle getCircle() {
        return null;
    }

    @Override
    public IPolygon getPolygon() {
        return this;
    }

    @Override
    public List<LogicalPoint> getPoints() {
        return new ArrayList<>(this.points);
    }

    @Override
    public String toString() {
    	assert(this.points.size()!=0);
    	
    	return this.points.get(0).toString();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((points == null) ? 0 : points.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		GeometryPolygon other = (GeometryPolygon) obj;
		if (points == null) {
			if (other.points != null) {
				return false;
			}
		} else if (!points.equals(other.points)) {
			return false;
		}
		return true;
	}
    
    
}
