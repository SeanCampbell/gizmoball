package shapes;


/**
 * A common interface for different Geometric items.
 * The idea comes from the Null Object pattern, though naturally
 * is not an instance of the pattern since relevant method
 * conditionally returns null which must be checked for.
 */
public interface IGeometry {
	/**
	 * Fetch an instance of ICircle.
	 * @return null if the instance is an IPolygon,
	 * 		otherwise an instance of ICircle
	 */
	public ICircle getCircle();
	/**
	 * Fetch an instance of IPolygon.
	 * @return null if the instance is an ICircle,
	 * 		otherwise an instance of IPolygon
	 */
	public IPolygon getPolygon();
}
