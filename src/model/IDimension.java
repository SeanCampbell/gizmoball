package model;

import shapes.LogicalPoint;
import model.gizmos.Rectangle;
/**
 * An interface which can be exposed via the MVC architecture to the View;
 * allows the View to interpret the dimensions of particular Gizmos or 
 * GizmoBalls.
 */
public interface IDimension {
	/**
	 * Check whether the Gizmo or GizmoBall is of variable size.
	 * @return true if variable, false if not.
	 */
	boolean isVariableSize();
	/**
	 * Fetch the actual physical length in L-Value of the Gizmo or GizmoBall.
	 * @return a double representing the length in L-Value
	 */
	double getLength();
	/**
	 * Fetch the actual physical width in L-Value of the Gizmo or GizmoBall.
	 * @return a double representing the width in L-Value
	 */
	double getWidth();
	/**
	 * Retrieve a boundary box for the Gizmo or GizmoBall.  This denotes the
	 * maximum area which the Gizmo or GizmoBall can occupy through all
	 * permutations of its orientation.
	 * @return a Rectangle branching from the top-left corner of the item
	 * to the bottom-right corner
	 */
	Rectangle getBoundaryInfo();
	/**
	 * Fetch the origin of the Gizmo.
	 * @return a LogicalPoint allowing the origin to be accessed.
	 */
	LogicalPoint getOrigin();
}
