package model.gizmos;

public interface IBoardItem {
	/**
	 * 
	 * @return - the name of the board item.
	 */
	String getName();
	/**
	 * 
	 * @return - the string which represents the board item when it is saved.
	 */
	String getSaveString();
}
