package model.gizmos;

import java.awt.Color;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import model.GizmoTrigger;
import model.IGizmoBall;
import shapes.IGeometry;

public abstract class AbsGizmo implements IGizmo {
	// Reduce space consumed by the set; grows to four anyway to accommodate flippers
	private static final int ANTICPATED_CAP = 1;
	// Shape information
    private final Set<IGeometry> geometries;
    private final String name;
    private Color color;
    // If the gizmo acts as source, this is the trigger object
    private GizmoTrigger gt;
    
    protected AbsGizmo(final Color color){
    	this.geometries = new HashSet<>(AbsGizmo.ANTICPATED_CAP);
    	//this.geometries = new ArrayList<>(AbsGizmo.ANTICPATED_CAP);
    	this.name = null;
    	this.color = color;
    	this.gt = null;
    }
    
    protected AbsGizmo(final String name) {
    	this.geometries = new HashSet<>(AbsGizmo.ANTICPATED_CAP);
        this.name = name;
        this.gt = null;
    }
    
    protected AbsGizmo(final String name, final Color color) {
    	this.geometries = new HashSet<>(AbsGizmo.ANTICPATED_CAP);
    	this.name = name;
    	this.color = color;
    	this.gt = null;
    }
    
    @Override
    public String getName() {
    	return this.name;
    }
    
    @Override
    public Collection<IGeometry> getGeometryList() {
    	return Collections.unmodifiableCollection(this.geometries);
    }
    
    @Override
    public Color getColor() {
    	return this.color; //Color is immutable.
    }
    
    /**
     * The majority of gizmos are symmetrical.  The Gizmos which require 
     * rotation can simply override.
     */
    @Override
    public void rotate() throws UnsupportedOperationException {
    	throw new UnsupportedOperationException();
    }
    
    protected void addGeometry(final Collection<IGeometry> geometries) {
    	this.geometries.addAll(geometries);
    }
    
    protected void setGeometries(final Collection<IGeometry> new_geometries) {
    	this.geometries.clear();
    	this.geometries.addAll(new_geometries);
    }
    
    protected void setColour(final Color color) {
    	this.color = color;
    }
    
    public abstract void move(final int x, final int y);
     
    @Override
    public void collide(IGizmoBall ball) {
    	updateTrigger();
    }
    
    @Override
    public void tick(double deltaT){
    	return;
    }
    
    @Override
    public void addTrigger(GizmoTrigger g) {
    	this.gt = g;
    }
    
    @Override
    public void removeTrigger() {
    	this.gt = null;
    }
    
    @Override
    public void updateTrigger() {
    	if (this.gt != null) gt.onCollision(name);
    }
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AbsGizmo other = (AbsGizmo) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public Collection<IGizmoBall> hasGizmoBalls(){
		return null;
	}
}