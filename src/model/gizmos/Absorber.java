package model.gizmos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.World;

import model.GizmoFactory;
import model.GizmoSettings;
import model.IGizmoBall;
import shapes.GeometryPolygon;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class Absorber extends AbsGizmo {
	private static final String NAME = GizmoSettings.getName(Absorber.class);
	private static final Color DEFAULT = GizmoSettings.getDefaultColor(Absorber.class);
	// Used for action
	private final Queue<IGizmoBall> heldBalls;
	// Used for movement of absorber
	private final int length, width;
	private LogicalPoint origin;

	private Body physicsBody;
	
	static {
		GizmoFactory.getInstance().registerGizmo(Absorber.NAME, Absorber.class);
	}

	public Absorber(){
		super(Absorber.DEFAULT);
		this.heldBalls = null;
		this.origin = new LogicalPoint(0,0);
		this.width = 2;	
		this.length = 1;
		this.generateItems();
	}

	public Absorber(final String name, final int tlx, final int tly, final int brx,
			final int bry) {
		super(name, Absorber.DEFAULT);
		this.heldBalls = new LinkedList<>();
		this.origin = new LogicalPoint(tlx, tly);
		this.width = brx - tlx;	
		this.length = bry - tly;
		this.generateItems();
	}

	/**
	 * Generates the geometries and the physics properties for the absorber
	 * based on a List of Logical Point.
	 */
	private void generateItems(){
		List<LogicalPoint> points = generatePoints(this.origin.getX(), this.origin.getY());
		super.setGeometries(getShapes(points));
	}

	/**
	 * Generates a list of LogicalPoint which are the points from which the
	 * physics and the geometries for the gizmo can be determined. These points
	 * will be each corner of the Absorber - (x, y), (x, y + length of absorber), 
	 * (x + width of absorber, y + length of absorber) and (x + width of absorber, y), 
	 * where x and y are the corordinates of the origin (i.e. top left 
	 * corner of the absorber).
	 */
	private List<LogicalPoint> generatePoints(double x, double y) {
		List<LogicalPoint> points = new ArrayList<>();
		points.add(new LogicalPoint(x, y));
		points.add(new LogicalPoint(x + this.width, y));
		points.add(new LogicalPoint(x + this.width, y + this.length));
		points.add(new LogicalPoint(x, y + this.length));

		return points;
	}

	private Collection<IGeometry> getShapes(List<LogicalPoint> points) {
		Collection<IGeometry> shapes = new ArrayList<>();
		shapes.add(new GeometryPolygon(points));
		return shapes;
	}

	@Override
	public void move(final int x, final int y) {
		this.origin = new LogicalPoint(x, y);
		this.generateItems();
		if(!this.heldBalls.isEmpty()){
			for(IGizmoBall ball : this.heldBalls)
				ball.capture(new LogicalPoint(this.origin.getX() + this.width - 0.25, 
				this.origin.getY() + this.length - 0.25));
		}
	}

	@Override
	public void doAction() {
		if(!this.heldBalls.isEmpty()){
			IGizmoBall ball = this.heldBalls.remove();
			ball.release(new LogicalPoint(this.origin.getX() + this.width - 0.25, 
					this.origin.getY() - 0.25), GizmoSettings.ejectionVelocity());
		}
	}

	@Override
	public String getSaveString() {
		LogicalPoint end = new LogicalPoint(this.origin.getX() + this.width, this.origin.getY() + this.length);
		StringBuilder builder = new StringBuilder();
		builder.append(Absorber.NAME + " " + super.getName() + " " + this.origin.toString() + " " +
				end.toString() + "\n");
		return builder.toString();
	}

	@Override
	public Rectangle getBoundaryInfo(){
		return new Rectangle(this.origin.getX(), this.origin.getX() + this.getWidth(),
				this.origin.getY(), this.origin.getY() + this.getLength());
	}

	@Override
	public boolean isVariableSize() {
		return true;
	}

	@Override
	public double getLength() {
		return this.length;
	}

	@Override
	public double getWidth() {
		return this.width;
	}

	@Override
	public LogicalPoint getOrigin() {
		return this.origin;
	}

	@Override
	public void addPhysics(World world) {
		BodyDef base = new BodyDef();
	    base.position.set((float)origin.getX(), (float)origin.getY());
	    
	    PolygonShape square = new PolygonShape();
	    
	    // add vertices
	    Vec2 vertices[] = new Vec2[4];
	    vertices[0] = new Vec2(0, 0);
	    vertices[1] = new Vec2((float)this.width, 0);
	    vertices[2] = new Vec2((float)this.width, (float)this.length);
	    vertices[3] = new Vec2(0, (float)this.length);
	    
	    square.set(vertices, vertices.length);
	    
	    physicsBody = world.createBody(base);
	    physicsBody.setUserData(this);
	    physicsBody.createFixture(square, 0).m_isSensor = true;
	}
	
	@Override
	public void collide(IGizmoBall ball) {
		this.heldBalls.add(ball);
		ball.capture(new LogicalPoint(this.origin.getX() + this.width - 0.25, 
				this.origin.getY() + this.length - 0.25));
		updateTrigger();
	}

	@Override
	public void destroyPhysics(World world) {
		world.destroyBody(this.physicsBody);
	}
	
	@Override
	public Collection<IGizmoBall> hasGizmoBalls(){
		return this.heldBalls;
	}
}
