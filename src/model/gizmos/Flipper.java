package model.gizmos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.joints.RevoluteJoint;
import org.jbox2d.dynamics.joints.RevoluteJointDef;

import model.GizmoSettings;
import shapes.GeometryCircle;
import shapes.GeometryPolygon;
import shapes.IGeometry;
import shapes.LogicalPoint;

public abstract class Flipper extends AbsGizmo {
	private static final Color DEFAULT = GizmoSettings
			.getDefaultColor(Flipper.class);
	// Positional information
	private static final double CENTRE_X = GizmoSettings.getLvalue(),
			CENTRE_Y = CENTRE_X, WIDTH = Flipper.CENTRE_X / 2.0,
			LENGTH = Flipper.WIDTH * 3.0, RADIUS = Flipper.WIDTH / 2.0,
			OFFSET = Flipper.RADIUS;
	private static final double BOX_WIDTH = Flipper.CENTRE_X * 2.0,
			BOX_LENGTH = Flipper.CENTRE_Y * 2.0;
	// Angular information; initial and limiting angles in radians
	private static final double LEFT_INIT_RAD = 0,
			RIGHT_INIT_RAD = -Math.PI / 2;
	// Initial orientation for the flippers
	private static final int LEFT_INIT_OR = 0, RIGHT_INIT_OR = 1;
	// Internal reference so we don't need to do the check every time.
	private double internal_ang;
	// Used for movement and building the composite
	private LogicalPoint origin, pivot, unitCentre;	
	
	// Used for rotation
	private int orientation;
	private boolean isLeftFlipper;
	private boolean upswing;
	
	private Body physicsBody;
	private RevoluteJoint revolutionJoint;
	private Body circleJoint;

	public Flipper(final boolean isLeftFlipper) {
		super(Flipper.DEFAULT);
		this.origin = new LogicalPoint(0, 0);
		this.pivot = new LogicalPoint(Flipper.OFFSET, Flipper.OFFSET);
		this.unitCentre = new LogicalPoint(Flipper.CENTRE_X, Flipper.CENTRE_Y);
		if (isLeftFlipper) {
			this.orientation = LEFT_INIT_OR;
			this.internal_ang = LEFT_INIT_RAD;
		} else {
			this.orientation = Flipper.RIGHT_INIT_OR;
			this.internal_ang = Flipper.RIGHT_INIT_RAD;
		}

		this.generateItems();
	}

	public Flipper(final String name, final boolean leftFlipper, final int x,
			final int y) {
		super(name, Flipper.DEFAULT);
		this.isLeftFlipper = leftFlipper;
		this.upswing = false;
		this.origin = new LogicalPoint(x, y);
		this.pivot = new LogicalPoint(Flipper.OFFSET + x, Flipper.OFFSET + y);
		this.unitCentre = new LogicalPoint(Flipper.CENTRE_X + x,
				Flipper.CENTRE_Y + y);

		if (isLeftFlipper) {
			this.orientation = LEFT_INIT_OR;
			this.internal_ang = LEFT_INIT_RAD;
		} else {
			this.orientation = Flipper.RIGHT_INIT_OR;
			this.internal_ang = Flipper.RIGHT_INIT_RAD;
		}

		this.generateItems();
	}

	/**
	 * Generates the geometries and the physics properties for the flipper
	 * based on a List of Logical Point.
	 */
	private void generateItems() {
		List<LogicalPoint> points = this.generatePoints(this.internal_ang);
		super.setGeometries(getShapes(points));
	}
	
	private CircleShape getCircleShape(Vec2 pos){
	   	CircleShape circle = new CircleShape();
	   	circle.setRadius((float)Flipper.RADIUS);
	   	circle.m_p.set(new Vec2(pos.x, pos.y));
		
		return circle;
	}
	
	public void addPhysics(World world) {
		// create the flipper object from the points at angle 0
		List<LogicalPoint> points = generatePoints(internal_ang);
		
		BodyDef rectangle = new BodyDef();
		rectangle.position.set(origin.getVec().addLocal(1,1));
		rectangle.type = BodyType.DYNAMIC;
	    PolygonShape rect = new PolygonShape();
	    
		// create the circles first
		CircleShape circle1 = getCircleShape(points.get(0).getVec().sub(rectangle.position));
		CircleShape circle2 = getCircleShape(points.get(1).getVec().sub(rectangle.position));
		
	    // add vertices
	    Vec2 vertices[] = new Vec2[4];
	    vertices[0] = points.get(2).getVec().sub(rectangle.position);
	    vertices[1] = points.get(3).getVec().sub(rectangle.position);
	    vertices[2] = points.get(4).getVec().sub(rectangle.position);
	    vertices[3] = points.get(5).getVec().sub(rectangle.position);
	    rect.set(vertices, vertices.length);
	    
	    System.out.println(vertices[0]);
	    System.out.println(vertices[1]);
	    System.out.println(vertices[2]);
	    System.out.println(vertices[3]);
	    
	    FixtureDef fixture = new FixtureDef();
	    fixture.shape = circle1;
	    fixture.restitution = .95f;
	    fixture.density = 5;
	    FixtureDef fixture2 = new FixtureDef();
	    fixture2.shape = circle2;
	    fixture2.restitution = .95f;
	    fixture2.density = 5;
	    FixtureDef fixture3 = new FixtureDef();
	    fixture3.shape = rect;
	    fixture3.restitution = .95f;
	    fixture3.density = 5;
	    
	    // Create the body
	    physicsBody = world.createBody(rectangle);
	    physicsBody.createFixture(fixture);
	    physicsBody.createFixture(fixture2);
	    physicsBody.createFixture(fixture3);
	    physicsBody.m_mass = 15;
	    physicsBody.setUserData(this);
	    
	    BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.STATIC;
		bodyDef.position.set(points.get(0).getVec());
		circleJoint = world.createBody(bodyDef);
		
		System.out.println("S:" + points.get(0).getVec());
		Vec2 anchor = points.get(0).getVec().sub(rectangle.position);
		anchor = new Vec2(anchor.x, anchor.y);
		System.out.println("anchor at" + anchor.add(rectangle.position));
		System.out.println("centre at" + rectangle.position);
	    RevoluteJointDef rjd = new RevoluteJointDef();
	    rjd.initialize(physicsBody, circleJoint, points.get(0).getVec());
	    System.out.println("physicsBody.getWorldCenter()" + physicsBody.getWorldCenter());
	    rjd.maxMotorTorque = 1000;
	    rjd.motorSpeed = 0.4f;
	    rjd.enableLimit = true;
	    
	    if (isLeftFlipper) {
	    rjd.lowerAngle = (float)(0*Math.PI/180);
	    rjd.upperAngle = (float)(90*Math.PI/180);
	    }
	    else {
		    rjd.lowerAngle = -(float)(90*Math.PI/180);
		    rjd.upperAngle = (float)(0*Math.PI/180);
	    }
	    
	    revolutionJoint = (RevoluteJoint)world.createJoint(rjd);
	}

	private List<LogicalPoint> generatePoints(final double angle) {
		final double x = this.origin.getX(), y = this.origin.getY();
		List<LogicalPoint> points = new ArrayList<>();
		
		// pivot circle
		points.add(this.pivot.rotate(this.unitCentre, this.orientation));
		
		// end circle
		points.add(this.rotate(new LogicalPoint(x + Flipper.OFFSET, y
				+ Flipper.LENGTH + Flipper.OFFSET), this.unitCentre,
				this.pivot, this.orientation, angle)); 
		// tl - 2
		points.add(this.rotate(new LogicalPoint(x, y + Flipper.OFFSET),
				this.unitCentre, this.pivot, this.orientation, angle));
		// tr - 3
		points.add(this.rotate(new LogicalPoint(x, y + Flipper.OFFSET
				+ Flipper.LENGTH), this.unitCentre, this.pivot,
				this.orientation, angle));
		// br - 4
		points.add(this.rotate(new LogicalPoint(x + Flipper.WIDTH, y
				+ Flipper.OFFSET + Flipper.LENGTH), this.unitCentre,
				this.pivot, this.orientation, angle));
		// bl - 5
		points.add(this.rotate(new LogicalPoint(x + Flipper.WIDTH, y
				+ Flipper.OFFSET), this.unitCentre, this.pivot,
				this.orientation, angle));
		
		return points;
	}

	@Override
	public void move(final int x, final int y) {
		this.origin = new LogicalPoint(x, y);
		this.pivot = new LogicalPoint(Flipper.OFFSET + x, Flipper.OFFSET + y);
		this.unitCentre = new LogicalPoint(Flipper.CENTRE_X + x,
				Flipper.CENTRE_Y + y);
		this.generateItems();
	}

	@Override
	public void doAction() {
		if (upswing)
			upswing = false;
		else
			upswing = true;
	}
	
	@Override
	public void tick(double deltaT) {

		if (isLeftFlipper) {
			internal_ang = -(revolutionJoint.getJointAngle() - ((Math.PI/2) * orientation));
			
			if (upswing)
			{
				revolutionJoint.setMotorSpeed((float)(6*Math.PI));
				revolutionJoint.enableMotor(true);
			}
			else
			{
				revolutionJoint.setMotorSpeed(-(float)(6*Math.PI));
				revolutionJoint.enableMotor(true);
			}
		}
		else {
			internal_ang = -(revolutionJoint.getJointAngle() - 3*((Math.PI/2) * orientation));
			
			if (upswing)
			{
				revolutionJoint.setMotorSpeed(-(float)(6*Math.PI));
				revolutionJoint.enableMotor(true);
			}
			else
			{
				revolutionJoint.setMotorSpeed((float)(6*Math.PI));
				revolutionJoint.enableMotor(true);
			}
		}
		
		System.out.println("getJointAngle(): " + revolutionJoint.getJointAngle());
		System.out.println("cjoint: " + circleJoint.getPosition());
		System.out.println("centre at" + physicsBody.getPosition());
		this.generateItems();
	}

	private Collection<IGeometry> getShapes(List<LogicalPoint> points) {
		// This angle reference is temporary to allow view testing.
		// Will alter once decided how to use angle and alter it from above
		// level.
		return getShapeFlipper(this.internal_ang, points);
	}

	private Collection<IGeometry> getShapeFlipper(final double angle,
			List<LogicalPoint> points) {

		Collection<IGeometry> ret = new ArrayList<>();	

	

			ret.add(new GeometryCircle(points.get(0), Flipper.RADIUS));
			ret.add(new GeometryCircle(points.get(1), Flipper.RADIUS));
			ret.add(new GeometryPolygon(points.subList(2, points.size())));
		
		return ret;
	}

	private LogicalPoint rotate(final LogicalPoint point,
			final LogicalPoint centre, final LogicalPoint pivot,
			final int orientation, final double angle) {
		LogicalPoint temp = point.rotate(pivot, angle);
		temp = temp.rotate(centre, orientation);
		return temp;
	}

	@Override
	public String getSaveString() {
		StringBuilder builder = new StringBuilder();
		builder.append(" " + super.getName() + " " + this.origin.toString()
				+ "\n");
		int lim = (isLeftFlipper) ? this.orientation : this.orientation - 1;
		// If rightflipper rotated three times
		if (lim == -1)
			lim = 3;
		for (; lim != 0; lim--) {
			builder.append("Rotate" + super.getName() + "\n");
		}
		return builder.toString();
	}

	@Override
	public Rectangle getBoundaryInfo() {
		return new Rectangle(this.origin.getX(), this.origin.getX()
				+ Flipper.BOX_WIDTH, this.origin.getY(), this.origin.getY()
				+ Flipper.BOX_LENGTH);
	}

	@Override
	public boolean isVariableSize() {
		return false;
	}

	@Override
	public double getWidth() {
		return Flipper.WIDTH;
	}

	@Override
	public double getLength() {
		return Flipper.LENGTH;
	}

	@Override
	public LogicalPoint getOrigin() {
		return this.origin;
	}

	@Override
	public void destroyPhysics(World world) {
		world.destroyJoint(revolutionJoint);
		world.destroyBody(circleJoint);
		world.destroyBody(physicsBody);
	}
}
