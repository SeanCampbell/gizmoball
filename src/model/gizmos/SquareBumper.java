package model.gizmos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import shapes.IGeometry;
import shapes.LogicalPoint;
import shapes.GeometryPolygon;
import model.GizmoFactory;
import model.GizmoSettings;

public class SquareBumper extends AbsGizmo {
	private static final String NAME = GizmoSettings.getName(SquareBumper.class);
	private static final Color DEFAULT = GizmoSettings.getDefaultColor(SquareBumper.class), 
			ONACTION = GizmoSettings.getActiveColor(SquareBumper.class);
	private static final double OFFSET = GizmoSettings.getLvalue();
	
	private LogicalPoint origin;
	private Body physicsBody;
	private boolean disabled;
	
	static {
		GizmoFactory.getInstance().registerGizmo(SquareBumper.NAME, SquareBumper.class);
	}
	
	public SquareBumper(){
		super(SquareBumper.DEFAULT);
		this.origin = new LogicalPoint(0, 0);
		this.generateItems();
		disabled = true;
	}
	
	public SquareBumper(final String name, final int x, final int y) {
		super(name, SquareBumper.DEFAULT);
		this.origin = new LogicalPoint(x, y);
		this.generateItems();
		disabled = false;
	}
	
	public void addPhysics(World world) {
		BodyDef base = new BodyDef();
	    base.position.set((float)origin.getX(), (float)origin.getY());
	    
	    PolygonShape square = new PolygonShape();
	    
	    // add vertices
	    Vec2 vertices[] = new Vec2[4];
	    vertices[0] = new Vec2(0, 0);
	    vertices[1] = new Vec2(1, 0);
	    vertices[2] = new Vec2(1, 1);
	    vertices[3] = new Vec2(0, 1);
	    
	    square.set(vertices, vertices.length);
	    
	    FixtureDef fixture = new FixtureDef();
	    fixture.shape = square;
	    fixture.restitution = 1.f;
	    
	    physicsBody = world.createBody(base);
	    physicsBody.createFixture(fixture);
	    physicsBody.setUserData(this);
	    
	    if (disabled)
	    	physicsBody.setActive(false);
	}
	
	private void generateItems(){
		List<LogicalPoint> points = generatePoints(this.origin.getX(), this.origin.getY());
		super.setGeometries(getShapes(points));
	}
	
	/**
	 * Generates a list of LogicalPoint which are the points from which the
	 * physics and the geometries for the bumper can be determined. These points
	 * will be each corner of the Square Bumper - (x,y), (x,y+1), (x+1, y+1) and 
	 * (x+1,y), where x and y are the corordinates of the origin (i.e. top left 
	 * corner of the bumper.
	 */
	private List<LogicalPoint> generatePoints(double x, double y) {
		List<LogicalPoint> points = new ArrayList<>();
		points.add(new LogicalPoint(x, y));
		points.add(new LogicalPoint(x + SquareBumper.OFFSET, y));
		points.add(new LogicalPoint(x + SquareBumper.OFFSET, y + SquareBumper.OFFSET));
		points.add(new LogicalPoint(x, y + SquareBumper.OFFSET));
	
		return points;
	}
	
	private Collection<IGeometry> getShapes(List<LogicalPoint> points){
		Collection<IGeometry> shapes = new ArrayList<>();
		shapes.add(new GeometryPolygon(points));
		return shapes;
	}

	@Override
	public void move(final int x, final int y) {
		this.origin = new LogicalPoint(x, y);
		this.generateItems();
		
		if (!disabled && physicsBody != null) {
			physicsBody.setTransform(origin.getVec(), 0);
		}
	}

	@Override
	public void doAction() {
		super.setColour((super.getColor()==SquareBumper.DEFAULT)?
				SquareBumper.ONACTION :
					SquareBumper.DEFAULT);
	}
	
	@Override
	public String getSaveString() {
		StringBuilder builder = new StringBuilder();
		builder.append(SquareBumper.NAME + " " + super.getName() + " " + this.origin.toString() + "\n");
		return builder.toString();
	}
	
	@Override
	public Rectangle getBoundaryInfo(){
		return new Rectangle(this.origin.getX(), this.origin.getX() + SquareBumper.OFFSET,
				this.origin.getY(), this.origin.getY() + SquareBumper.OFFSET);
	}
	
	@Override
	public boolean isVariableSize() {
		return false;
	}

	@Override
	public double getLength() {
		return SquareBumper.OFFSET;
	}

	@Override
	public double getWidth() {
		return SquareBumper.OFFSET;
	}
	
	@Override
	public LogicalPoint getOrigin() {
		return this.origin;
	}

	@Override
	public void destroyPhysics(World world) {
		world.destroyBody(this.physicsBody);
	}
}
