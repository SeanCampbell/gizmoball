package model.gizmos;

public class Rectangle {
	private final double minx, maxx, miny, maxy;

	public Rectangle(final double minx, final double maxx, final double miny, final double maxy){
		this.minx = minx;
		this.maxx = maxx;
		this.miny = miny;
		this.maxy = maxy;
	}
	
	public double getMinx() {
		return this.minx;
	}

	public double getMaxx() {
		return this.maxx;
	}

	public double getMiny() {
		return this.miny;
	}

	public double getMaxy() {
		return this.maxy;
	}

	public boolean doIntersect(Rectangle rect){
		return this.getMaxx() > rect.getMinx() && this.getMinx() < rect.getMaxx() &&
	               this.getMaxy() > rect.getMiny() && this.getMiny() < rect.getMaxy(); 
	}
	
	public boolean exactIntersect(Rectangle rect){
		return this.getMaxx() >= rect.getMinx() && this.getMinx() <= rect.getMaxx() &&
	               this.getMaxy() >= rect.getMiny() && this.getMiny() <= rect.getMaxy(); 
	}
	
	public boolean contains(Rectangle rect){
		return this.getMinx() <= rect.getMinx() && this.getMiny() <= rect.getMiny() &&
				this.getMaxx() >= rect.getMaxx() && this.getMaxy() >= rect.getMaxy();		
	}
}
