package model.gizmos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;

import model.GizmoFactory;
import model.GizmoSettings;
import model.IGizmoBall;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Settings;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.World;

import shapes.GeometryCircle;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class GravityWell extends AbsGizmo {
	
	private static final String NAME = GizmoSettings.getName(GravityWell.class);
	private static final Color DEFAULT = GizmoSettings.getDefaultColor(GravityWell.class);
	private static final int WIDTH = 4, LENGTH = 4;
	private static final double OFFSET = 2;
	private static final double RADIUS = 2;
	
	// physical properties
	private LogicalPoint origin;
	private Body physicsBody;
	private boolean disabled;
	
	static {
		GizmoFactory.getInstance().registerGizmo(GravityWell.NAME, GravityWell.class);
	}
	
	public GravityWell() {
		super(GravityWell.DEFAULT);
		origin = new LogicalPoint(0, 0);
		super.setGeometries(getShapes());
		disabled = true;
	}
	
	public GravityWell(String name, int x, int y) {
		super(name, GravityWell.DEFAULT);
		origin = new LogicalPoint(x, y);
		super.setGeometries(getShapes());
		disabled = false;
	}
	
	@Override
	public void collide(IGizmoBall ball) {
		Vec2 centre = origin.getVec().addLocal((float)OFFSET, (float)OFFSET);
		Vec2 position = ball.getPosition().getVec();
		Vec2 d = centre.sub(position);
		
		if (d.lengthSquared() >= Settings.EPSILON*Settings.EPSILON) {
			d.normalize();
			Vec2 F = d.mulLocal(200f);
			ball.applyForce(F, centre);
		}
	}

	@Override
	public void doAction() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getSaveString() {
		StringBuilder builder = new StringBuilder();
		builder.append(GravityWell.NAME + " " + super.getName() + " " + this.origin.toString() + "\n");
		return builder.toString();
	}

	@Override
	public void addPhysics(World world) {
		BodyDef base = new BodyDef();
	    base.position.set(origin.getVec().addLocal((float)OFFSET, (float)OFFSET));
	    
	    CircleShape circle = new CircleShape();
	    
	    // set radius
	    circle.setRadius((float)RADIUS);
	    
	    physicsBody = world.createBody(base);
	    physicsBody.setUserData(this);
	    physicsBody.createFixture(circle, 0).m_isSensor = true;
	    
	    if (disabled)
	    	physicsBody.setActive(false);
	}

	@Override
	public void destroyPhysics(World world) {
		world.destroyBody(physicsBody);
	}

	@Override
	public boolean isVariableSize() {
		return false;
	}

	@Override
	public double getLength() {
		return LENGTH;
	}

	@Override
	public double getWidth() {
		return WIDTH;
	}

	@Override
	public Rectangle getBoundaryInfo() {
		return new Rectangle(this.origin.getX(), this.origin.getX() + WIDTH,
				this.origin.getY(), this.origin.getY() + LENGTH);
	}

	@Override
	public LogicalPoint getOrigin() {
		return this.origin;
	}

	@Override
	public void move(int x, int y) {
		origin = new LogicalPoint(x, y);
		super.setGeometries(getShapes());
		
		if (!disabled && physicsBody != null) {
			physicsBody.setTransform(origin.getVec().addLocal((float)OFFSET, (float)OFFSET), 0);
		}
	}
	
	private Collection<IGeometry> getShapes() {
		Collection<IGeometry> shapes = new ArrayList<>();
		shapes.add(new GeometryCircle(new LogicalPoint(this.origin.getX() + OFFSET,
				this.origin.getY() + OFFSET),
				RADIUS));
		return shapes;
	}

}
