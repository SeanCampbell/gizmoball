package model.gizmos;

import java.util.ArrayList;

import org.jbox2d.collision.shapes.EdgeShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import model.GizmoFactory;
import model.GizmoSettings;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class Wall extends AbsGizmo {
	private static final String NAME = GizmoSettings.getName(Wall.class);
	private static final int WIDTH = GizmoSettings.getGameBoardWidth();
	private static final int HEIGHT = GizmoSettings.getGameBoardLength();
	private ArrayList<Body> walls;
	
	static {
		GizmoFactory.getInstance().registerGizmo(Wall.NAME, Wall.class);
	}
	
	public Wall() {
		super(Wall.NAME);
		walls = new ArrayList<>();
		super.setGeometries(new ArrayList<IGeometry>());
	}
	
	public Wall(final String name) {
		super(name);
		walls = new ArrayList<>();
		super.setGeometries(new ArrayList<IGeometry>());
	}
	
	private Body createWall(World world, Vec2 start, Vec2 end) {
		BodyDef base = new BodyDef();
	    base.position.set(start);
	    
	    EdgeShape wall = new EdgeShape();
	    wall.set(start.sub(start), end.sub(start));
	    
	    FixtureDef fixture = new FixtureDef();
	    fixture.restitution = 1.f;
	    fixture.shape = wall;
	    
	    Body body = world.createBody(base);
	    body.createFixture(fixture);
	    body.setUserData(this);
	    return body;
	}
	
	public void addPhysics(World world) {
	    walls.add(createWall(world, new Vec2(0.f, 0.f), new Vec2(Wall.HEIGHT, 0.f)));
	    walls.add(createWall(world, new Vec2(Wall.HEIGHT, 0.f), new Vec2(Wall.HEIGHT, Wall.WIDTH)));
	    walls.add(createWall(world, new Vec2(Wall.HEIGHT, Wall.WIDTH), new Vec2(0.f, Wall.WIDTH)));
	    walls.add(createWall(world, new Vec2(0.f, Wall.WIDTH), new Vec2(0.f, 0.f)));
	}
	
	@Override
	public void move(int x, int y) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void doAction() {
		return;
	}

	@Override
	public String getSaveString() {
		return "";
	}
	
	@Override
	public Rectangle getBoundaryInfo(){
		return new Rectangle(-2.0, -2.0, -2.0, -2.0);
	}

	@Override
	public boolean isVariableSize() {
		return false;
	}

	@Override
	public double getLength() {
		return Wall.HEIGHT;
	}

	@Override
	public double getWidth() {
		return Wall.WIDTH;
	}
	
	@Override
	public LogicalPoint getOrigin() {
		return new LogicalPoint(0,0);
	}

	@Override
	public void destroyPhysics(World world) {
		for(Body wall : this.walls)
			world.destroyBody(wall);
	}
}
