package model.gizmos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.jbox2d.collision.shapes.PolygonShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import shapes.IGeometry;
import shapes.LogicalPoint;
import shapes.GeometryPolygon;
import model.GizmoFactory;
import model.GizmoSettings;

public class TriangleBumper extends AbsGizmo {
	private static final String NAME = GizmoSettings.getName(TriangleBumper.class);
	private static final Color DEFAULT = GizmoSettings.getDefaultColor(TriangleBumper.class), 
			ONACTION = GizmoSettings.getActiveColor(TriangleBumper.class);

	private static final double OFFSET = GizmoSettings.getLvalue(),
			CENTRE_OFFSET = TriangleBumper.OFFSET/2.0;
	// Used for rotation.
	private LogicalPoint origin;
	// Used for rotation and movement.
	private int orientation;
	private Body physicsBody;
	private boolean disabled;
	
	static {
		GizmoFactory.getInstance().registerGizmo(TriangleBumper.NAME, TriangleBumper.class);
	}
	
	public TriangleBumper(){
		super(TriangleBumper.DEFAULT);
		this.origin = new LogicalPoint(0, 0);
		this.generateItems();
		disabled = true;
	}
	
	public TriangleBumper(final String name, final int x, final int y) {
		super(name, TriangleBumper.DEFAULT);
		this.orientation = 0;
		this.origin = new LogicalPoint(x, y);
		this.generateItems();
		disabled = false;
	}
	
	public void addPhysics(World world) {
		BodyDef base = new BodyDef();
	    base.position.set((float)(origin.getX()+CENTRE_OFFSET), (float)(origin.getY()+CENTRE_OFFSET));
	    
	    PolygonShape square = new PolygonShape();
	    
	    // add vertices
	    Vec2 vertices[] = new Vec2[3];
	    vertices[0] = new Vec2(0-0.5f, 0-0.5f);
	    vertices[1] = new Vec2(1-0.5f, 0-0.5f);
	    vertices[2] = new Vec2(0-0.5f, 1-0.5f);
	    
	    square.set(vertices, vertices.length);
	    
	    FixtureDef fixture = new FixtureDef();
	    fixture.shape = square;
	    fixture.restitution = 1.f;
	    
	    physicsBody = world.createBody(base);
	    physicsBody.createFixture(fixture);
	    physicsBody.setUserData(this);
	    
	    if (disabled)
	    	physicsBody.setActive(false);
	}
	
	@Override
	public void destroyPhysics(World world) {
		world.destroyBody(this.physicsBody);
	}
	
	private void generateItems(){
		List<LogicalPoint> points = this.generatePoints();
		super.setGeometries(this.getShapes(points));
	}
	
	/**
	 * Generates a list of LogicalPoint which are the points from which the
	 * physics and the geometries for the bumper can be determined. These points
	 * will be each corner of the Triangle Bumper and can change depending on the rotation.
	 */
	private List<LogicalPoint> generatePoints() {
		final double x = this.origin.getX(), y = this.origin.getY();
		List<LogicalPoint> points = new ArrayList<>();
		LogicalPoint centre = new LogicalPoint(x + TriangleBumper.CENTRE_OFFSET,
							y + TriangleBumper.CENTRE_OFFSET);
		
		points.add(new LogicalPoint(x, y).rotate(centre, this.orientation));
		points.add(new LogicalPoint(x + TriangleBumper.OFFSET, y).rotate(centre, this.orientation));
		points.add(new LogicalPoint(x, y + TriangleBumper.OFFSET).rotate(centre, this.orientation));

		return points;
	}
	
	private Collection<IGeometry> getShapes(List<LogicalPoint> points){
		Collection<IGeometry> shapes = new ArrayList<>();
		shapes.add(new GeometryPolygon(points));
		return shapes;
	}

	@Override
	public void rotate() {
		this.orientation = (this.orientation + 1) % 4;
		System.out.println("oreitentation: " + orientation);
		
		if (!disabled && physicsBody != null) {
			physicsBody.setTransform(origin.getVec().addLocal((float)CENTRE_OFFSET, (float)CENTRE_OFFSET),
				(float)((90*orientation)*Math.PI)/180);
		}
		
		this.generateItems();
	}
	
	@Override
	public void move(final int x, final int y) {
		this.origin = new LogicalPoint(x, y);
		this.generateItems();
		
		if (!disabled && physicsBody != null) {
			physicsBody.setTransform(origin.getVec().addLocal((float)CENTRE_OFFSET, (float)CENTRE_OFFSET),
				(float)((90*orientation)*Math.PI)/180);
		}
	}

	@Override
	public void doAction() {
		super.setColour((super.getColor()==TriangleBumper.DEFAULT)?
				TriangleBumper.ONACTION :
					TriangleBumper.DEFAULT);
	}

	@Override
	public String getSaveString() {
		StringBuilder builder = new StringBuilder();
		builder.append(TriangleBumper.NAME + " " + super.getName() + " " + this.origin.toString() + "\n");
		for (int i = this.orientation; i != 0; i--) {
			builder.append("Rotate " + super.getName() + "\n");
		}
		return builder.toString();
	}
	
	@Override
	public Rectangle getBoundaryInfo(){
		return new Rectangle(this.origin.getX(), this.origin.getX() + TriangleBumper.OFFSET,
				this.origin.getY(), this.origin.getY() + TriangleBumper.OFFSET);
	}

	@Override
	public boolean isVariableSize() {
		return false;
	}

	@Override
	public double getLength() {
		return TriangleBumper.OFFSET;
	}

	@Override
	public double getWidth() {
		return TriangleBumper.OFFSET;
	}
	
	@Override
	public LogicalPoint getOrigin() {
		return this.origin;
	}
}
