package model.gizmos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import model.GizmoFactory;
import model.GizmoSettings;
import shapes.GeometryCircle;
import shapes.IGeometry;
import shapes.LogicalPoint;

public final class CircleBumper extends AbsGizmo {
	private static final String NAME = GizmoSettings.getName(CircleBumper.class);
	private static final Color DEFAULT = GizmoSettings.getDefaultColor(CircleBumper.class), 
			ONACTION = GizmoSettings.getActiveColor(CircleBumper.class);
	private static final double OFFSET = GizmoSettings.getLvalue()/2.0, 
			RADIUS = GizmoSettings.getLvalue()/2.0;
	private static final double LENGTH = GizmoSettings.getLvalue(), WIDTH = CircleBumper.LENGTH;
	
	private LogicalPoint origin;
	private Body physicsBody;
	private boolean disabled;
	
	static {
		GizmoFactory.getInstance().registerGizmo(CircleBumper.NAME, CircleBumper.class);
	}
	
	public CircleBumper(){
		super(CircleBumper.DEFAULT);
		this.origin = new LogicalPoint(0, 0);
		super.setGeometries(this.getShapes());
		disabled = true;
	}
	
	public CircleBumper(final String name, final int x, final int y) {
		super(name, CircleBumper.DEFAULT);
		this.origin = new LogicalPoint(x, y);
		super.setGeometries(this.getShapes());
		disabled = false;
	}
	
	@Override
	public void addPhysics(World world) {
		BodyDef base = new BodyDef();
	    base.position.set(origin.getVec().add(new Vec2((float)RADIUS, (float)RADIUS)));
	    
	    CircleShape circle = new CircleShape();
	    circle.setRadius((float)RADIUS);
	    
	    FixtureDef fixture = new FixtureDef();
	    fixture.shape = circle;
	    fixture.restitution = 1.f;
	    
	    physicsBody = world.createBody(base);
	    physicsBody.createFixture(fixture);
	    physicsBody.setUserData(this);
	    
	    if (disabled)
	    	physicsBody.setActive(false);
	}
	
	private Collection<IGeometry> getShapes() {
		Collection<IGeometry> shapes = new ArrayList<>();
		shapes.add(new GeometryCircle(new LogicalPoint(this.origin.getX() + CircleBumper.OFFSET,
				this.origin.getY() + CircleBumper.OFFSET),
				CircleBumper.RADIUS));
		return shapes;
	}

	@Override
	public void move(final int x, final int y) {
		this.origin = new LogicalPoint(x, y);
		super.setGeometries(this.getShapes());
		
		if (!disabled && physicsBody != null) {
			physicsBody.setTransform(origin.getVec(), 0);
		}
	}

	@Override
	public void doAction() {
		super.setColour((super.getColor()==CircleBumper.DEFAULT)?
				CircleBumper.ONACTION :
					CircleBumper.DEFAULT);
	}

	@Override
	public String getSaveString() {
		StringBuilder builder = new StringBuilder();
		builder.append(CircleBumper.NAME + " " + super.getName() + " " + this.origin.toString() + "\n");
		return builder.toString();
	}
	
	@Override
	public Rectangle getBoundaryInfo(){
		return new Rectangle(this.origin.getX(), this.origin.getX() + CircleBumper.WIDTH,
				this.origin.getY(), this.origin.getY() + CircleBumper.LENGTH);
	}

	@Override
	public boolean isVariableSize() {
		return false;
	}

	@Override
	public double getLength() {
		return CircleBumper.LENGTH;
	}

	@Override
	public double getWidth() {
		return CircleBumper.WIDTH;
	}
	
	@Override
	public LogicalPoint getOrigin() {
		return this.origin;
	}

	@Override
	public void destroyPhysics(World world) {
		world.destroyBody(this.physicsBody);
	}
}
