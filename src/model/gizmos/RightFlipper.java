package model.gizmos;

import model.GizmoFactory;
import model.GizmoSettings;


public class RightFlipper extends Flipper {
	private static final String NAME = GizmoSettings.getName(RightFlipper.class);
	static {
		GizmoFactory.getInstance().registerGizmo(RightFlipper.NAME, RightFlipper.class);
	}
	
	public RightFlipper() {
		super(false);
	}
	
	public RightFlipper(final String name, final int x, final int y) {
		super(name, false, x, y);
	}

	@Override
	public String getSaveString() {
		return RightFlipper.NAME + super.getSaveString();
	}
}
