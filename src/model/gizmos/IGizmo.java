package model.gizmos;

import java.util.Collection;

import model.IDimension;
import model.IGizmoBall;
import model.IPhysics;
import model.IShape;
import model.ITriggerSource;

public interface IGizmo extends IBoardItem, IPhysics, IShape, IDimension, ITriggerSource  {
	/**
	 * Rotates a gizmo 90 degrees to the right.
	 * @throws UnsupportedOperationException - throws this exception when a particular
	 * gizmo is unable to be rotated (eg. Absorber)
	 */
	void rotate() throws UnsupportedOperationException;
	/**
	 * Moves a gizmo the coordinates (x, y).
	 * @param x
	 * @param y
	 */
	void move(int x, int y);
	/**
	 * The gizmo performs its action (eg. change colour).
	 */
	void doAction();
	Collection<IGizmoBall> hasGizmoBalls();
}