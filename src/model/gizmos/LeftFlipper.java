package model.gizmos;

import model.GizmoFactory;
import model.GizmoSettings;


public class LeftFlipper extends Flipper {
	private static final String NAME = GizmoSettings.getName(LeftFlipper.class);
	static {
		GizmoFactory.getInstance().registerGizmo(LeftFlipper.NAME, LeftFlipper.class);
	}
	
	public LeftFlipper(){
		super(true);
	}
	
	public LeftFlipper(final String name, final int x, final int y) {
		super(name, true, x, y);
	}

	@Override
	public String getSaveString() {
		return LeftFlipper.NAME + super.getSaveString();
	}
}
