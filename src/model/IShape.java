package model;

import java.awt.Color;
import java.util.Collection;

import shapes.IGeometry;

/**
 * An interface which can be exposed via the MVC architecture to the View;
 * allows the View to interpret the shapes which correspond to a particular
 * gizmo as well as the colour of the shapes to render.
 */
public interface IShape {
	/**
	 * Fetch the colour of the Geometric shapes.
	 * @return the colour
	 */
	public Color getColor();
	/**
	 * Fetch a collection of {@link IGeometry.class} which correlates to the
	 * shapes a Gizmo is composed of.
	 * @return the collection of geometries linked to a Gizmo
	 */
	public Collection<IGeometry> getGeometryList();
}
