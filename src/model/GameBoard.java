package model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.World;

import model.gizmos.IGizmo;
import model.gizmos.Rectangle;
import model.gizmos.Wall;
import observer.IMessage;
import observer.IObservable;
import observer.IObserver;
import observer.Message;
import exceptions.DuplicateNameException;
import exceptions.ManufactureException;
import exceptions.NoSuchItemException;
import exceptions.OutOfBoundsException;
import exceptions.OverlappingItemException;

public class GameBoard implements IObservable, IGameBoard {
	private static final double DELTAT = 0.02;

	private final IGizmoFactory factory;
	private final Map<String, IDimension> gizmoDimensions;

	private final List<IObserver> observers;
	private final Map<String, IGizmo> gizmos;
	private final Map<String, IGizmoBall> gizmoballs;
	private final ITriggerManager tm;
	private final Rectangle boardArea;

	private final World world;

	public GameBoard(IGizmoFactory factory) {
		this.factory = factory;
		this.gizmoDimensions = this.initDimensions();

		this.observers = new ArrayList<>();
		this.gizmos = new HashMap<>();
		this.gizmoballs = new HashMap<>();
		this.tm = new TriggerManager();
		this.boardArea = new Rectangle(0, GizmoSettings.getGameBoardLength(),
				0, GizmoSettings.getGameBoardWidth());

		IGizmo wall = new Wall();
		this.gizmos.put(GizmoSettings.getName(Wall.class), wall);

		// create world
		this.world = new World(new Vec2(0, 25));
		wall.addPhysics(world);
		world.setContactListener(new MyContactListener());
	}

	@Override
	public synchronized void addObserver(final IObserver obs) {
		this.observers.add(obs);
	}

	@Override
	public synchronized void removeObserver(final IObserver obs) {
		this.observers.remove(obs);
	}

	@Override
	public synchronized void notifyObservers(final IMessage msg) {
		for (IObserver obs : this.observers)
			obs.update(msg);
	}

	private IMessage getMessage() {
		final List<Pair<String, IShape>> shapes = new LinkedList<>();

		// loop through every gizmo and add to list of shapes
		for (Map.Entry<String, IGizmo> entry : this.gizmos.entrySet()) {
			shapes.add(new Pair<String, IShape>(entry.getKey(), entry
					.getValue()));
		}
		for (Map.Entry<String, IGizmoBall> entry : this.gizmoballs.entrySet()) {
			shapes.add(new Pair<String, IShape>(entry.getKey(), entry
					.getValue()));
		}

		return new Message(shapes);
	}

	public void fireNotification() {
		this.notifyObservers(this.getMessage());
	}

	@Override
	public void addGizmo(String type, String name, Object... params)
			throws DuplicateNameException, OverlappingItemException,
			ManufactureException, OutOfBoundsException {
		// Check factory has and no duplicate name
		checkAddGizmo(type, name);

		IGizmo gizmo = null;
		final Object[] params_ = new Object[params.length + 1];
		for (int i = 0; i < params.length; i++) {
			params_[i + 1] = params[i];
		}
		params_[0] = name;
		// create gizmo using these parameters
		try {
			gizmo = this.factory.createGizmo(type, params_);
		} catch (Exception e) {
			throw new ManufactureException(
					"Something went wrong in the manufacturing process!");
		}
		Rectangle rect = gizmo.getBoundaryInfo();
		if (this.isItemAt(rect))
			throw new OverlappingItemException(
					"This would cause gizmos to overlap!");
		if (!this.boardArea.contains(rect))
			throw new OutOfBoundsException(
					"This would cause gizmos to be outwith the playing area!");
		// add to our map
		gizmos.put(name, gizmo);

		// add to physics
		gizmo.addPhysics(world);
		fireNotification();
	}

	private void checkAddGizmo(final String type, final String name)
			throws DuplicateNameException, ManufactureException {
		if (!GizmoFactory.getInstance().canManufacture(type))
			throw new ManufactureException(
					"Factory cannot manufacture gizmo type " + type);
		else if (this.gizmos.containsKey(name)
				|| this.gizmoballs.containsKey(name)) {
			throw new DuplicateNameException("Duplicate name exception: "
					+ name);
		}
	}

	@Override
	public void moveGizmo(final String name, final int destx, final int desty)
			throws OverlappingItemException, NoSuchItemException,
			OutOfBoundsException {
		// Check the top left box as an immediate check. Rectangle check later.
		if (this.hasGizmoAt(destx, desty))
			throw new OverlappingItemException(
					"Move will cause overlap with another gizmo!");
		// Fetch the name of the item to move
		if (!this.gizmos.containsKey(name))
			throw new NoSuchItemException("No such gizmo " + name);
		final IGizmo giz = this.gizmos.remove(name);
		final double origX = giz.getOrigin().getX(), origY = giz.getOrigin()
				.getY();
		giz.move(destx, desty);
		final Rectangle rect = giz.getBoundaryInfo();
		if (this.isItemAt(rect)) {
			giz.move(Math.round(Math.round(origX)),
					Math.round(Math.round(origY)));
			this.gizmos.put(name, giz);
			throw new OverlappingItemException(
					"Move will cause overlap with another gizmo!");
		}
		if (!this.boardArea.contains(rect)) {
			giz.move(Math.round(Math.round(origX)),
					Math.round(Math.round(origY)));
			this.gizmos.put(name, giz);
			throw new OutOfBoundsException(
					"This would cause gizmos to be outwith the playing area!");
		}

		this.gizmos.put(name, giz);
		fireNotification();
	}

	@Override
	public void rotateGizmo(final String name) throws NoSuchItemException {
		if (!this.gizmos.containsKey(name))
			throw new NoSuchItemException("No such gizmo " + name);
		this.gizmos.get(name).rotate();
		fireNotification();
	}

	@Override
	public void removeGizmo(final String name) throws NoSuchItemException {
		if (!this.gizmos.containsKey(name))
			throw new NoSuchItemException("No such gizmo " + name);

		IGizmo ref = this.gizmos.remove(name);
		Collection<IGizmoBall> balls = ref.hasGizmoBalls();
		if (balls != null) {
			for (IGizmoBall ball : balls)
				this.removeGizmoBall(ball.getName());
		}
		ref.destroyPhysics(world);
		if (this.tm.flushGizmo(ref))
			ref = null;
		fireNotification();
	}

	public boolean connect(final String giz1, final String giz2)
			throws NoSuchItemException {
		if (!this.gizmos.containsKey(giz1))
			throw new NoSuchItemException("No such gizmo " + giz1);
		else if (!this.gizmos.containsKey(giz2))
			throw new NoSuchItemException("No such gizmo " + giz2);

		return this.tm.connect(this.gizmos.get(giz1), this.gizmos.get(giz2));
	}

	@Override
	public boolean connect(final int keyCode, String giz, final boolean keyPress)
			throws NoSuchItemException {
		if (!this.gizmos.containsKey(giz))
			throw new NoSuchItemException("No such gizmo " + giz);

		return this.tm.connect(keyCode, gizmos.get(giz), keyPress);
	}

	@Override
	public boolean disconnect(final String giz1, final String giz2)
			throws NoSuchItemException {
		if (!this.gizmos.containsKey(giz1))
			throw new NoSuchItemException("No such gizmo " + giz1);
		else if (!this.gizmos.containsKey(giz2))
			throw new NoSuchItemException("No such gizmo " + giz2);

		return this.tm.disconnect(this.gizmos.get(giz1), this.gizmos.get(giz2));
	}

	@Override
	public boolean disconnect(final int keyCode, final String giz,
			final boolean keyPress) throws NoSuchItemException {
		if (!this.gizmos.containsKey(giz))
			throw new NoSuchItemException("No such gizmo " + giz);

		return this.tm.disconnect(keyCode, gizmos.get(giz), keyPress);
	}

	@Override
	public void disconnect(String giz) throws NoSuchItemException {
		if (!this.gizmos.containsKey(giz))
			throw new NoSuchItemException("No such gizmo " + giz);

		this.tm.flushGizmo(this.gizmos.get(giz));
	}

	@Override
	public ITriggerManager getTM() {
		return this.tm;
	}

	@Override
	public void addGizmoBall(final String name, final double x, final double y,
			final double vx, final double vy) throws DuplicateNameException,
			OverlappingItemException, OutOfBoundsException {
		if (this.gizmoballs.containsKey(name) || this.gizmos.containsKey(name))
			throw new DuplicateNameException("Duplicate name exception: "
					+ name);

		final IGizmoBall ball = new GizmoBall(name,
				GizmoSettings.getDefaultColor(GizmoBall.class), x, y, vx, vy);
		final Rectangle rect = ball.getBoundaryInfo();
		if (this.isItemAt(rect))
			throw new OverlappingItemException(name
					+ " will overlap with another gizmo!");
		if (!this.boardArea.contains(rect))
			throw new OutOfBoundsException(
					"This would cause gizmos to be outwith the playing area!");
		this.gizmoballs.put(name, ball);
		ball.addPhysics(world);
		fireNotification();
	}

	@Override
	public void moveGizmoBall(String name, final double destx,
			final double desty) throws OverlappingItemException,
			NoSuchItemException, OutOfBoundsException {
		if (this.hasGizmoBallAt(destx, desty))
			throw new OverlappingItemException(
					"Move will cause overlap with another gizmo!");
		if (!this.gizmoballs.containsKey(name))
			throw new NoSuchItemException("No such gizmoball " + name);

		final IGizmoBall b = this.gizmoballs.remove(name);
		final double origX = b.getOrigin().getX(), origY = b.getOrigin().getY();
		b.move(destx, desty);
		final Rectangle rect = b.getBoundaryInfo();
		if (isItemAt(rect)) {
			b.move(origX, origY);
			this.gizmoballs.put(name, b);
			throw new OverlappingItemException(
					"Move will cause overlap with another gizmo!");
		}
		if (!this.boardArea.contains(rect)) {
			b.move(origX, origY);
			this.gizmoballs.put(name, b);
			throw new OutOfBoundsException(
					"This would cause gizmos to be outwith the playing area!");
		}
		this.gizmoballs.put(name, b);
		b.destroyPhysics(world);
		b.addPhysics(world);
		fireNotification();
	}

	@Override
	public void removeGizmoBall(final String name) throws NoSuchItemException {
		if (!this.gizmoballs.containsKey(name))
			throw new NoSuchItemException("No such gizmoball " + name);

		this.gizmoballs.remove(name).destroyPhysics(world);
		fireNotification();
	}

	@Override
	public void setGravity(final double grav) {
		GizmoSettings.setGravity(grav);
		world.setGravity(new Vec2(0, (float) grav));
	}

	@Override
	public void setFriction(final double mu, final double mu2) {
		GizmoSettings.setMU(mu);
		GizmoSettings.setMU2(mu2);
	}

	@Override
	public void tick() {
		world.step((float) DELTAT, 8, 3);
		for (IGizmoBall balls : gizmoballs.values()) {
			balls.tick(DELTAT);
		}
		for (IGizmo gizmo : gizmos.values()) {
			gizmo.tick(DELTAT);
		}

		fireNotification();
	}

	public void clearBoard() {
		this.gizmos.clear();
		this.gizmoballs.clear();
		this.tm.clearTriggers();
		fireNotification();
	}

	private boolean isItemAt(final Rectangle rect) {
		for (IGizmo giz : this.gizmos.values())
			if (rect.doIntersect(giz.getBoundaryInfo()))
				return true;
		for (IGizmoBall ball : this.gizmoballs.values())
			if (rect.doIntersect(ball.getBoundaryInfo()))
				return true;

		return false;
	}

	/**
	 * These exposed methods segregate the board items, take different
	 * parameters and use exact positioning.
	 */

	@Override
	public String gizmoNameAt(final int x, final int y)
			throws NoSuchItemException {
		final Rectangle rect = new Rectangle(x + 0.00000001, x + 0.00000001,
				y + 0.00000001, y + 0.00000001);
		for (IGizmo giz : this.gizmos.values())
			if (rect.exactIntersect(giz.getBoundaryInfo()))
				return giz.getName();
		throw new NoSuchItemException("Cannot find a gizmo at " + x + " " + y);
	}

	@Override
	public String gizmoBallNameAt(final double x, final double y)
			throws NoSuchItemException {
		final Rectangle rect = new Rectangle(x, x, y, y);
		for (IGizmoBall giz : this.gizmoballs.values())
			if (rect.exactIntersect(giz.getBoundaryInfo()))
				return giz.getName();
		throw new NoSuchItemException("Cannot find a gizmo at " + x + " " + y);
	}

	@Override
	public boolean hasGizmoAt(final int x, final int y) {
		final Rectangle rect = new Rectangle(x + 0.00000001, x + 0.00000001,
				y + 0.00000001, y + 0.00000001);
		for (IGizmo giz : this.gizmos.values())
			if (rect.exactIntersect(giz.getBoundaryInfo()))
				return true;
		return false;
	}

	@Override
	public boolean hasGizmoBallAt(final double x, final double y) {
		final Rectangle rect = new Rectangle(x, x, y, y);
		for (IGizmoBall ball : this.gizmoballs.values())
			if (rect.exactIntersect(ball.getBoundaryInfo()))
				return true;
		return false;
	}

	@Override
	public boolean hasGizmo(final String name) {
		return this.gizmos.containsKey(name);
	}

	@Override
	public boolean hasGizmoBall(final String name) {
		return this.gizmoballs.containsKey(name);
	}

	private Map<String, IDimension> initDimensions() {
		final Map<String, IDimension> ret = new HashMap<>();
		try {
			ret.putAll(this.factory.createDimensions());
		} catch (Exception e) {
			System.err.println("Error in initDimensions(). " + e.toString());
		}
		final IDimension gizBall = new GizmoBall();
		ret.put(GizmoSettings.getName(GizmoBall.class), gizBall);
		return ret;
	}

	public Map<String, IShape> getGizmoLooks() {
		final Map<String, IShape> ret = new HashMap<>();
		try {
			ret.putAll(this.factory.createGizmos());
		} catch (Exception e) {
			System.err.println("Error in getGizmoLooks().");
		}
		return ret;
	}

	/**
	 * Returns an entire map in the case there are mutlipe types of gizmoballs
	 * in the implementation.
	 */
	public Map<String, IShape> getGizmoBallLooks() {
		final Map<String, IShape> ret = new HashMap<>();
		final IShape gizBall = new GizmoBall();
		ret.put(GizmoSettings.getName(GizmoBall.class), gizBall);
		return ret;
	}

	public IDimension getGizmoDimensions(final String type)
			throws NoSuchItemException {
		if (!this.gizmoDimensions.containsKey(type))
			throw new NoSuchItemException("No such item: " + type);

		return this.gizmoDimensions.get(type);
	}

	public String saveBoard() {
		return this.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for (Map.Entry<String, IGizmo> entry : this.gizmos.entrySet()) {
			builder.append(entry.getValue().getSaveString());
		}
		for (Map.Entry<String, IGizmoBall> entry : this.gizmoballs.entrySet()) {
			builder.append(entry.getValue().getSaveString());
		}
		builder.append(this.tm.saveTriggers());
		builder.append(GizmoSettings.saveGravity());
		builder.append(GizmoSettings.saveFriction());
		return builder.toString();
	}

	@Override
	public IDimension getGizmoDimensionByName(String name)
			throws NoSuchItemException {
		if (!this.gizmos.containsKey(name))
			throw new NoSuchItemException("No such gizmo " + name);

		return this.gizmos.get(name);
	}

	@Override
	public IDimension getGizmoBallDimensionByName(String name)
			throws NoSuchItemException {
		if (!this.gizmoballs.containsKey(name))
			throw new NoSuchItemException("No such gizmoball " + name);

		return this.gizmoballs.get(name);
	}
}
