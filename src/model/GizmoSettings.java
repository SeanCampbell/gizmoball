package model;

import java.awt.Color;

public class GizmoSettings {
	// Java is unable to a) switch based on an array of strings and b) switch on the hashcodes of constant string
	private static final String C = "CircleBumper", S = "SquareBumper",
						T = "TriangleBumper", A = "Absorber",
						L = "LeftFlipper", R = "RightFlipper", F = "Flipper",
						B = "GizmoBall", W = "Wall", G = "GravityWell";
	
	// Names
	private static final String C_BUMP = "Circle", S_BUMP = "Square", T_BUMP = "Triangle",
								ABS = "Absorber", L_FLIPPER = "LeftFlipper", R_Flipper = "RightFlipper",
								BALL = "Ball", WALL = "OuterWalls", GRAV = "GravityWell";
	// Default colours (consistent with current GUI images)
	private static final Color DEF_C = Color.ORANGE, DEF_S = Color.PINK, DEF_T = Color.CYAN, DEF_A = Color.GREEN,
			DEF_F = Color.YELLOW, DEF_B = Color.RED, DEF_GRAV = new Color(0.f, 0.77f, 0.77f, 0.60f);
	// Action colours
	private static final Color ACT_C = Color.BLUE, ACT_S = Color.YELLOW, ACT_T = Color.MAGENTA;
	// Value of L
	private static final double LVALUE = 1.0;
	private static final int LENGTH = 20, WIDTH = 20;
	// Absorber ejection velocty
	private static final float EJECTION_X = 0, EJECTION_Y = -50;
	// Save strings for physics effects
	private static final String SAVE_GRAVITY = "Gravity";
	private static final String SAVE_FRICTION = "Friction";
	// Mutables
	private static double GRAVITY = 25.0, MU = 0.025, MU2 = 0.025;
	
		
	public static String getName(Class<? extends Object> cls){
		final String name = cls.getSimpleName();
		String desiredName = null;
		switch(name){
			case GizmoSettings.C:	desiredName = GizmoSettings.C_BUMP;
									break;
			case GizmoSettings.S:	desiredName = GizmoSettings.S_BUMP;
									break;
			case GizmoSettings.T:	desiredName = GizmoSettings.T_BUMP;
									break;
			case GizmoSettings.A:	desiredName = GizmoSettings.ABS;
									break;
			case GizmoSettings.L:	desiredName = GizmoSettings.L_FLIPPER;
									break;
			case GizmoSettings.R:	desiredName = GizmoSettings.R_Flipper;
									break;
			case GizmoSettings.B:	desiredName = GizmoSettings.BALL;
									break;
			case GizmoSettings.W:	desiredName = GizmoSettings.WALL;
									break;
			case GizmoSettings.G:	desiredName = GizmoSettings.GRAV;
									break;
			default:	assert false : "Unknown callee.";
			
		}
		return desiredName;
	}
	
	public static Color getDefaultColor(Class<? extends Object> cls){
		final String name = cls.getSimpleName();
		Color desiredColor = null;
		switch(name){
			case GizmoSettings.C:	desiredColor = GizmoSettings.DEF_C;
									break;
			case GizmoSettings.S:	desiredColor = GizmoSettings.DEF_S;
									break;
			case GizmoSettings.T:	desiredColor = GizmoSettings.DEF_T;
									break;
			case GizmoSettings.A:	desiredColor = GizmoSettings.DEF_A;
									break;
			case GizmoSettings.B:	desiredColor = GizmoSettings.DEF_B;
									break;
			case GizmoSettings.F:	desiredColor = GizmoSettings.DEF_F;
									break;
			case GizmoSettings.G:	desiredColor = GizmoSettings.DEF_GRAV;
									break;
			default:	assert false : "Unknown callee.";
			
		}
		return desiredColor;
	}
	
	public static Color getActiveColor(Class<? extends Object> cls){
		final String name = cls.getSimpleName();
		Color desiredColor = null;
		switch(name){
			case GizmoSettings.C:	desiredColor = GizmoSettings.ACT_C;
									break;
			case GizmoSettings.S:	desiredColor = GizmoSettings.ACT_S;
									break;
			case GizmoSettings.T:	desiredColor = GizmoSettings.ACT_T;
									break;
			default:	assert false : "Unknown callee.";
		}
		return desiredColor;
	}
	
	public static double getLvalue() {
		return GizmoSettings.LVALUE;
	}

	public static double getGravity() {
		return GizmoSettings.GRAVITY;
	}
	
	public static void setGravity(final double grav){
		GizmoSettings.GRAVITY = grav;
	}

	public static double getMu() {
		return GizmoSettings.MU;
	}
	
	public static void setMU(final double mu){
		GizmoSettings.MU = mu;
	}

	public static double getMu2() {
		return GizmoSettings.MU2;
	}
	
	public static void setMU2(final double mu2){
		GizmoSettings.MU2 = mu2;
	}

	
	public static int getGameBoardWidth(){
		return GizmoSettings.WIDTH;
	}
	
	public static int getGameBoardLength(){
		return GizmoSettings.LENGTH;
	}
	
	public static String saveGravity(){
		StringBuilder builder = new StringBuilder();
		builder.append(GizmoSettings.SAVE_GRAVITY + " " + Double.toString(GizmoSettings.getGravity()) + "\n");
		return builder.toString();
	}
	
	public static String saveFriction(){
		StringBuilder builder = new StringBuilder();
		builder.append(GizmoSettings.SAVE_FRICTION + " " + Double.toString(GizmoSettings.getMu()) + " " +
				Double.toString(GizmoSettings.getMu2()) + "\n");
		return builder.toString();
	}
	
	public static org.jbox2d.common.Vec2 ejectionVelocity(){
		return new org.jbox2d.common.Vec2(GizmoSettings.EJECTION_X, GizmoSettings.EJECTION_Y);
	}
}
