package model;

import java.util.HashSet;
import java.util.Set;

import model.gizmos.IGizmo;

public class GizmoTrigger implements ITrigger {
	private static final String COMMAND = "Connect";
	private Set<IGizmo> gizmos;
	private IGizmo source;

	public GizmoTrigger(IGizmo source) {
		this.source = source;
		gizmos = new HashSet<IGizmo>();
	}

	@Override
	public IGizmo getSource() {
		return source;
	}
	
	@Override
	public boolean connect(IGizmo gizmo, boolean x) {
		return gizmos.add(gizmo);
	}
	
	public boolean canDisconnect(IGizmo gizmo){
		return gizmos.contains(gizmo);
	}

	@Override
	public boolean disconnect(IGizmo gizmo, boolean x) {
		return gizmos.remove(gizmo);
	}

	@Override
	public boolean isEmpty() {
		return gizmos.isEmpty();
	}
	
	@Override
	public boolean onCollision(String gizmoName) {
		if (source.getName().equals(gizmoName)){
			for (IGizmo gizmo : gizmos) {
				gizmo.doAction();
			}
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onKeyEvent(int i, boolean b) {
		return false;
	}

	@Override
	public String saveTrigger() {
		StringBuilder builder = new StringBuilder();
		for(IGizmo giz : this.gizmos)
			builder.append(GizmoTrigger.COMMAND + " " + this.source.getName() + " " + giz.getName() + "\n");
		return builder.toString();
	}
}
