package model;

import model.gizmos.IGizmo;

/**
 * The interface for the Trigger Manager class, which holds all the ITrigger
 * objects and relays commands to them. It also allows streamlining of commands
 * (eg key presses can bypass the board)
 * 
 * @author Stuart Bennett
 * 
 */
public interface ITriggerManager {

	/**
	 * Sets up a connection between giz1 and giz2 by adding giz2 to the Trigger
	 * with giz1 as source. If no such trigger object exists, it will be created
	 * 
	 * @param giz1
	 *            The source of the new connection
	 * @param giz2
	 *            The reciever for the new connection
	 * @return true if the connection was established, false if the connection
	 *         already existed.
	 */
	public boolean connect(IGizmo giz1, IGizmo giz2);

	/**
	 * Sets up a connection between a keyEvent and a gizmo, with the key Event
	 * identified by its keyCode and whether it was a keyPress or keyRelease.
	 * The keyCode is treated as the source. If no such source is present, a new
	 * trigger object will be created
	 * 
	 * @param keyCode
	 *            The keyCode of the trigger source
	 * @param giz
	 *            The reciver of the new connection
	 * @param keyPress
	 *            Whether the trigger is linked to a keyPress or a keyRelease
	 * @return true if the connection was established, false if the connection
	 *         already existed.
	 */
	public boolean connect(int keyCode, IGizmo giz, boolean keyPress);

	/**
	 * Finds a connection between giz1 and giz2, and if it exists, severs it. If
	 * this leaves giz1 with no recievers, it is removed from the collection of
	 * triggers
	 * 
	 * @param giz1
	 *            The source of the connection to be cut
	 * @param giz2
	 *            The reciever of the connection to be cut
	 * @return true if the connection was found and severed, false if there was
	 *         no such connection
	 */
	public boolean disconnect(IGizmo giz1, IGizmo giz2);

	/**
	 * Finds the trigger object related to keyCode, and removes giz from the
	 * keyDown recievers or the keyUp recievers set depending on keyPress. If
	 * the trigger object now has no recievers, the object is removed from the
	 * collection of triggers
	 * 
	 * @param keyCode
	 *            the keyCode of the source key
	 * @param giz
	 *            the reciever to be cut
	 * @param keyPress
	 *            whether the reciever is to be removed from the keyDown set or
	 *            the keyUp set
	 * @return true if the connection was found and severed, false if there was
	 *         no such connection
	 */
	public boolean disconnect(int keyCode, IGizmo giz, boolean keyPress);

	/**
	 * Calls onKeyEvent(keyCode, keyPress) on all ITriggers until either one
	 * returns true or the end of the set is reached
	 * 
	 * @param keyCode
	 *            the keyCode of the source of the correct trigger
	 * @param keypress
	 *            whether the keyEvent was a keyUp or a keyDown
	 * @return true if a trigger was activated, false otherwise
	 */
	public boolean onKeyEvent(int keyCode, boolean keyPress);

	/**
	 * Removes any triggers that have gizmo as a source and disconnects gizmo
	 * from all sources. To be used before a gizmo is deleted
	 * 
	 * @param gizmo The gizmo to be flushed from the Manager
	 * @return true when it's finished. This lets the board know it's safe to continue
	 */
	public boolean flushGizmo(IGizmo gizmo);

	/**
	 * Removes all triggers from the set
	 */
	public void clearTriggers();
	/**
	 * Creates the save string for a particular trigger.
	 * @return String representing the value to be written to file
	 */
	public String saveTriggers();
}
