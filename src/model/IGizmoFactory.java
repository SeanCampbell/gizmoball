package model;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import model.gizmos.IGizmo;

public interface IGizmoFactory {
	/**
	 * Check whether the factory can manufacture a particular type of Gizmo
	 * @param name The type to check the factory for
	 * @return true if the factory can, false if not
	 */
	boolean canManufacture(String name);
	/**
	 * Request the factory manufacture all Gizmos it is capable of producing and 
	 * return a map of gizmo type to their respective shapes
	 * @return a map of gizmo types to IShapes, allowing the fetching of Gizmo appearances
	 * @throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException
	 * 			IllegalArgumentException, InvocationTargetException
	 */
	Map<String, IShape> createGizmos() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;
	/**
	 * Request the factory manufacture all Gizmos it is capable of producing and 
	 * return a map of the gizmo type to the dimensions of that gizmo.
	 * @return a map of String to IDimension, allowing the controller to be informed of boundaries
	 * @throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException
	 * 			IllegalArgumentException, InvocationTargetException
	 */
	Map<String, IDimension> createDimensions() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;
	/**
	 * Request the factory build a Gizmo by supplying the name of the Gizmo type.
	 * @param name the type of Gizmo to be created
	 * @return an IGizmo, abstraction from the concrete implementation
	 * @throws InstantiationException, IllegalAccessException
	 */
	IGizmo createGizmo(String name) throws InstantiationException, IllegalAccessException;
	/**
	 * Request the factory manufacture a Gizmo by supplying the name and the arguments required for the
	 * desired constructor.
	 * @return an IGizmo, abstraction from the concrete implementation
	 * @throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException
	 * 			IllegalArgumentException, InvocationTargetException
	 */
	IGizmo createGizmo(String name, Object ...args) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException;
	/**
	 * Register a Gizmo with the factory, allowing the factory to create instances of the class.
	 * Requires that the class be loaded already.
	 * @param name The name the factory is to associate with a particular class; this is the parameter
	 * 				supplied when requesting the factory create a particular type.
	 * @param an IGizmo, abstraction from the concrete implementation
	 */
	void registerGizmo(String name, Class<? extends IGizmo> gizmo);
}
