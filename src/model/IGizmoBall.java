package model;

import org.jbox2d.common.Vec2;

import shapes.LogicalPoint;
import model.gizmos.IBoardItem;

public interface IGizmoBall extends IBoardItem, IPhysics, IShape, IDimension {
	/**
	 * Moves the ball to the position (x, y)
	 * @param x - x coordinate 
	 * @param y - y coordinate
	 */
	void move(double x, double y);
	
	/**
	 * @return - a physics vector representing the position of this ball.
	 */
	LogicalPoint getPosition();
	
	/**
	 * Sets the position of the ball based on the physics vector.
	 * @param vect - the new position of the ball represented as a vector.
	 */
	void setPosition(LogicalPoint vect);
	
	/**
	 * @return - a physics vector representing the current velocity of the ball.
	 */
	Vec2 getVelocity();
	
	/**
	 * Sets the velocity of the ball to the velocity vect.
	 * @param vect - the new velocity of the ball.
	 */
	void setVelocity(Vec2 vect);

	/**
	 * Returns true if the ball is captured in the absorber and false
	 * otherwise.
	 */
	boolean isCaptured();
	
	/**
	 * Invoked when the ball collides with the absorber. The ball will retained
	 * in the absorber at the position specified by vect and the balls velocity
	 * set to stationary.
	 * @param vect - the position at which the absorber will hold the ball while
	 * it is captured.
	 */
	void capture(LogicalPoint pt);
	
	/**
	 * Invoked when the absorber's action is called and the ball is released from the
	 * absorber. Consequently, the absorber will no longer be considered to be 
	 * captured and its velocity will be set to vect.
	 * @param vect - the new velocity of the ball once it is released from the absorber.
	 */
	void release(LogicalPoint releasePoint, Vec2 vect);

	/**
	 * Apply a force to be included on the next frame.
	 * @param f the force to be applied
	 * @param position the direction to apply
	 */
	void applyForce(Vec2 f, Vec2 position);	
}

