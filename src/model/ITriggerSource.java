package model;

/**
 * This is a modified version of the Observer Pattern, the difference being
 * there will only be one observer
 * 
 * @author Stuart Bennett
 */
public interface ITriggerSource {
	/**
	 * Gives the gizmo a reference to the trigger object of which it is the
	 * source. This allows the gizmo to activate this object.
	 * 
	 * @param g
	 *            the trigger object
	 */
	public void addTrigger(GizmoTrigger g);

	/**
	 * Removes the reference to a trigger object and sets it to null. This means
	 * that if said trigger object is deleted, that a null pointer doesn't occur
	 */
	public void removeTrigger();

	/**
	 * Tells the trigger object to trigger the actions of all recipients
	 */
	public void updateTrigger();
}
