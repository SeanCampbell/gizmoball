package model;

import org.jbox2d.dynamics.World;

/**
 * The main interface for objects that have an effect on other objects on the
 * board. Which at this point is all of them.
 */
public interface IPhysics {
	/**
	 * Uses the information in the ball to change its direction upon contact
	 * with the local physics model, then updates the trigger
	 * 
	 * @param ball
	 *            The gizmoball that collided with this
	 */
	public void collide(IGizmoBall ball);

	/**
	 * Simulates a single time step for this object
	 * 
	 * @param deltaT
	 *            the duration of one time step
	 */
	public void tick(double deltaT);

	/**
	 * Uses information from the world to generate a physics model and registers
	 * it with the world
	 * 
	 * @param world
	 *            The world the object is inhabiting
	 */
	public void addPhysics(World world);

	/**
	 * Unregisters the physics model with the world and dereferences it
	 * 
	 * @param world
	 *            The world the object is inhabiting
	 */
	public void destroyPhysics(World world);
}
