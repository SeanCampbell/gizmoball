package model;

import model.gizmos.IGizmo;

/**
 * The interface for the individual triggers, implemented by GizmoTrigger and
 * KeyTrigger which use a different source (a gizmo and a keycode respectively)
 * 
 * @author Stuart Bennett
 */
public interface ITrigger {

	/**
	 * Returns either an Integer or Gizmo object representing the source of the
	 * trigger. This is hidden behind the Object interface as it is assumed it
	 * will only be used for equality purposes
	 * 
	 * @return the Gizmo or Integer that acts as the source of the trigger
	 */
	public Object getSource();

	/**
	 * Adds a gizmo to the set of gizmos that will be activated by the source
	 * using HashSet.add(), with keypress denoting which set in the case of a
	 * KeyTrigger (GizmoTriggers ignore the boolean)
	 * 
	 * @param gizmo
	 *            the Gizmo to be connected to the source
	 * @return true if the gizmo was added successfully, false if the gizmo is
	 *         already present
	 */
	public boolean connect(IGizmo gizmo, boolean keyPress);

	/**
	 * Removes a gizmo from the set of gizmos that will be activated by the
	 * source using HashSet.remove(), with keypress denoting which set in the
	 * case of a KeyTrigger (GizmoTriggers ignore the boolean)
	 * 
	 * @param gizmo
	 *            the Gizmo to be disconnected from the source
	 * @return true if the gizmo was removed successfully, false if the gizmo
	 *         was not already present
	 */
	public boolean disconnect(IGizmo gizmo, boolean keyPress);
	
	/**
	 * Returns whether or not the source set is empty, using HashSet.isEmpty()
	 * 
	 * @return true if the set is empty, false otherwise
	 */
	public boolean isEmpty();

	/**
	 * If gizmoName matches the name of the source, the trigger will call
	 * doAction() on all gizmos in the set and return true. Whether or not the
	 * set is empty does not affect the return value
	 * 
	 * @param gizmoName
	 *            the name of the trigger that should be activated, which may
	 *            not be this one
	 * @return true if this is a gizmo trigger with the source identified by
	 *         gizmoName, false otherwise
	 */
	public boolean onCollision(String gizmoName);

	/**
	 * If keyCode matches the source keycode, the trigger will call
	 * doAction(keypress) on all gizmos in the set and return true. Whether or
	 * not the set is empty does not affect the return value
	 * 
	 * @param keyCode
	 *            the code of the key that should activate a trigger, which may
	 *            not be this one
	 * @param keypress
	 *            whether the event was a key press or a key release
	 * @return true if this is a key trigger with keycode as its source, false
	 *         otherwise
	 */
	public boolean onKeyEvent(int keyCode, boolean keypress);
	/**
	 * Fetch the specific string which is to be saved to a file, allowing the connection to be
	 * re-established upon load.
	 * @return the save string
	 */
	public String saveTrigger();
}
