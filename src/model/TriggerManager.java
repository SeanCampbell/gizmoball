package model;

import java.util.Set;
import java.util.HashSet;

import model.gizmos.IGizmo;

public class TriggerManager implements ITriggerManager {

	private Set<ITrigger> triggers;

	public TriggerManager() {
		this.triggers = new HashSet<>();
	}

	@Override
	public boolean connect(IGizmo giz1, IGizmo giz2) {
		for (ITrigger t : triggers) {
			if (giz1.equals(t.getSource())) {
				return t.connect(giz2, true);
			}
		}
		GizmoTrigger t = new GizmoTrigger(giz1);
		giz1.addTrigger(t);
		t.connect(giz2, true);
		return triggers.add(t);
	}

	@Override
	public boolean connect(int keyCode, IGizmo giz, boolean keyPress) {
		for (ITrigger t : triggers) {
			if (t.getSource().equals(keyCode)) {
				return t.connect(giz, keyPress);
			}
		}
		ITrigger t = new KeyTrigger(keyCode);
		t.connect(giz, keyPress);
		return triggers.add(t);
	}

	@Override
	public boolean disconnect(IGizmo giz1, IGizmo giz2) {
		for (ITrigger t : triggers) {
			if (giz1.equals(t.getSource())) {
				if (t.disconnect(giz2, false)) {
					if (t.isEmpty()) {
						giz1.removeTrigger();
						triggers.remove(t);
					}
					return true;
				} else
					return false;
			}
		}
		return false;
	}

	@Override
	public boolean disconnect(int keyCode, IGizmo giz, boolean keyPress) {
		for (ITrigger t : triggers) {
			if (t.getSource().equals(keyCode)) {
				if (t.disconnect(giz, keyPress)) {
					if (t.isEmpty()) {
						triggers.remove(t);
					}
					return true;
				} else
					return false;
			}
		}
		return false;
	}

	@Override
	public boolean onKeyEvent(int keyCode, boolean keypress) {
		for (ITrigger t : triggers) {
			if (t.onKeyEvent(keyCode, keypress))
				return true;
		}
		return false;
	}
	
	@Override
	public boolean flushGizmo (IGizmo gizmo) {
		for (ITrigger t : triggers) {
			if (gizmo.equals(t.getSource())){
				gizmo.removeTrigger();
				triggers.remove(t);
			}
			else {
				t.disconnect(gizmo,false);
				t.disconnect(gizmo,true);
			}
		}
		return true;
	}
	
	public String saveTriggers(){
		StringBuilder builder = new StringBuilder();
		for(ITrigger trig : this.triggers)
			builder.append(trig.saveTrigger());
		return builder.toString();
	}
	
	// Only call this for creating a new board.
	// Unsure if we should also iterate through the triggers and explicitly remove
	// to assist with garbage collection since objects will persist.
	@Override
	public void clearTriggers(){
		this.triggers.clear();
	}
}
