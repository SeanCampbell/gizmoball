package model;

public class Pair<A, B> {
    
    private final A a;
    private final B b;
    
    public Pair(final A a, final B b) {
        this.a = a;
        this.b = b;
    }
    
    public A getA() {
        return this.a;
    }
    
    public B getB() {
        return this.b;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Pair<?, ?>)) {
            return false;
        }
        
        // ok try and identify the types
        final Pair<?, ?> other = (Pair<?, ?>)obj;
        
        // check if A and B are equal
        return (getA().equals(other.getA())) && (getB().equals(other.getB()));
    }
}