package model;

import java.util.HashSet;
import java.util.Set;

import model.gizmos.IGizmo;

public class KeyTrigger implements ITrigger {
	private static final String COMMAND = "KeyConnect";
	private static final String KEY = "key";
	private static final String UP = "up", DOWN = "down";

	private Set<IGizmo> downGizmos;
	private Set<IGizmo> upGizmos;
	private int keyCode;

	public KeyTrigger(int keyCode) {
		this.keyCode = keyCode;
		downGizmos = new HashSet<IGizmo>();
		upGizmos = new HashSet<IGizmo>();
	}

	@Override
	public Integer getSource() {
		return keyCode;
	}

	@Override
	public boolean connect(IGizmo gizmo, boolean keyPress) {
		if (keyPress)
			return downGizmos.add(gizmo);
		else
			return upGizmos.add(gizmo);
	}

	@Override
	public boolean disconnect(IGizmo gizmo, boolean keyPress) {
		if (keyPress)
			return downGizmos.remove(gizmo);
		else
			return upGizmos.remove(gizmo);
	}

	@Override
	public boolean isEmpty() {
		return downGizmos.isEmpty() && upGizmos.isEmpty();
	}

	@Override
	public boolean onCollision(String gizmoName) {
		return false;
	}

	@Override
	public boolean onKeyEvent(int keyCode, boolean keyPress) {
		if (this.keyCode == keyCode) {
			if (keyPress)
				for (IGizmo gizmo : downGizmos) {
					gizmo.doAction();
				}
			else
				for (IGizmo gizmo : upGizmos) {
					gizmo.doAction();
				}
			return true;
		}
		return false;
	}

	@Override
	public String saveTrigger() {
		StringBuilder builder = new StringBuilder();
		for (IGizmo giz : this.upGizmos)
			builder.append(KeyTrigger.COMMAND + " " + KeyTrigger.KEY + " "
					+ this.keyCode + " " + KeyTrigger.UP + " " + giz.getName() + "\n");
		for (IGizmo giz : this.downGizmos)
			builder.append(KeyTrigger.COMMAND + " " + KeyTrigger.KEY + " "
					+ this.keyCode + " "  + KeyTrigger.DOWN + " " + giz.getName()
					+ "\n");
		return builder.toString();
	}
}
