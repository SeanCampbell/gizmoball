package model;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import model.gizmos.IGizmo;

public class GizmoFactory implements IGizmoFactory {
	private static IGizmoFactory instance;
	private HashMap<String, Class<? extends IGizmo>> registeredGizmos;
	
	static {
		try {
			Class.forName("model.GizmoSettings");
			Class.forName("model.gizmos.LeftFlipper");
			Class.forName("model.gizmos.RightFlipper");
			Class.forName("model.gizmos.SquareBumper");
			Class.forName("model.gizmos.CircleBumper");
			Class.forName("model.gizmos.TriangleBumper");
			Class.forName("model.gizmos.Absorber");
			Class.forName("model.gizmos.Wall");
			Class.forName("model.gizmos.GravityWell");
		}
		
		catch(ClassNotFoundException e) {
			// just exit, something will go wrong
			e.printStackTrace();
			System.exit(-1);
		}
	}

	public static IGizmoFactory getInstance() {
		if(instance == null) {
			instance = new GizmoFactory();
		}
	
		// return the instance of this factory
		return instance;
	}
	
	// private constructor for singleton pattern
	private GizmoFactory() {
		registeredGizmos = new HashMap<String, Class<? extends IGizmo>>();
	}
	

	@Override
	public boolean canManufacture(String name) {
		boolean found = false;
		if(registeredGizmos.containsKey(name))
			found = true;
		return found;
	}
	
	@Override
	public Map<String, IShape> createGizmos() throws NoSuchMethodException, SecurityException, 
			InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Map<String, IShape> ret = new HashMap<>();
		for(Map.Entry<String, Class<? extends IGizmo>> entry : this.registeredGizmos.entrySet()){
			ret.put(entry.getKey(), this.createGizmo(entry.getKey()));
		}
		return ret;
	}
	
	@Override
	public Map<String, IDimension> createDimensions()
			throws NoSuchMethodException, SecurityException,
			InstantiationException, IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		Map<String, IDimension> ret = new HashMap<>();
		for(Map.Entry<String, Class<? extends IGizmo>> entry : this.registeredGizmos.entrySet()){
			ret.put(entry.getKey(), this.createGizmo(entry.getKey()));
		}
		return ret;
	}
	
	@Override
	public IGizmo createGizmo(String name) throws InstantiationException, IllegalAccessException {
		// check if we have this gizmo registered
		if(!registeredGizmos.containsKey(name)) {
			throw new InstantiationException("Unrecognised gizmo: " + name);
		}

		//create the gizmo
		return registeredGizmos.get(name).newInstance();
	}
	
	@Override
	public IGizmo createGizmo(String name, Object ...args) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		// check if we have this gizmo registered
		if(!registeredGizmos.containsKey(name)) {
			throw new InstantiationException("Unrecognised gizmo: " + name);
		}

		System.out.println("ctor: " + registeredGizmos.get(name));
		// get the constructor we need
		@SuppressWarnings("unchecked")
		Constructor<? extends IGizmo> ctor = getConstructor((Constructor<? extends IGizmo>[]) registeredGizmos.get(name).getConstructors(), args);
		
		System.out.println("ctor: " + ctor);
		// create object
		return ctor.newInstance(args);
	}
	
	@Override
	public void registerGizmo(String name, Class<? extends IGizmo> gizmo) {
		registeredGizmos.put(name, gizmo);
	}
	
	private Constructor<? extends IGizmo> getConstructor(Constructor<? extends IGizmo>[] ctors, Object[] args) throws InstantiationException {
		for(Constructor<? extends IGizmo> ctor : ctors) {
			// do a simple check to see if we have a possible match
			if(ctor.getParameterTypes().length != args.length) {
				System.out.println("got arg: " + ctor.getParameterTypes().length);
				System.out.println("got arg: " + args.length);
				continue;
			}

			boolean match = true;
			
			// loop through types to try and find a match
			for(int i = 0; i < args.length; i++) {
				Class<?> type = args[i].getClass();
				Class<?> ctor_type = ctor.getParameterTypes()[i];
				
				// check if our argument is not assignable
				if(!ctor_type.isAssignableFrom(type)) {
					// check if its a primitive
					if(ctor_type.isPrimitive()) {
						// taken from stackoverflow, ty
						// check if the passed type matches the boxed class
						match = (int.class.equals(ctor_type) && Integer.class.equals(type))
								|| (double.class.equals(ctor_type)) && Double.class.equals(type)
			                    || (long.class.equals(ctor_type) && Long.class.equals(type))
			                    || (char.class.equals(ctor_type) && Character.class.equals(type))
			                    || (short.class.equals(ctor_type) && Short.class.equals(type))
			                    || (boolean.class.equals(ctor_type) && Boolean.class.equals(type))
			                    || (byte.class.equals(ctor_type) && Byte.class.equals(type));
					}
					else {
						match = false;
						break;
					}
				}
			}
			// check if this CTOR matched
			if(match) {
				// it did, return it
				return ctor;
			}
		}
		
		// throw an instantiation error
		throw new InstantiationException("Could not find acceptable constructor");
	}
}
