package model;

import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.contacts.Contact;

import model.IGizmoBall;

public class MyContactListener implements ContactListener {

	@Override
	public void beginContact(Contact con) {
		// get the two associated body
		Body a = con.getFixtureA().getBody();
		Body b = con.getFixtureB().getBody();
		
		if (a.getUserData() instanceof IPhysics && b.getUserData() instanceof IPhysics) {
			if (a.getUserData() instanceof IGizmoBall) {
				IGizmoBall ball = (IGizmoBall)a.getUserData();
				IPhysics physics = (IPhysics)b.getUserData();
				
				// do collision
				physics.collide(ball);
			}
			
			if (b.getUserData() instanceof IGizmoBall) {
				IGizmoBall ball = (IGizmoBall)b.getUserData();
				IPhysics physics = (IPhysics)a.getUserData();
				
				// do collision
				physics.collide(ball);
			}
		}
	}

	@Override
	public void endContact(Contact con) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postSolve(Contact arg0, ContactImpulse arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void preSolve(Contact arg0, Manifold arg1) {
		// TODO Auto-generated method stub
		
	}

}
