package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.jbox2d.collision.shapes.CircleShape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.jbox2d.dynamics.BodyDef;
import org.jbox2d.dynamics.BodyType;
import org.jbox2d.dynamics.FixtureDef;
import org.jbox2d.dynamics.World;

import model.gizmos.Rectangle;
import shapes.GeometryCircle;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class GizmoBall implements IGizmoBall {
	private static final String NAME = GizmoSettings.getName(GizmoBall.class);
	private static final double RADIUS = GizmoSettings.getLvalue()/4.0;
	private final String id;

	private Color colour;
	
	// visual related
	private LogicalPoint position;
	private boolean isCaptured;
	
	// physics related stuff
	private Body physicsBody;
	private Vec2 velocity;
	private boolean releaseOnTick;
	
	public GizmoBall(){
		this.id = null;
		position = new LogicalPoint(RADIUS, RADIUS);
	}

	public GizmoBall(final String id,Color colour, double x, double y, double xVel, double yVel) {
		this.id = id;
		Random rng = new Random();
		this.colour = new Color(rng.nextFloat(), rng.nextFloat(), rng.nextFloat());
		position = new LogicalPoint(x, y);
		velocity = new Vec2((float)xVel, (float)yVel);
		isCaptured = false;
	}

	@Override
	public void addPhysics(World world) {
		BodyDef base = new BodyDef();
	    base.position.set(position.getVec());
	    base.type = BodyType.DYNAMIC;
	    
	    CircleShape circle = new CircleShape();
	    circle.setRadius((float)RADIUS);
	    
	    FixtureDef fd = new FixtureDef();
	    fd.restitution = 0.75f;
	    fd.density = 5.f;
	    fd.shape = circle;
	    fd.friction = 0.025f;
	    
	    physicsBody = world.createBody(base);
	    physicsBody.createFixture(fd);
	    physicsBody.m_userData = this;
	}
	
	@Override
	public void collide(IGizmoBall ball) {
	}

	@Override
	public String getName(){
		return this.id;
	}

	@Override
	public Color getColor() {
		return colour;
	}

	@Override
	public List<IGeometry> getGeometryList() {
		List<IGeometry> shapes = new ArrayList<>(1);
		shapes.add(new GeometryCircle(new LogicalPoint(position.getX(), position.getY()), RADIUS));
		return shapes;
	}

	@Override
	public void move(double x, double y) {
		position = new LogicalPoint(x, y);
	}

	@Override
	public LogicalPoint getPosition() {
		return position;
	}

	@Override
	public void setPosition(LogicalPoint pt) {
		physicsBody.setTransform(pt.getVec(), 0.f);
		position = pt;
	}

	@Override
	public boolean isCaptured() {
		return isCaptured;
	}

	@Override
	public void release(LogicalPoint position, Vec2 velocity) {
		this.releaseOnTick = true;
		this.position = position;
		this.velocity = velocity;
	}

	@Override
	public void capture(LogicalPoint pt){
		isCaptured = true;
		this.velocity = new Vec2(0, 0);
		this.position = pt;
		releaseOnTick = false;
	}

	public String getSaveString(){
		return this.toString();
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(GizmoBall.NAME + " " + this.id + " " + this.position.getX() + " " +
				this.position.getY() + " " + this.velocity.x + " " + this.velocity.y + "\n");
		return builder.toString();
	}

	@Override
	public boolean isVariableSize() {
		return false;
	}

	@Override
	public double getLength() {
		return GizmoBall.RADIUS*2.0;
	}

	@Override
	public double getWidth() {
		return GizmoBall.RADIUS*2.0;
	}

	@Override
	public Rectangle getBoundaryInfo(){
		return new Rectangle(this.position.getX() - GizmoBall.RADIUS, this.position.getX() + GizmoBall.RADIUS,
				this.position.getY() - GizmoBall.RADIUS, this.position.getY() + GizmoBall.RADIUS);
	}

	@Override
	public LogicalPoint getOrigin() {
		return new LogicalPoint(this.position.getX(), this.position.getY());
	}

	@Override
	public void tick(double deltaT) {
		if(isCaptured){
			physicsBody.setActive(false);
			
			if (releaseOnTick) {
				physicsBody.setTransform(position.getVec(), 0);
				physicsBody.setLinearVelocity(velocity);
				physicsBody.setActive(true);
				releaseOnTick = false;
				isCaptured = false;
			}
		}
		else{
			float mag = physicsBody.getLinearVelocity().length();
			float scale = (float)(1-GizmoSettings.getMu()*deltaT - GizmoSettings.getMu2()*mag*deltaT);
			Vec2 force = physicsBody.getLinearVelocity().mul(scale).sub(physicsBody.getLinearVelocity());
			physicsBody.applyForceToCenter(force.mulLocal(25));
			position = new LogicalPoint(physicsBody.getPosition().x, physicsBody.getPosition().y);
		}
	}

	@Override
	public Vec2 getVelocity() {
		return this.velocity;
	}

	@Override
	public void setVelocity(Vec2 vect) {
		velocity = vect;
		physicsBody.setLinearVelocity(velocity);
	}

	@Override
	public void destroyPhysics(World world) {
		world.destroyBody(this.physicsBody);
	}

	@Override
	public void applyForce(Vec2 f, Vec2 position) {
		// TODO Auto-generated method stub
		physicsBody.applyForce(f, position);
	}
}
