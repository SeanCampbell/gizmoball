package model;

import java.util.Map;

import exceptions.DuplicateNameException;
import exceptions.ManufactureException;
import exceptions.NoSuchItemException;
import exceptions.OutOfBoundsException;
import exceptions.OverlappingItemException;
import observer.IObservable;
/**
 * The crucial model interface declaring the methods which can be invoked upon
 * any instance of an IGameBoard.
 *
 */
public interface IGameBoard extends IObservable {
	/**
	 * Gizmo oriented operations
	 */
	/**
	 * Add a Gizmo to the instance of IGameBoard
	 * @param type the type of Gizmo to be added
	 * @param name the name of the Gizmo to be added
	 * @param params a variadic argument which can contain any
	 * 			number of primitive values or their auto-boxed equivalents
	 * 
	 * @throws DuplicateNameException {@link DuplicateNameException}
	 * @throws OverlappingItemException {@link OverlappingItemException}
	 * @throws ManufactureException {@link ManufactureException}
	 * @throws OutOfBoundsException {@link OutOfBoundsException}
	 */
	void addGizmo(String type, String name, Object... params) throws DuplicateNameException,
		OverlappingItemException, ManufactureException, OutOfBoundsException;
	/**
	 * Move an existing Gizmo to a particular location of the game-board
	 * @param name the String identifier of the Gizmo to move
	 * @param destx the x co-ordinate to move the Gizmo to
	 * @param desty the y co-ordinate to move the Gizmo to
	 * 
	 * @throws OverlappingItemException {@link OverlappingItemException}
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 * @throws OutOfBoundsException {@link OutOfBoundsException}
	 */
	void moveGizmo(String name, int destx, int desty) throws OverlappingItemException, NoSuchItemException,
		OutOfBoundsException;
	/**
	 * Rotate an existing Gizmo
	 * @param name the String identifier of the Gizmo to move
	 * 
	 * @throws UnsupportedOperationException thrown to indicate the operation on the Gizmo is unsupported
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	void rotateGizmo(String name) throws UnsupportedOperationException, NoSuchItemException;
	/**
	 * Remove an existing Gizmo
	 * @param name the String identifier of the Gizmo to move
	 * 
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	void removeGizmo(String name) throws NoSuchItemException;
	
	/**
	 * GizmoBall oriented operations
	 */
	/**
	 * Add a GizmoBall to the instance of IGameBoard
	 * @param name the name of the GizmoBall to be added
	 * @param x the x co-ordinate indicating the origin of the GizmoBall
	 * @param y the y co-ordinate indicating the origin of the GizmoBall
	 * @param vx the initial velocity in the x direction of the GizmoBall
	 * @param vy the initial velocity in the y direction of the GizmoBall
	 * 
	 * @throws DuplicateNameException {@link DuplicateNameException}
	 * @throws OverlappingItemException {@link OverlappingItemException}
	 * @throws OutOfBoundsException {@link OutOfBoundsException}
	 */
	void addGizmoBall(String name, double x, double y, double vx, double vy) throws DuplicateNameException,
		OverlappingItemException, OutOfBoundsException;
	/**
	 * Move an existing GizmoBall to a specific location
	 * @param name the String identifier of the GizmoBall to move
	 * @param destx the x co-ordinate to move the GizmoBall to
	 * @param desty the y co-ordinate to move the GizmoBall to
	 * 
	 * @throws OverlappingItemException {@link OverlappingItemException}
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 * @throws OutOfBoundsException {@link OutOfBoundsException}
	 */
	void moveGizmoBall(String name, double destx, double desty) throws OverlappingItemException, NoSuchItemException,
		OutOfBoundsException;
	/**
	 * Remove an existing GizmoBall from the game-board
	 * @param name the String identifier of the GizmoBall to remove
	 * 
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	void removeGizmoBall(String name) throws NoSuchItemException;
	
	/**
	 * Trigger oriented operations
	 */
	/**
	 * Fetch the the instance of the ITriggerManager employed in the model
	 * @return the ITriggerManager
	 */
	ITriggerManager getTM();
	/**
	 * Creates a connections between giz1 and giz2 whereby giz1 is the source
	 * of the trigger and giz2 is the target
	 * @param giz1 the source of the new connection
	 * @param giz2 the target of the new connection
	 * @return true if the connection was successfully created, otherwise false
	 * 
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	boolean connect(String giz1, String giz2) throws NoSuchItemException;
	/**
	 * Create a connection between a specific key-event and a Gizmo, further identifying
	 * whether the trigger is to occur upon a key-press or key-release
	 * @param keyCode the key number of the trigger source
	 * @param giz  the target of the connection
	 * @param keyPress if key-press pass in true, otherwise false
	 * @return true if the connection was successfully created, otherwise false
	 * 
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	boolean connect(int keyCode, String giz, boolean keyPress) throws NoSuchItemException;
	/**
	 * Attempt to disconnect giz1 and giz2
	 * @param giz1 the source of the connection to be severed
	 * @param giz2 the target of the connection to be severed
	 * @return true if the connection existed and was severed, otherwise false
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	boolean disconnect(String giz1, String giz2) throws NoSuchItemException;
	/**
	 * Attempt to disconnect a specific key-event from a specific gizmo
	 * @param keyCode the source key of the connection to be severed
	 * @param giz the target of the connection to be severed
	 * @param keyPress if key-press pass in true and was severed, otherwise false
	 * @return true if the connection existed and was severed, otherwise false
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	boolean disconnect(int keyCode, String giz, boolean keyPress) throws NoSuchItemException;
	/**
	 * Attempt to disconnect all connections relating to gizmo.
	 * @param gizmo the gizmo to cut all connections to and from
	 * @return true if connections were severed, otherwise false
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	void disconnect(String giz) throws NoSuchItemException;
	/**
	 * Miscellaneous operations
	 */
	/**
	 * Set the value of gravitational field strength to be employed by the game-board
	 * @param g a double corresponding to the gravitational field strength
	 */
	void setGravity(final double g);
	/**
	 * Set the value of friction in both the x and y directions
	 * @param mu a double corresponding to the desired x friction value
	 * @param mu2 a double corresponding to the desired x friction value
	 */
	void setFriction(final double mu, final double mu2);
	/**
	 * Create a save-string which precisely describes the contents of the board,
	 * the connections established upon the board and the value of specific physics
	 * parameters
	 * @return the String describing the board to be written to file
	 */
	String saveBoard();
	/**
	 * Completely wipe the current game-board.  This eliminates all Gizmos,
	 * GizmoBalls, connections and specific physics settings.
	 */
	void clearBoard();
	
	/**
	 * The solitary physics command available to the game-board.
	 * Advances the state of the physics of all board items by a single
	 * tick.
	 */
	void tick();
	/**
	 * Inform observers that the instance if IGameBoard has been altered in some
	 * manner
	 */
	void fireNotification();
	/**
	 * Query the game-board on whether a Gizmo is present at a specific location
	 * @param x the x co-ordinate to check for a Gizmo
	 * @param y the y co-ordinate to check for a Gizmo
	 * @return true if a Gizmo is present at that location, otherwise false
	 */
	boolean hasGizmoAt(int x, int y);
	/**
	 * Query the game-board on whether a GizmoBall is present at a specific location
	 * @param x the x co-ordinate to check for a Gizmo
	 * @param y the y co-ordinate to check for a Gizmo
	 * @return true if a GizmoBall is present at that location, otehrwise false
	 */
	boolean hasGizmoBallAt(double x, double y);
	/**
	 * Query the model for the name of a Gizmo located at a specific location.
	 * @param x the x co-ordinate to check for a Gizmo
	 * @param y the y co-ordinate to check for a Gizmo
	 * @return the name of the Gizmo if one is located at said location, otherwise 
	 * 			a NoSuchItemException is thrown
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	String gizmoNameAt(final int x, final int y) throws NoSuchItemException;
	/**
	 * Query the model for the name of a GizmoBall located at a specific location.
	 * @param x the x co-ordinate to check for a GizmoBall
	 * @param y the y co-ordinate to check for a GizmoBall
	 * @return the name of the Gizmo if one is located at said location, otherwise 
	 * 			a NoSuchItemException is thrown
	 * @throws NoSuchItemException {@link NoSuchItemException}
	 */
	String gizmoBallNameAt(final double x, final double y) throws NoSuchItemException;
	/**
	 * Query whether the model possesses a Gizmo with a particular name.
	 * @param name the identifier to query the game-board about
	 * @return true if the game-board possesses such a named item, otherwise false
	 */
	boolean hasGizmo(String name);
	/**
	 * Query whether the model possesses a GizmoBall with a particular name.
	 * @param name the identifier to query the game-board about
	 * @return true if the game-board possesses such a named item, otherwise false
	 */
	boolean hasGizmoBall(String name);
	/**
	 * Query the game-board for  a Map of Gizmo names to their desired renderings
	 * @return a Map of Gizmo type identifiers to their respective geometries
	 */
	Map<String, IShape> getGizmoLooks();
	/**
	 * Query the game-board for a Map of GizmoBall names to their desired renderings
	 * @return a Map of GizmoBall type identifiers to their respective geometries
	 */
	Map<String, IShape> getGizmoBallLooks();
	/**
	 * Fetch an {@link IDimension} for a specific type of item
	 * @param type the type of Gizmo to retrieve a dimension reference for
	 * @return the {@link IDimension} for that specific type
	 * @throws NoSuchItemException {@link NoSuchItemException)
	 */
	IDimension getGizmoDimensions(String type) throws NoSuchItemException;
	/**
	 * Fetch an {@link IDimension} for a specific type of Gizmo
	 * @param name the identifier of the Gizmo
	 * @return the {@link IDimension} for that specific Gizmo
	 * @throws NoSuchItemException {@link NoSuchItemException)
	 */
	IDimension getGizmoDimensionByName(String name) throws NoSuchItemException;
	/**
	 * Fetch an {@link IDimension} for a specific type of GizmoBall
	 * @param name the identifier of the GizmoBall
	 * @return the {@link IDimension} for that specific GizmoBall
	 * @throws NoSuchItemException {@link NoSuchItemException)
	 */
	IDimension getGizmoBallDimensionByName(String name) throws NoSuchItemException;
}