package exceptions;
/**
 * An exception encompassing a range of exceptions thrown via the
 * construction methods in {@link IFactory}.
 * Indicates that an exceptions was thrown while attempting to
 * construct an item using the factory.
 */
public class ManufactureException extends Exception {

	private static final long serialVersionUID = -8779568224596337434L;

	public ManufactureException(String message) {
		super(message);
	}

}
