package exceptions;
/**
 * An exception thrown to indicate the attempted placement of
 * the item would have resulted in it being placed outwith the
 * bounds of the playing area.
 */
public class OutOfBoundsException extends Exception {
	private static final long serialVersionUID = 3694003347393222254L;

	public OutOfBoundsException(String message) {
		super(message);
	}

}
