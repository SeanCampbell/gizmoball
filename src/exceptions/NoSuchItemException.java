package exceptions;
/**
 * An exception thrown to indicate there is no such item within
 * the model.  Crucial since {@link IGameBoard} employs Strings to
 * reference {@link IGizmo} and {@link IGizmoBall}.
 */
public class NoSuchItemException extends Exception {

	private static final long serialVersionUID = -3126371128979137917L;

	public NoSuchItemException(String message) {
		super(message);
	}

}
