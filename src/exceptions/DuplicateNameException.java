package exceptions;
/**
 * An exception thrown to indicate that a board item of the
 * same name already exists within {@link IGameBoard}.
 */
public class DuplicateNameException extends Exception {

	private static final long serialVersionUID = -7177647679349416602L;
	
	public DuplicateNameException(String message) {
		super(message);
	}

}
