package exceptions;
/**
 * An exception thrown to indicate the addition of the item
 * would have resulted in an overlap with another item occurring.
 */
public class OverlappingItemException extends Exception {
	
	private static final long serialVersionUID = -2886894941343838765L;

	public OverlappingItemException(String message) {
		super(message);
	}

}
