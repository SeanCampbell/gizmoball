package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import observer.IMessage;
import observer.IObserver;
import model.IGameBoard;
import controller.ContextSwitchListener;

public class RunView implements IObserver{
	
	private JFrame frame;
	private Container contentPane;
	private static final String BACKGROUND_COLOR = "#D3D3D3";
	//private static final String TOP_BACKGROUND_COLOR = "0xA9A9A9";
	private GameBoardView gameBoardView;
	private List<JButton> buttons;
	
	private ContextSwitchListener csl;
	
	public RunView(IGameBoard model) {
		model.addObserver(this);
		buildGUI();
	}
	
	private void buildGUI() {

		// main frame
		frame = new JFrame("Run Mode");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setFocusable(true);
		
		contentPane = frame.getContentPane();
		JPanel basePanel = new JPanel(new BorderLayout(10,10));
		basePanel.setBackground(Color.decode(BACKGROUND_COLOR));
		
		// create top panel
		TopPanel topPanel = new TopPanel();
		// buttons for top panel
		JButton buildModeButton = topPanel.createButton("Build");
		buildModeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				csl.switchState();
				frame.dispose();
				
			}
		});
		topPanel.add(buildModeButton,BorderLayout.LINE_START);
		
		JButton quitButton = topPanel.createButton("Quit");
		quitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int n = JOptionPane.showConfirmDialog(
					    frame,
					    "Are you sure you want to quit?",
					    "Quit",
					    JOptionPane.YES_NO_OPTION);
				if(n==JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		} );
		topPanel.add(quitButton,BorderLayout.LINE_END);
		
		basePanel.add(topPanel, BorderLayout.PAGE_START);

		
		// create the board view
		gameBoardView = new GameBoardView(false);
		basePanel.add(gameBoardView, BorderLayout.LINE_END);
		
		
		// create side panel
		GridLayoutPanel sidePanel = new GridLayoutPanel(gameBoardView.getPreferredSize().height, 3, 1);
		
		this.buttons = new ArrayList<>();
		// buttons for side panel
		JButton playButton = sidePanel.createButton("Play");
		this.buttons.add(playButton);
		sidePanel.add(playButton);
		
		JButton pauseButton = sidePanel.createButton("Pause");
		this.buttons.add(pauseButton);
		sidePanel.add(pauseButton);
		
		JButton tickButton = sidePanel.createButton("Tick");
		this.buttons.add(tickButton);
		sidePanel.add(tickButton);
		
		basePanel.add(sidePanel, BorderLayout.LINE_START);


		BottomPanel bottomPanel = new BottomPanel();
		basePanel.add(bottomPanel, BorderLayout.AFTER_LAST_LINE);
		
		contentPane.add(basePanel);
		frame.pack();
		frame.setVisible(true);
	}
	
	public void addKeyListener(KeyListener keyListener) {
		frame.addKeyListener(keyListener);
	}
	
	public void addContextSwitchListener(ContextSwitchListener listener) {
		csl = listener;
	}
	
	public void addButtonListener(ActionListener listener){
		for(JButton but : this.buttons)
			but.addActionListener(listener);
	}

	@Override
	public void update(IMessage msg) {
		gameBoardView.reset();
		gameBoardView.setGizmos(msg);
		gameBoardView.repaint();
		
	}
	
	public void addPlayListener(){
		
	}
	
	public void setFocus() {
		frame.requestFocusInWindow();
	}
}
