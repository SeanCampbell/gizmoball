package view;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import model.GameBoard;
import model.GizmoFactory;
import model.IGameBoard;
import controller.ContextSwitchListener;
import controller.StateController;

public class ViewMain {

	@SuppressWarnings("unused")
	public static void main(String[] args){
		try {
            // Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } 
	    catch (UnsupportedLookAndFeelException e) {
	       // handle exception
	    }
	    catch (ClassNotFoundException e) {
	       // handle exception
	    }
	    catch (InstantiationException e) {
	       // handle exception
	    }
	    catch (IllegalAccessException e) {
	       // handle exception
	    }
		
		IGameBoard board = new GameBoard(GizmoFactory.getInstance());
		ContextSwitchListener csl = new StateController(board);
	}

}
