package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class TopPanel extends JPanel {

	private static final long serialVersionUID = 2699867398337249080L;
	private static final String BACKGROUND_COLOR = "#D3D3D3";
	private static final String TOP_BACKGROUND_COLOR = "0xA9A9A9";
			
	public TopPanel() {
		this.setLayout(new FlowLayout(FlowLayout.CENTER, 15, 0));
		this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		this.setBackground(Color.decode(TOP_BACKGROUND_COLOR));
	}
	
	public JButton createButton(String name) {
		JButton button = new JButton(name);
		Border buttonBorder = BorderFactory.createRaisedBevelBorder();
		button.setBorder(buttonBorder);
		button.setPreferredSize(new Dimension(70, 30));
		button.setBackground(Color.decode(BACKGROUND_COLOR));
		
		return button;
	}
}
