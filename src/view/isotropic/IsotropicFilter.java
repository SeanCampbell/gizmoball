package view.isotropic;

import java.awt.Point;

import shapes.LogicalPoint;


public class IsotropicFilter {

	private int physicalWidth = 20, physicalHeight = 20;
	private double logicalWidth = 20.0, logicalHeight = 20.0;
	private double pixelScale = 1.0;
	
	/**
	 * Create an Isotropic Filter for the desired coordinate systems.
	 * 
	 * @param pWidth The width of the physical view.
	 * @param pHeight The height of the the physical view.
	 * @param lWidth Width of the logical space to use.
	 * @param lHeight Height of the logical space to use.
	 */
	public IsotropicFilter(int pWidth, int pHeight, double lWidth, double lHeight) {
		physicalWidth = pWidth;
		physicalHeight = pHeight;
		logicalWidth = lWidth;
		logicalHeight = lHeight;
		
		// calculate the scale factor
		pixelScale = Math.max(logicalWidth/physicalWidth, logicalHeight/physicalHeight);
	}
	
	/**
	 * Convert a logical space co-ordinate to a physical space.
	 * @param logical a point in the logical space.
	 * @return corresponding physical space point.
	 */
	public Point getPhysical(LogicalPoint logical) {
		int x = (int)Math.round(logical.getX() / pixelScale);
		int y = (int)Math.round(logical.getY() / pixelScale);
		
		return new Point(x, y);
	}
	
	/**
	 * Convert a logical space co-ordinate to a physical space. Wrapper around {@link #getPhysical(LogicalPoint logical)}.
	 * @param x x co-ordinate of the logical point.
	 * @param y y co-ordinate of the logical point.
	 * @return corresponding physical space point.
	 */
	public Point getPhysical(double x, double y) {
		LogicalPoint logical = new LogicalPoint(x, y);
		return getPhysical(logical);
	}
	
	/**
	 * Convert a physical memory location to a logical point.
	 * @param pt the physical space point.
	 * @return corresponding logical space point.
	 */
	public LogicalPoint getLogical(Point pt) {
		double x = pt.getX() * pixelScale;
		double y = pt.getY() * pixelScale;
		
		return new LogicalPoint(x, y);
	}
	
	/**
	 * set the size of the physical space.
	 * @param width new width of the physical space.
	 * @param height new height of the physical space.
	 */
	public void setPhysicalSize(int width, int height) {
		physicalWidth = width;
		physicalHeight = height;
		
		// calculate the scale factor
		pixelScale = Math.max(logicalWidth/physicalWidth, logicalHeight/physicalHeight);
	}
}
