package view;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import controller.buildmode.BuildSettingsAction;
import model.IGameBoard;

public class BuildMenu extends JMenuBar {
	private static final long serialVersionUID = -8105825457465833810L;
	private BuildSettingsAction bsa;
	
	public BuildMenu(IGameBoard model) {
		bsa = new BuildSettingsAction(model);
		
		createMenuBar();
	}

	private void createMenuBar() {
		JMenu settings = new JMenu("Settings");
		this.createMenuItem("Clear Board", settings);
		this.createMenuItem("Set Gravity", settings);
		this.createMenuItem("Set Friction", settings);
		
		this.add(settings);
	}
	
	private void createMenuItem(String menuItemName, JMenu menu) {
		JMenuItem menuItem = new JMenuItem(menuItemName);
		menuItem.addActionListener(bsa);
		menu.add(menuItem);
	}

}
