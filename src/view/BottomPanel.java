package view;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class BottomPanel extends JPanel {

	private static final long serialVersionUID = 4967909229732805804L;
	private static final String BACKGROUND_COLOR = "#D3D3D3";
	//private static final String TOP_BACKGROUND_COLOR = "0xA9A9A9";

	public BottomPanel() {
		this.setLayout(new FlowLayout(FlowLayout.CENTER));
		this.setBorder(BorderFactory.createEmptyBorder(10, 10, 20, 40));
		this.setBackground(Color.decode(BACKGROUND_COLOR));
		//this.setPreferredSize(new Dimension(ImageObserver.WIDTH, 150));
	}
}
