package view;

import java.awt.Graphics;
import java.awt.Point;

import shapes.LogicalPoint;
import view.isotropic.IsotropicFilter;

public class GizmoCircle implements GizmoGeometry {

	private LogicalPoint cornerPt;
	private LogicalPoint endPt;
	
	/**
	 * Create a renderable circle for gizmos.
	 * @param centerX the centre x of the circle.
	 * @param centerY the centre y of the circle.
	 * @param radius the radius of the circle.
	 */
	public GizmoCircle(LogicalPoint pt, double radius) {
		double x = pt.getX()-radius;
		double y = pt.getY()-radius;
		this.cornerPt = new LogicalPoint(x, y);
		this.endPt = new LogicalPoint(pt.getX()+radius, pt.getY()+radius);
	}
	
	@Override
	public void draw(Graphics g, IsotropicFilter filter) {
		Point pt = filter.getPhysical(cornerPt);
		Point pEnd = filter.getPhysical(endPt);
		g.fillOval((int)pt.getX(), (int)pt.getY(), (int)(pEnd.getX() - pt.getX()), (int)(pEnd.getY() - pt.getY()));
	}

	@Override
	public void move(double dx, double dy) {
		cornerPt = new LogicalPoint(cornerPt.getX()+dx, cornerPt.getY()+dy);
		endPt = new LogicalPoint(endPt.getX()+dx, endPt.getY()+dy);
	}

}