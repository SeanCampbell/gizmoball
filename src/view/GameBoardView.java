package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

import controller.DragEventListener;
import controller.GridComponentListener;
import controller.GridMouseListener;
import model.IShape;
import model.Pair;
import observer.IMessage;
import shapes.LogicalPoint;
import view.isotropic.IsotropicFilter;

public class GameBoardView extends JPanel implements DragEventListener, ComponentListener {
	private static final long serialVersionUID = 1154375957942585765L;
	private static final int ROWS = 20;
	private static final int COLUMNS = 20;
	private boolean hasGrid = false;
	private List<Pair<String, GizmoShape>> gizmoList;
	private IsotropicFilter isoFilter;
	private GridMouseListener gridListener;
	private LogicalPoint dragStart;
	private List<GizmoShape> overlays;
	private GizmoShape dragOverlay;
	private GizmoShape selectionOverlay;
	
	public GameBoardView(boolean hasGrid) {
		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(ROWS * 20, COLUMNS * 20));
		this.hasGrid = hasGrid;
		gizmoList = new ArrayList<>();
		
		// create isotropic filter
		isoFilter = new IsotropicFilter(getWidth(), getHeight(), 20.0, 20.0);
		
		// add as component listener to self (for size changes)
		addComponentListener(this);
		
		// allocate for overlay
		overlays = new LinkedList<>();
		
		// create our selection overlay and add it to the list of overlay
		selectionOverlay = new GizmoShape();
		overlays.add(selectionOverlay);
	}

	@Override
	public void paint(Graphics g) {
		// set it to draw anti aliased
		Graphics2D g2d = (Graphics2D)g;
		g2d.setRenderingHint(
			    RenderingHints.KEY_ANTIALIASING,
			    RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.setRenderingHint(
			    RenderingHints.KEY_TEXT_ANTIALIASING,
			    RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			
		// clear
		g.clearRect(0, 0, getWidth(), getHeight());
		
		// draw background
		Point p1 = isoFilter.getPhysical(0, 0);
		Point p2 = isoFilter.getPhysical(20, 20);
		g.setColor(getBackground());		
		g.fillRect((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY());

		if(hasGrid) {
			// draw grid
			drawGrid(g);
		}
		
		// draw gizmos
		for(Pair<String, GizmoShape> gizmo : gizmoList) {
			gizmo.getB().draw(g, isoFilter);
		}
		
		// draw any overlays
		for(GizmoShape geo : overlays) {
			geo.draw(g, isoFilter);
		}
	}

	protected void reset() {
		gizmoList.clear();
	}
	
	protected void setGizmos(IMessage msg) {
		for(int i = 0; i < msg.size(); i++) {
			Pair<String, IShape> gizmo = msg.get(i);
			GizmoShape gizmo_shape = GizmoShapeBuilder.convertShape(gizmo.getB());
			
			gizmoList.add(new Pair<>(gizmo.getA(), gizmo_shape));
		}
	}

	private void drawGrid(Graphics g) {
		// set colour
		g.setColor(Color.WHITE);

		// first draw a border
		Point p1 = isoFilter.getPhysical(0, 0);
		Point p2 = isoFilter.getPhysical(20, 20);
		g.drawRect((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY());

		// draw rows
		for(int i = 0; i < ROWS; i++) {
			p1 = isoFilter.getPhysical(0, i);
			p2 = isoFilter.getPhysical(20, i);
			g.drawLine((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY());
		}

		// draw columns
		for(int i = 0; i < COLUMNS; i++) {
			p1 = isoFilter.getPhysical(i, 0);
			p2 = isoFilter.getPhysical(i, 20);
			g.drawLine((int)p1.getX(), (int)p1.getY(), (int)p2.getX(), (int)p2.getY());
		}
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// do nothing
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// do nothing
	}

	@Override
	public void componentResized(ComponentEvent e) {
		isoFilter.setPhysicalSize(getWidth(), getHeight());
		repaint();
	}

	@Override
	public void componentShown(ComponentEvent e) {
		isoFilter.setPhysicalSize(getWidth(), getHeight());
		repaint();
	}

	public void setDragMode(boolean enabled) {
		gridListener.setDragMode(enabled);
	}
	
	public void addGridComponentListener(GridComponentListener listener) {
		// create mouse listener for the grid
		gridListener = new GridMouseListener(listener, isoFilter);
		gridListener.setDragEventListener(this);
		addMouseListener(gridListener);
		addMouseMotionListener(gridListener);
	}

	@Override
	public void onDragBegin(LogicalPoint pt) {
		dragStart = pt;
		dragOverlay = new GizmoShape();
		
		dragOverlay.clear();
		dragOverlay.add(createGridRectangle(dragStart, pt));
		dragOverlay.setColour(new Color(0.0f, 1.0f, 0.5f, .55f));
		overlays.add(dragOverlay);
		repaint();
	}

	@Override
	public void onDragEnd(LogicalPoint pt) {
		// TODO Auto-generated method stub
		overlays.remove(dragOverlay);
		repaint();
	}

	@Override
	public void onDragUpdate(LogicalPoint pt) {
		dragOverlay.clear();
		dragOverlay.add(createGridRectangle(dragStart, pt));
		dragOverlay.setColour(new Color(0.0f, 1.0f, 0.5f, .55f));
		repaint();
	}
	
	private GizmoPolygon createGridRectangle(LogicalPoint start, LogicalPoint end) {
		GizmoPolygon poly = new GizmoPolygon();
		
		// we need to align to the board
		double sx = Math.floor(Math.min(start.getX(), end.getX()));
		double sy = Math.floor(Math.min(start.getY(), end.getY()));
		double ex = Math.min(Math.floor(Math.max(start.getX(), end.getX()))+1, ROWS);
		double ey = Math.min(Math.floor(Math.max(start.getY(), end.getY()))+1, COLUMNS);
	
		poly.addPoint(new LogicalPoint(sx, sy));
		poly.addPoint(new LogicalPoint(sx, ey));
		poly.addPoint(new LogicalPoint(ex, ey));
		poly.addPoint(new LogicalPoint(ex, sy));
		
		return poly;
	}

	public void setSelected(double x, double y, double width, double height) {
		selectionOverlay.setColour(new Color(.88f, .0f, .88f, .75f));
		
		double thickness = Math.max(0.1, 0.125 * Math.min(width, height));
		double length = 0.35 * Math.min(width, height);
		
		System.out.println("w: " + width +" " + "h:" + height);
		System.out.println("thickness: " + thickness);
		GizmoPolygon corner1 = new GizmoPolygon();
		corner1.addPoint(new LogicalPoint(x, y));
		corner1.addPoint(new LogicalPoint(x+length, y));
		corner1.addPoint(new LogicalPoint(x+length, y+thickness));
		corner1.addPoint(new LogicalPoint(x+thickness, y+thickness));
		corner1.addPoint(new LogicalPoint(x+thickness, y+length));
		corner1.addPoint(new LogicalPoint(x, y+length));

		GizmoPolygon corner2 = new GizmoPolygon();
		corner2.addPoint(new LogicalPoint(x+width, y));
		corner2.addPoint(new LogicalPoint(x+width-length, y));
		corner2.addPoint(new LogicalPoint(x+width-length, y+thickness));
		corner2.addPoint(new LogicalPoint(x+width-thickness, y+thickness));
		corner2.addPoint(new LogicalPoint(x+width-thickness, y+length));
		corner2.addPoint(new LogicalPoint(x+width, y+length));
		
		GizmoPolygon corner3 = new GizmoPolygon();
		corner3.addPoint(new LogicalPoint(x+width, y+height));
		corner3.addPoint(new LogicalPoint(x+width-length, y+height));
		corner3.addPoint(new LogicalPoint(x+width-length, y+height-thickness));
		corner3.addPoint(new LogicalPoint(x+width-thickness, y+height-thickness));
		corner3.addPoint(new LogicalPoint(x+width-thickness, y+height-length));
		corner3.addPoint(new LogicalPoint(x+width, y+height-length));
		
		GizmoPolygon corner4 = new GizmoPolygon();
		corner4.addPoint(new LogicalPoint(x, y+height));
		corner4.addPoint(new LogicalPoint(x+length, y+height));
		corner4.addPoint(new LogicalPoint(x+length, y+height-thickness));
		corner4.addPoint(new LogicalPoint(x+thickness, y+height-thickness));
		corner4.addPoint(new LogicalPoint(x+thickness, y+height-length));
		corner4.addPoint(new LogicalPoint(x, y+height-length));
		
		selectionOverlay.add(corner1);
		selectionOverlay.add(corner2);
		selectionOverlay.add(corner3);
		selectionOverlay.add(corner4);
		repaint();
	}

	public void removeSelection() {
		// clear selection
		selectionOverlay.clear();
		repaint();
	}
}
