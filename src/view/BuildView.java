package view;

import java.awt.Container;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import layout.SpringUtilities;
import model.IDimension;
import model.IGameBoard;
import model.IShape;
import observer.IMessage;
import observer.IObserver;
import controller.ContextSwitchListener;
import controller.GridComponentListener;
import exceptions.NoSuchItemException;

public class BuildView implements IObserver, IBuildView {
	
	private ContextSwitchListener csl;
	
	private JFrame frame;
	private GameBoardView gameBoardView;
	private GizmoPropertiesPanel gizmoPropertiesPanel;
	private Map<String, GizmoShape> gizmos;
	private List<GizmoButton> gizmoButtons;
	private List<JButton> gizmoModificationButtons;
	private BuildMenu menuBar;
	private GizmoCursor cursor;
	private JButton saveButton, loadButton;
	
	//private static final String BACKGROUND_COLOR = "#D3D3D3";
	//private static final String TOP_BACKGROUND_COLOR = "0xA9A9A9";
	
	public BuildView(IGameBoard model) {
		// create list to store all our gizmo things
		gizmoButtons = new LinkedList<>();
		gizmoModificationButtons = new LinkedList<>();
		gizmos = new HashMap<>();
		
		// add this as observer to model, then build GUI components
		model.addObserver(this);
		
		// lets create the buttons + components first
		buildGizmoComponents(model);
		buildGUI(model);
		
		// add cursor handler
		cursor = new GizmoCursor(frame);
	}
	
	@Override
	public void setModel(IGameBoard model) {
		// not much to be done really
		model.addObserver(this);
	}
	
	private void buildGizmoComponents(IGameBoard model) {
		Map<String, IShape> gizmos = model.getGizmoLooks();
		try{
			for (Map.Entry<String, IShape> gizmo : gizmos.entrySet()) {
				String type = gizmo.getKey();
				IDimension dimension = model.getGizmoDimensions(type);
			
				if (gizmo.getValue().getGeometryList().size() != 0) {
					GizmoShape shape = GizmoShapeBuilder.convertShape(gizmo.getValue());
					this.gizmos.put(type, shape);
					gizmoButtons.add(new GizmoButton(type, shape, dimension));
				}
			}
		} catch(NoSuchItemException e) {
			System.err.println("Error fetching dimension name.");
			e.printStackTrace();
		}
		
		Map<String, IShape> gizmoballs = model.getGizmoBallLooks();
		try{
			for (Map.Entry<String, IShape> ball : gizmoballs.entrySet()) {
				String type = ball.getKey();
				IDimension dimension = model.getGizmoDimensions(type);
			
				if (ball.getValue().getGeometryList().size() != 0) {
					GizmoShape shape = GizmoShapeBuilder.convertShape(ball.getValue());
					this.gizmos.put(type, shape);
					gizmoButtons.add(new GizmoBallButton(type, shape, dimension));
				}
			}
		} catch(NoSuchItemException e) {
			System.err.println("Error fetching dimension name.");
			e.printStackTrace();
		}
	}
	
	private void buildGUI(IGameBoard model) {
		// main frame
		frame = new JFrame("Build Mode");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container contentPane = frame.getContentPane();
		
		menuBar = new BuildMenu(model);
		frame.setJMenuBar(menuBar);
		
		// use a spring
		contentPane.setLayout(new SpringLayout());
		
		JPanel top = buildTopPanel();
		JPanel mid = new JPanel(new SpringLayout());
		
		// create panel for gizmo buttons
		JPanel gizmoBtnPanel = buildGizmoButtonPanel();
		
		mid.add(gizmoBtnPanel);		
		// create the board view
		gameBoardView = new GameBoardView(true);
		mid.add(gameBoardView);
		
		JPanel bot = buildBotPanel();
		
		// use spring utilities to make a grid
		//Lay out the panel.
		SpringUtilities.makeCompactGrid(mid,
		                                1, 2, //rows, cols
		                                6, 6,        //initX, initY
		                                6, 6);       //xPad, yPad	
		
		contentPane.add(top);
		contentPane.add(mid);
		contentPane.add(bot);

		//Lay out the panel.
		SpringUtilities.makeCompactGrid(contentPane,
		                                3, 1, //rows, cols
		                                6, 6,        //initX, initY
		                                6, 6);       //xPad, yPad	
		frame.pack();
		frame.setVisible(true);
	}
	
	private JPanel buildBotPanel() {
		// Create bottom panel
		BottomPanel bottomPanel = new BottomPanel();
		
		gizmoPropertiesPanel = new GizmoPropertiesPanel();

		// buttons for the gizmo properties
		JButton moveButton = gizmoPropertiesPanel.createButton("Move", "MOVEGIZMO");
		gizmoPropertiesPanel.addButton(moveButton);
		gizmoModificationButtons.add(moveButton);
		
		JButton rotateButton = gizmoPropertiesPanel.createButton("Rotate", "ROTATEGIZMO");
		gizmoPropertiesPanel.addButton(rotateButton);
		gizmoModificationButtons.add(rotateButton);
		
		JButton deleteButton = gizmoPropertiesPanel.createButton("Delete", "DELETEGIZMO");
		gizmoPropertiesPanel.addButton(deleteButton);
		gizmoModificationButtons.add(deleteButton);
		
		JButton connectButton = gizmoPropertiesPanel.createButton("Connect", "CONNECTTRIGGER");
		gizmoPropertiesPanel.addButton(connectButton);
		gizmoModificationButtons.add(connectButton);
		
		JButton keyConnectButton = gizmoPropertiesPanel.createButton("Key-Connect", "CONNECTKEYTRIGGER");
		gizmoPropertiesPanel.addButton(keyConnectButton);
		gizmoModificationButtons.add(keyConnectButton);
		
		JButton disconnectButton = gizmoPropertiesPanel.createButton("Disconnect-All", "DISCONNECTTRIGGER");
		gizmoPropertiesPanel.addButton(disconnectButton);
		gizmoModificationButtons.add(disconnectButton);
		
		/*JButton keyDisconnectButton = gizmoPropertiesPanel.createButton("Key-Disconnect", "DISCONNECTKEYTRIGGER");
		gizmoPropertiesPanel.addButton(keyDisconnectButton);
		gizmoModificationButtons.add(keyDisconnectButton);*/
		
		bottomPanel.add(gizmoPropertiesPanel);
		return gizmoPropertiesPanel;
	}

	private JPanel buildTopPanel() {
		// create top panel
		TopPanel topPanel = new TopPanel();
		
		// buttons for top panel
		JButton runModeButton = topPanel.createButton("Run");
		runModeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				csl.switchState();
				frame.dispose();
			}
		});
		topPanel.add(runModeButton);
				
		saveButton = topPanel.createButton("Save");
		saveButton.setActionCommand("SAVEGAME");
		topPanel.add(saveButton);
		
		loadButton = topPanel.createButton("Load");
		loadButton.setActionCommand("LOADGAME");
		topPanel.add(loadButton);
		
		JButton quitButton = topPanel.createButton("Quit");
		quitButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int n = JOptionPane.showConfirmDialog(
					    frame,
					    "Are you sure you want to quit?",
					    "Quit",
					    JOptionPane.YES_NO_OPTION);
				if(n==JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		} );
		
		topPanel.add(quitButton);
		return topPanel;
	}

	private JPanel buildGizmoButtonPanel() {
		// create spring layout panel
		JPanel panel = new JPanel(new SpringLayout());
		JPanel col1 = new JPanel(new SpringLayout());
		JPanel col2 = new JPanel(new SpringLayout());
		
		for (int i = 0; i < gizmoButtons.size(); i++) {
			if (i % 2 == 0) {
				col1.add(gizmoButtons.get(i));
			}
			else {
				col2.add(gizmoButtons.get(i));
			}
		}
		
		// use spring utilities to make a grid
		//Lay out the panel.
		SpringUtilities.makeGrid(col1,
						col1.getComponentCount(), 1, //rows, cols
		                                3, 6,        //initX, initY
		                                3, 6);       //xPad, yPad
		//Lay out the panel.
		SpringUtilities.makeGrid(col2,
		                                col2.getComponentCount(), 1, //rows, cols
		                                3, 6,        //initX, initY
		                                3, 6);       //xPad, yPad
		
		panel.add(col1);
		panel.add(col2);
		//Lay out the panel.
		SpringUtilities.makeCompactGrid(panel,
		                                1, 2, //rows, cols
		                                6, 6,        //initX, initY
		                                6, 6);       //xPad, yPad
		return panel;
	}

	public void addContextSwitchListener(ContextSwitchListener listener) {
		csl = listener;
	}
	
	@Override
	public void update(IMessage msg) {
		// reset the gameboard and add the gizmos
		gameBoardView.reset();
		gameBoardView.setGizmos(msg);
		gameBoardView.repaint();
	}

	@Override
	public void addGizmoButtonListener(ActionListener listener) {
		for(GizmoButton btn : gizmoButtons) {
			btn.addActionListener(listener);
		}
	}

	public void addGridComponentListener(GridComponentListener listener) {
		gameBoardView.addGridComponentListener(listener);
	}

	@Override
	public void setDragMode(boolean enabled) {
		gameBoardView.setDragMode(enabled);
	}

	@Override
	public void setShowModificationButtons(boolean enabled) {
		if (enabled) {
			gizmoPropertiesPanel.showButtons();
		}
		else {
			gizmoPropertiesPanel.showText();
		}
	}

	@Override
	public void setInformationText(String text) {
		gizmoPropertiesPanel.writeMessage(text);
	}

	@Override
	public void addGizmoModificationButtonListener(ActionListener listener) {
		for(JButton button : gizmoModificationButtons) {
			button.addActionListener(listener);
		}
	}

	@Override
	public void setGizmoCursor(String type) {
		if (type == null) {
			// empty string, set default
			frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
		else {
			// get the gizmo shape
			cursor.setCursor(gizmos.get(type));
		}
	}

	@Override
	public void setLoadSaveButtonListener(ActionListener listener) {
		saveButton.addActionListener(listener);
		loadButton.addActionListener(listener);
	}

	@Override
	public String getLoadFile() {
		File file = FileSelection.openFile(frame);
		
		if (file != null) {
			return file.getPath();
		}
		
		return null;
	}
	
	@Override
	public File getSaveFile(){
		File file = FileSelection.saveFile(frame);
		if(file != null) {
			return file;
		}
		return null;
	}

	@Override
	public void showErrorDialog(String error) {
		// just use a standard message dialog
		JOptionPane.showMessageDialog(frame, error, "Error", JOptionPane.ERROR_MESSAGE);
	}

	@Override
	public void setSelected(double x, double y, double width, double height) {
		gameBoardView.setSelected(x, y, width, height);
	}

	@Override
	public void removeSelection() {
		gameBoardView.removeSelection();
	}

	@Override
	public String getKeyTriggerType() {
		String[] options =  {	"Key pressed",
								"Key release",
								"Both"
	        				};
            
		int resp = JOptionPane.showOptionDialog(frame, 
				"Which type of key-trigger would you like?", 
				"Key Trigger", 
				JOptionPane.YES_NO_CANCEL_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				options,
				options[2]);

		// check for cancel
		if (resp < 0) {
			// return null on this case
			return null;
		}
		
		String[] responses = { "PRESS", "RELEASE", "BOTH" };
		
		// else we return a valid number
		return responses[resp];
	}

	@Override
	public int getKeyBind() {
		KeyBindDialog key = new KeyBindDialog(frame);
		key.setVisible(true);
		return key.getInputtedKey();
	}
}
