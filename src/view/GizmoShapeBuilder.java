package view;

import model.IShape;
import shapes.ICircle;
import shapes.IGeometry;
import shapes.IPolygon;
import shapes.LogicalPoint;

public class GizmoShapeBuilder {
	
	static GizmoShape convertScaledShape(IShape shape, double x, double y) {
		GizmoShape gizmoShape = new GizmoShape();
		gizmoShape.setColour(shape.getColor());
		LogicalPoint maxExtent = new LogicalPoint(0, 0);
		
		for(IGeometry geo : shape.getGeometryList()) {
			// convert to gizmo geometries
			GizmoGeometry gizmoGeometry = null;
			
			if (geo.getCircle() != null) {
				// convert a circle
				gizmoGeometry = convertCircle(geo.getCircle());
				LogicalPoint extent = getCircleExtent(geo.getCircle());

				maxExtent = getMaxExtent(maxExtent, extent);
			}
			else if (geo.getPolygon() != null) {
				// convert a polygon
				gizmoGeometry = convertPolygon(geo.getPolygon());
			
				// get the max extent
				LogicalPoint extent = getPolygonExtent(geo.getPolygon());
				maxExtent = getMaxExtent(maxExtent, extent);
			}
			else {
				assert false : "invalid shape. gizmoGeometry always null";
			}
			
			// add geometry to list
			gizmoShape.add(gizmoGeometry);
		}
		
		System.out.println("MAX: " + maxExtent.getX() + ", " + maxExtent.getY());
		
		// now move the shape as needed
		gizmoShape.move((x-maxExtent.getX())/2, (y-maxExtent.getY())/2);
		return gizmoShape;
	}
	
	private static LogicalPoint getMaxExtent(LogicalPoint pt1, LogicalPoint pt2) {
		return new LogicalPoint(Math.max(pt1.getX(), pt2.getX()), Math.max(pt1.getY(), pt2.getY()));
	}

	private static LogicalPoint getPolygonExtent(IPolygon polygon) {
		LogicalPoint extent = new LogicalPoint(0, 0);
		
		for(LogicalPoint pt : polygon.getPoints()) {
			extent = getMaxExtent(pt, extent);
		}
		
		return extent;
	}
	
	private static LogicalPoint getCircleExtent(ICircle circle) {
		LogicalPoint extent = new LogicalPoint(circle.getCentre().getX()+circle.getRadius(),
								circle.getCentre().getY()+circle.getRadius());
		
		return extent;
	}
	
	static GizmoShape convertShape(IShape shape) {
		GizmoShape gizmoShape = new GizmoShape();
		gizmoShape.setColour(shape.getColor());
		
		for(IGeometry geo : shape.getGeometryList()) {
			// convert to gizmo geometries
			GizmoGeometry gizmoGeometry = null;
			
			if (geo.getCircle() != null) {
				// convert a circle
				gizmoGeometry = convertCircle(geo.getCircle());
			}
			else if (geo.getPolygon() != null) {
				// convert a polygon
				gizmoGeometry = convertPolygon(geo.getPolygon());
			}
			else {
				assert false : "invalid shape. gizmoGeometry always null";
			}
			
			// add geometry to list
			gizmoShape.add(gizmoGeometry);
		}
		
		return gizmoShape;
	}

	private static GizmoGeometry convertPolygon(IPolygon polygon) {
		GizmoPolygon gizPoly = new GizmoPolygon();
		
		for(LogicalPoint pt : polygon.getPoints()) {
			// add point to polygon
			gizPoly.addPoint(pt);
		}
		
		// return the converted polygon
		return gizPoly;
	}

	private static GizmoGeometry convertCircle(ICircle circle) {
		return new GizmoCircle(circle.getCentre(), circle.getRadius());
	}
}
