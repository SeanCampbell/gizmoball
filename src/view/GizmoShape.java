package view;

import java.awt.Color;
import java.awt.Graphics;
import java.util.LinkedList;
import java.util.List;

import view.isotropic.IsotropicFilter;

public class GizmoShape {
	
	private Color colour;
	private List<GizmoGeometry> geometries;
	
	public GizmoShape() {
		// set default colour to red
		colour = Color.red;
		
		// initialise list
		geometries = new LinkedList<>();
	}
	
	public void setColour(Color colour) {
		this.colour = colour;
	}
	
	public void add(GizmoGeometry geo) {
		geometries.add(geo);
	}
	
	public void draw(Graphics g, IsotropicFilter filter) {
		// set colour for all geometries
		g.setColor(colour);
		
		// draw all geometries
		for(GizmoGeometry geo : geometries) {
			geo.draw(g, filter);
		}
	}

	public void clear() {
		geometries.clear();
		colour = Color.red;
	}

	public void move(double dx, double dy) {
		// draw all geometries
		for(GizmoGeometry geo : geometries) {
			geo.move(dx, dy);
		}
	}
	
	public List<GizmoGeometry> getGeometries() {
		return geometries;
	}
}