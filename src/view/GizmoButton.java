package view;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.border.Border;

import view.isotropic.IsotropicFilter;
import model.IDimension;
import controller.buildmode.AddGizmoAction;

public class GizmoButton extends JButton implements AddGizmoAction {

	private static final long serialVersionUID = 3565239341626760559L;
	private static final String TOP_BACKGROUND_COLOR = "0xA9A9A9";
	private String gizmoName;
	
	public GizmoButton(String type, GizmoShape shape, IDimension dimension) {
		gizmoName = type;
		System.out.println("SHAPE: " + type);

		double isomaxx = Math.max(2.0, dimension.getBoundaryInfo().getMaxx());
		double isomaxy = Math.max(2.0, dimension.getBoundaryInfo().getMaxy());
		
		// scale the shape according to the boundary
		shape.move((isomaxx - dimension.getBoundaryInfo().getMaxx())/2, (isomaxy - dimension.getBoundaryInfo().getMaxy()) / 2);
		
		
		// create icon
		BufferedImage img = new BufferedImage(50, 50, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = img.createGraphics();
		IsotropicFilter filter = new IsotropicFilter(50, 50, isomaxx, isomaxy);
		shape.draw(g, filter);
		setIcon(new ImageIcon(img));
		
		// remove the borders
		Border buttonBorder = BorderFactory.createRaisedBevelBorder();
		setBorder(buttonBorder);
		setBackground(Color.decode(TOP_BACKGROUND_COLOR));
	
		// set action command
		setActionCommand("ADDGIZMO");
		
		this.setToolTipText(type);
	}

	@Override
	public String getGizmoType() {
		return gizmoName;
	}
}
