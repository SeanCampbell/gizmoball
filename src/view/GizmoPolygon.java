package view;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Polygon;
import java.util.LinkedList;
import java.util.List;

import shapes.LogicalPoint;
import view.isotropic.IsotropicFilter;

public class GizmoPolygon implements GizmoGeometry {

	List<LogicalPoint> points;
	
	public GizmoPolygon() {
		points = new LinkedList<LogicalPoint>();
	}
	
	/**
	 * Add a point to the polygon
	 * @param x the x co-ordinate of the point to add
	 * @param y the y co-ordinate of the point to add
	 */
	public void addPoint(LogicalPoint pt) {
		points.add(pt);
	}
	
	@Override
	public void draw(Graphics g, IsotropicFilter filter) {
		Polygon polygon = new Polygon();
		
		// create the polygon
		for(LogicalPoint pt : points) {
			Point physicalPt = filter.getPhysical(pt);
			polygon.addPoint((int)physicalPt.getX(), (int)physicalPt.getY());
		}
		
		// draw the polygon
		g.fillPolygon(polygon);
	}

	@Override
	public void move(double dx, double dy) {
		for(int i = 0; i < points.size(); i++) {
			LogicalPoint pt = points.remove(i);
			points.add(i, new LogicalPoint(pt.getX()+dx, pt.getY()+dy));
		}
	}
}