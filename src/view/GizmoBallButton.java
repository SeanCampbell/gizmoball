package view;

import model.IDimension;

public class GizmoBallButton extends GizmoButton {

	private static final long serialVersionUID = 6966446099220407618L;

	public GizmoBallButton(String type, GizmoShape shape_, IDimension dimension) {
		super(type, shape_, dimension);
		
		// now all we want to do is set the action command
		this.setActionCommand("ADDGIZMOBALL");
	}

}
