package view;

import java.util.Hashtable;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;

import model.IGameBoard;
import controller.buildmode.SetFrictionAction;

public class FrictionSliderBar {
	private JSlider slider, slider2;
	private SetFrictionAction sfa1, sfa2;
	
	public FrictionSliderBar(IGameBoard model) {
		slider = createSlider();
		slider2 = createSlider();
		sfa1 = new SetFrictionAction();
		sfa2 = new SetFrictionAction();
		slider.addChangeListener(sfa1);
		slider2.addChangeListener(sfa2);
		JLabel l1 = new JLabel("MU");
		JLabel space = new JLabel(" ");
		JLabel l2 = new JLabel("MU2");
		Object[] message = new Object[] {"Select a values for mu and mu2: ", l1, slider, space, l2, slider2};
		int s = JOptionPane.showConfirmDialog(null, message, "Friction", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(s==JOptionPane.OK_OPTION) {
			model.setFriction(sfa1.getValue(), sfa2.getValue());
		}
	}
	
	private JSlider createSlider() {
		JSlider s = new JSlider();
		s.setMajorTickSpacing(4);
		s.setMinorTickSpacing(1);
		s.setMaximum(20);
		Hashtable<Integer, JLabel> labelTable = new Hashtable<>();
		labelTable.put( new Integer( 0 ), new JLabel("0.0") );
		labelTable.put( new Integer( 4 ), new JLabel("0.02") );
		labelTable.put( new Integer( 8 ), new JLabel("0.04") );
		labelTable.put( new Integer( 12 ), new JLabel("0.06") );
		labelTable.put( new Integer( 16 ), new JLabel("0.08") );
		labelTable.put( new Integer( 20 ), new JLabel("0.1") );
		s.setLabelTable( labelTable );
		s.setValue(5);
		s.setPaintTicks(true);
		s.setPaintLabels(true);
		
		return s;
	}

}
