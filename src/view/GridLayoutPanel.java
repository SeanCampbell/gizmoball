package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class GridLayoutPanel extends JPanel {

	private static final long serialVersionUID = 8844919255973893688L;
	private static final String BACKGROUND_COLOR = "#D3D3D3";
	private static final String TOP_BACKGROUND_COLOR = "0xA9A9A9";
	
	public GridLayoutPanel(int height, int rows, int columns) {
		this.setLayout(new GridLayout(rows, columns, 5, 20));	//TODO change layout to make buttons nicer
		this.setPreferredSize(new Dimension(140,height));
		this.setBorder(BorderFactory.createEmptyBorder(70, 20, 70, 10));
		this.setBackground(Color.decode(BACKGROUND_COLOR));
	}
	
	public JButton createImageButton(String fileName) {
		
		JButton button = new JButton(new ImageIcon(fileName));
		Border buttonBorder = BorderFactory.createRaisedBevelBorder();
		button.setBorder(buttonBorder);
		button.setBackground(Color.decode(TOP_BACKGROUND_COLOR));
		
		return button;
	}
	

	public JButton createButton(String name) {
		
		JButton button = new JButton(name);
		Border buttonBorder = BorderFactory.createRaisedBevelBorder();
		button.setBorder(buttonBorder);
		button.setBackground(Color.decode(TOP_BACKGROUND_COLOR));
		button.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		return button;
	}

}
