package view;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JSlider;

import controller.buildmode.SetGravityAction;
import model.IGameBoard;

public class GravitySliderBar extends JDialog{
	private static final long serialVersionUID = -6738230263673192625L;
	private JSlider slider;
	private SetGravityAction sga;
	
	public GravitySliderBar(IGameBoard model) {

		sga = new SetGravityAction();
		slider = createSlider();
		Object[] message = new Object[] {"Select a value for gravity: ", slider};
		int s = JOptionPane.showConfirmDialog(null, message, "Gravity", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
		if(s==JOptionPane.OK_OPTION) {
			model.setGravity(sga.getValue());
		}
	}
	
	private JSlider createSlider() {
		JSlider s = new JSlider();
		s.setMajorTickSpacing(10);
		s.setMinorTickSpacing(5);
		s.setMaximum(50);
		s.setPaintTicks(true);
		s.setPaintLabels(true);
		s.addChangeListener(sga);
		
		return s;
	}

}
