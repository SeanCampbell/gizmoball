package view;

import java.awt.Graphics;

import view.isotropic.IsotropicFilter;

public interface GizmoGeometry {
	
	/**
	 * draw the geometry to the graphic object provided.
	 * @param g the graphic object to draw on
	 * @param filter the isotropic co-ordinate filter to use
	 */
	void draw(Graphics g, IsotropicFilter filter);

	/**
	 * move the geometry in the desired direction.
	 * @param dx the difference in x to move
	 * @param dy the difference in y to move
	 */
	void move(double dx, double dy);
}