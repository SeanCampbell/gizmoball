package view;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import view.isotropic.IsotropicFilter;

public class GizmoCursor {

	private Component pane;
	
	public GizmoCursor(Component pane) {
		this.pane = pane;
	}
	
	public void setCursor(GizmoShape shape) {
		// get the toolkit
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		
		// create icon
		BufferedImage img = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = img.createGraphics();
		IsotropicFilter filter = new IsotropicFilter(32, 32, 2, 2);
		shape.draw(g, filter);
		
		// draw pointer
		g.setColor(new Color(1f, 1f,1f, 1f));
		g.fillRect(0, 0, 16, 3);
		g.fillRect(0, 0, 3, 16);
		
		Cursor c = toolkit.createCustomCursor(img, new Point(pane.getX(), pane.getY()), "img");
		pane.setCursor(c);
	}
}
