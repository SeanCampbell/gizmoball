package view;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

class KeyBindDialog extends JDialog implements ActionListener, KeyListener {
	private static final long serialVersionUID = 6237232475584958613L;
	private int key;
	private CardLayout cl;
	private JPanel pane, keyPanel, keyRetrieved;
	private JOptionPane optionPane;
	
	public KeyBindDialog(Frame parent) {
		super(parent, true);
		
		setTitle("Key Binding");
		
		// set the current keybind to undefined
		key = KeyEvent.VK_UNDEFINED;
		
		buildKeyEntry();
		buildKeyRetreived();
		buildPane();
		
		setContentPane(pane);
		setLocationRelativeTo(parent);
		
		// add key listener
		addKeyListener(this);
		
		this.pack();
	}
	
	public int getInputtedKey() {
		return key;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	private void buildPane() {
		pane = new JPanel();
		cl = new CardLayout();
		pane.setLayout(cl);
		pane.add(keyPanel, "keyentry");
		pane.add(keyRetrieved, "keyretrieved");
		cl.show(pane, "keyentry");
	}
	
	private void buildKeyEntry() {
		keyPanel = new JPanel();
		keyPanel.add(new JLabel("Please press a key."));
		keyPanel.setPreferredSize(new Dimension(200, 50));
	}

	private void buildKeyRetreived() {
		keyRetrieved = new JPanel();
		
		optionPane = new JOptionPane(
                "Press ok to continue.",
                JOptionPane.PLAIN_MESSAGE,
                JOptionPane.DEFAULT_OPTION);
		
		keyRetrieved.add(optionPane);
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// only get input once
		System.out.println("got key: " + arg0.getKeyCode());
		key = arg0.getKeyCode();
		optionPane.setMessage("The \"" + Character.toString((char)key) + "\" key will be bound.");
		//cl.show(pane, "keyretrieved");
		removeKeyListener(this);
		dispose();
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
}