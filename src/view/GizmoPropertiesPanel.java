package view;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.Border;

public class GizmoPropertiesPanel extends JPanel {

	private static final long serialVersionUID = 3843292764824794796L;
	private static final String BACKGROUND_COLOR = "#D3D3D3";
	private static final String TOP_BACKGROUND_COLOR = "0xA9A9A9";
	private JPanel buttonPanel, messagePanel;
	private JTextArea textArea;
	private CardLayout cl;

	public GizmoPropertiesPanel() {
		cl = new CardLayout();
		this.setLayout(cl);
		this.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		//this.setPreferredSize(new Dimension(width-100,100));
		this.setBackground(Color.decode(TOP_BACKGROUND_COLOR));
		buttonPanel = new JPanel(new GridLayout(2, 3, 5, 5));
		buttonPanel.setBackground(Color.decode(TOP_BACKGROUND_COLOR));
		messagePanel = new JPanel();
		messagePanel.setBackground(Color.decode(TOP_BACKGROUND_COLOR));
		textArea = new JTextArea();
		textArea.setLineWrap(false);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);
		textArea.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 16));
		//textArea.setPreferredSize(new Dimension(getPreferredSize().width-20,getPreferredSize().height-20));
		textArea.setBackground(Color.decode(TOP_BACKGROUND_COLOR));
		messagePanel.add(textArea);
		this.add(messagePanel, "Message");
		this.add(buttonPanel,"Buttons");
	}
	
	public void showButtons() {
		cl.show(this, "Buttons");
	}
	
	public void showText() {
		cl.show(this,  "Message");
	}
	
	
	public JButton createButton(String name, String actionCommand) {
		JButton button = new JButton(name);
		Border buttonBorder = BorderFactory.createRaisedBevelBorder();
		button.setBorder(buttonBorder);
		button.setPreferredSize(new Dimension(70, 30));
		button.setBackground(Color.decode(BACKGROUND_COLOR));
		button.setActionCommand(actionCommand);
		return button;
	}
	
	public void addButton(JButton button) {
		buttonPanel.add(button);
	}
	
	public void writeMessage(String message) {
		textArea.setText(message);
	}
}
