package view;

import java.awt.Component;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class FileSelection {
	private static final String EXTENSION = "giz";
	private static final JFileChooser fc = new JFileChooser();
	private static final FileFilter filter = new FileNameExtensionFilter("Gizmo File", FileSelection.EXTENSION);
	
	public static File openFile(Component c){
		FileSelection.fc.setFileFilter(FileSelection.filter);
		FileSelection.fc.setAcceptAllFileFilterUsed(false);
		final int returnVal = FileSelection.fc.showOpenDialog(c);

		if(returnVal == JFileChooser.APPROVE_OPTION)
			return FileSelection.fc.getSelectedFile();

		return null;
	}
	
    public static File saveFile(Component c){
    	FileSelection.fc.setFileFilter(FileSelection.filter);
    	FileSelection.fc.setAcceptAllFileFilterUsed(false);
    	final int returnVal = FileSelection.fc.showSaveDialog(c);
    	
        if(returnVal == JFileChooser.APPROVE_OPTION){
        	File f = fc.getSelectedFile();
        	final String actName = f.getAbsolutePath();
        	if(!actName.endsWith(FileSelection.EXTENSION)){
        		f = new File(actName+"."+FileSelection.EXTENSION);
        	}
        	return f;
        }
        	
        return null;
	}
}

