package view;

import java.awt.event.ActionListener;

import model.IGameBoard;
import controller.ContextSwitchListener;
import controller.GridComponentListener;

public interface IBuildView {
	/**
	 * Set the cursor to a specific gizmo icon.
	 * @param type the type of gizmo to set. Pass null for the default cursor.
	 */
	public void setGizmoCursor(String type);
	
	/**
	 * Set whether we want the grid selection to be a drag select or a single selection.
	 * @param enabled true if we want to allow drag selection.
	 */
	public void setDragMode(boolean enabled);

	/**
	 * Set whether we want to show the gizmo modifier buttons. This will replace info messaging.
	 * @param enabled true to show the buttons, else false to restore the messaging.
	 */
	public void setShowModificationButtons(boolean enabled);
	
	/**
	 * Set the information panel text. NOTE this won't show if the modification buttons are displayed!
	 * @param text the text to display.
	 */
	public void setInformationText(String text);
	
	/**
	 * Set the listener for button presses involving load/save.
	 * @param listener an ActionListener able to handle "SAVEGAME" + "LOADGAME" action commands.
	 */
	public void setLoadSaveButtonListener(ActionListener listener);
	
	/**
	 * Set the listener for any button presses on the add gizmo buttons.
	 * @param listener a listener ready to handle actions from the add gizmo. Action command is "ADDGIZMO".
	 */
	public void addGizmoButtonListener(ActionListener listener);
	
	/**
	 * Set the listener for any gizmo modification button presses.
	 * <p>
	 * Action names are:
	 * ROTATEGIZMO - request to rotate a selected gizmo
	 * MOVEGIZMO - request to move a selected gizmo
	 * DELETEGIZMO - request to delete a selected gizmo
	 * CONNECTTRIGGER - request to connect the selected gizmo to a trigger
	 * DISCONNECTTRIGGER - request to disconnect the selected gizmo from a trigger
	 * ACTION - request for action??
	 * </p>
	 * @param listener a listener ready to handle the appropriate actions.
	 */
	public void addGizmoModificationButtonListener(ActionListener listener);
	
	/**
	 * Set the listener that will receive events when the gameboard grid has been interacted with.
	 * @param listener the GridComponentListener that will receive the notifications.
	 */
	public void addGridComponentListener(GridComponentListener listener);
	
	/**
	 * Set the listener for when the user wants to switch to run mode.
	 * @param listener A context switch listener that will transform the program into run mode.
	 */
	public void addContextSwitchListener(ContextSwitchListener listener);
	
	/**
	 * This will reset the view to use another model
	 * @param model the model the view is supposed to use.
	 */
	public void setModel(IGameBoard model);

	/**
	 * get the file to load. This is typically completed using a JFileChooser.
	 * @return path to the file to load, else null if canceled.
	 */
	public String getLoadFile();

	public java.io.File getSaveFile();
	/**
	 * show an error message to the user
	 * @param error the error message.
	 */
	public void showErrorDialog(String error);

	/**
	 * Sets a region to selected.
	 * @param x the x-coordinate of the region that has been chosen.
	 * @param y the y-coordinate of the region that has been chosen.
	 * @param width the width of the the selection.
	 * @param height the height of the selection.
	 */
	public void setSelected(double x, double y, double width, double height);

	/**
	 * Will remove any previous selections
	 */
	public void removeSelection();

	/**
	 * Gets the the key trigger type.
	 * @return either "PRESS", "RELEASE" or "BOTH". null on cancel.
	 */
	public String getKeyTriggerType();

	/**
	 * Opens a dialog to get a key input.
	 * @return the keycode of the input.
	 */
	public int getKeyBind();
}
