package controller;

import model.IGameBoard;

public interface IController {

	/**
	 * Called when the model has been changed and needs to be reintroduced.
	 * @param model the new model.
	 */
	void setModel(IGameBoard model);
	/**
	 * Called when the state is to be switched out; essentially an opportunity
	 * for clean-up.
	 */
	void stop();
}
