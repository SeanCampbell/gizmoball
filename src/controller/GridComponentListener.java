package controller;

public interface GridComponentListener {
	/**
	 * A notification method that will tell you when a point has been clicked on.
	 * @param x the x-coordinate (in L co-ordinates)
	 * @param y the y-coordinate (in L co-ordinates)
	 */
	public void selected(double x, double y);
	
	/**
	 * A notification method that will tell you when an area has been selected
	 * @param x the top left x-coordinate
	 * @param y the top left y-coordinate
	 * @param width the width of the selection
	 * @param height the height of the selection
	 */
	public void areaSelected(int x, int y, int width, int height);
}
