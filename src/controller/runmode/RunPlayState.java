package controller.runmode;

import java.awt.event.ActionEvent;

public class RunPlayState implements IRunState{
	private RunModeStateMachine context;
	
	public RunPlayState(RunModeStateMachine ctx){
		this.context = ctx;
	}
	
	@Override
	public void begin() {
		this.context.startTimer();
	}

	@Override
	public void end() {
		this.context.stopTimer();
	}

	@Override
	public String getName() {
		return "RUN_PLAY";
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		final String action = e.getActionCommand();
		if(action.equals("Timer")) {
			//System.out.println("System nano:" + System.nanoTime());
			//System.out.println("Timer TICK!");
			this.context.getModel().tick();
			this.context.getView().setFocus();
		}
		else if(action.equals("Pause")) {
			this.context.setState(new RunWaitingState(this.context));
		}
	}

}
