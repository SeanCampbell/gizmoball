package controller.runmode;

import java.awt.event.ActionEvent;

public class RunTickState implements IRunState {
	private RunModeStateMachine context;
	
	public RunTickState(RunModeStateMachine ctx){
		this.context = ctx;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
	}

	@Override
	public void begin() {
		this.context.getModel().tick();
		this.context.setState(new RunWaitingState(this.context));
	}

	@Override
	public void end() {
	}

	@Override
	public String getName() {
		return "RUN_TICK";
	}

}
