package controller.runmode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import controller.IController;
import controller.KeyTriggerListener;
import controller.MagicKeyListener;
import controller.StateController;
import view.RunView;
import model.IGameBoard;

public class RunModeStateMachine implements IController, ActionListener {
	private static final String ACTION = "Timer";
	//private StateController ctx;
	private IGameBoard model;
	private RunView view;
	
	private IRunState currentState;
	
	private Timer timer;
	
	public RunModeStateMachine(StateController state, IGameBoard model) {
		//this.ctx = state;
		this.model = model;
		this.view = new RunView(model);
		this.timer = new Timer(20, this);
		this.timer.setActionCommand(RunModeStateMachine.ACTION);
		//when changing the views the observer pattern is used to show
		//the graphics
		model.fireNotification(); 
		this.view.addContextSwitchListener(state);
		this.view.addButtonListener(this);
		this.view.addKeyListener(new MagicKeyListener(new KeyTriggerListener(model.getTM())));
		this.currentState = new RunWaitingState(this);
	}
	
	public void setState(IRunState state){
		System.out.println("Moving from state:" + currentState.getName() + " to state:" + state.getName());
		currentState.end();
		currentState = state;
		currentState.begin();
	}
	
	public void setModel(IGameBoard model) {
		this.model = model;
	}
	
	public IGameBoard getModel(){
		return this.model;
	}
	
	public RunView getView(){
		return this.view;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		currentState.actionPerformed(e);
	}
	
	public void startTimer(){
		this.timer.start();
	}
	
	public void stopTimer(){
		this.timer.stop();
	}

	@Override
	public void stop() {
		this.timer.stop();
		this.timer = null;
	}

}
