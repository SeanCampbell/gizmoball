package controller.runmode;

import java.awt.event.ActionListener;
/**
 * Following the state pattern, an instance of IRunState
 * must provide functionality upon entry of the state, exit
 * and for fetching the name of the state.
 */
public interface IRunState extends ActionListener {
	/**
	 * The entry to commence upon a state switch of the FSM
	 */
	void begin();
	/**
	 * The entry to end upon a state switch of the FSM
	 */
	void end();
	/**
	 * A simple String denoting the name of the state
	 * @return the state name
	 */
	String getName();
}
