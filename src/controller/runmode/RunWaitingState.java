package controller.runmode;

import java.awt.event.ActionEvent;


public class RunWaitingState implements IRunState {
	private RunModeStateMachine context;
	
	public RunWaitingState(RunModeStateMachine ctx){
		this.context = ctx;
	}
	
	
	@Override
	public void begin() {
	}

	@Override
	public void end() {
	}

	@Override
	public String getName() {
		return "RUN_WAITING";
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if(action.equals("Play")) {
			this.context.setState(new RunPlayState(this.context));
		}
		else if(action.equals("Tick")) {
			this.context.setState(new RunTickState(this.context));
		}
	}
}
