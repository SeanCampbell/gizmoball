package controller;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import shapes.LogicalPoint;
import view.isotropic.IsotropicFilter;

public class GridMouseListener implements MouseListener, MouseMotionListener {

	private boolean dragMode, dragging;
	private LogicalPoint start;
	private GridComponentListener listener;
	private IsotropicFilter filter;
	private DragEventListener dragEventListener;
	
	/**
	 * Create GridMouseListener with a component listener.
	 * <p>
	 * This function will allow to monitor mouse interaction with the board.
	 * The GridMouseListener will all record drag and singular click events.
	 * </p>
	 * @param listener the listener to notify on events
	 * @param filter the filter to apply to the points to translate into logical points.
	 */
	public GridMouseListener(GridComponentListener listener, IsotropicFilter filter) {
		dragMode = false;
		dragging = false;
		this.filter = filter;
		this.listener = listener;
	}
	
	/**
	 * Set whether to expect dragging or singular clicking.
	 * @param enabled true if we want drag information else false for clicking.
	 */
	public void setDragMode(boolean enabled) {
		dragMode = enabled;
		dragging = false;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if (dragMode && dragging && dragEventListener != null) {
			dragEventListener.onDragUpdate(filter.getLogical(e.getPoint()));
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		// do nothing
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// do nothing
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// do nothing
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// get the logical point
		LogicalPoint pt = filter.getLogical(e.getPoint());
		
		// are we in drag mode?
		if (dragMode) {
			// set start point and set dragging
			start = pt;
			dragging = true;
			
			// notify our handler
			if (dragEventListener != null) {
				dragEventListener.onDragBegin(pt);
			}
		}
		else {
			// singular click, notify
			listener.selected(pt.getX(), pt.getY());
		}
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// are we in drag mode?
		if (dragMode && dragging) {
			// end of drag
			dragging = false;
			
			// get point
			LogicalPoint pt = filter.getLogical(e.getPoint());
			
			if (dragEventListener != null) {
				dragEventListener.onDragEnd(filter.getLogical(e.getPoint()));
			}
			

			int sx = (int)Math.floor(Math.min(start.getX(), pt.getX()));
			int sy = (int)Math.floor(Math.min(start.getY(), pt.getY()));
			int ex = (int)Math.floor(Math.max(start.getX(), pt.getX()))+1;
			int ey = (int)Math.floor(Math.max(start.getY(), pt.getY()))+1;
			
			listener.areaSelected(sx, sy, ex-sx, ey-sy);
		}
	}

	public void setDragEventListener(DragEventListener listener) {
		dragEventListener = listener;
	}
}
