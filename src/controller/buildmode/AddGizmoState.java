package controller.buildmode;

import java.awt.event.ActionEvent;

import exceptions.DuplicateNameException;
import exceptions.ManufactureException;
import exceptions.NoSuchItemException;
import exceptions.OutOfBoundsException;
import exceptions.OverlappingItemException;


public class AddGizmoState extends BuildModeState {

	private BuildModeStateMachine ctx;
	private String type;
	private ActionEvent event;
	
	public AddGizmoState(BuildModeStateMachine ctx, ActionEvent e) {
		this.ctx = ctx;
		this.event = e;
	}

	@Override
	public void begin() {
		if (!(event.getSource() instanceof AddGizmoAction)) {
			// TODO: what do we do here? go back to waiting state?
			throw new IllegalArgumentException();
		}
		
		AddGizmoAction act = (AddGizmoAction)event.getSource();
		type = act.getGizmoType();
		System.out.println("Got type: " + type);

		// set to show information
		ctx.getView().setShowModificationButtons(false);
		ctx.getView().setInformationText("");
		
		// check if its a variable sized component
		try{
		ctx.getView().setDragMode(ctx.getModel().getGizmoDimensions(type).isVariableSize());
		} catch(NoSuchItemException e) {
			ctx.getView().setInformationText("Error fetching dimensions for type: " + type);
		}
		
		// set the cursor
		ctx.getView().setGizmoCursor(type);
	}
	
	@Override
	public void end() {
		// reset the cursor
		ctx.getView().setGizmoCursor(null);
	}
	
	@Override
	public void selected(double x_, double y_) {
		int x = (int)Math.floor(x_);
		int y = (int)Math.floor(y_);
		
		System.out.println("selected on grid: " + x + ", " + y);
		
		// get a valid name
		String name = ctx.createGizmoName(type);
		System.out.println("adding gizmo: " + type + " name: " + name);
		
		try {
			ctx.getModel().addGizmo(type, name, x, y);
		}  
		catch (DuplicateNameException e) {
			assert false : "Got a duplicate name! should be impossible";
		}
		catch (OverlappingItemException e) {
			ctx.getView().setInformationText("Cannot place here. A gizmo already occupies this area!");
			return;
		}
		catch (ManufactureException e) {
			ctx.getView().setInformationText("Problem with the factory!  Cannot manufacture!");
			return;
		}
		catch (OutOfBoundsException e) {
			ctx.getView().setInformationText("Cannot place here. Outwith playing area!");
			return;
		}
		
		// gizmo added!
		ctx.setState(new WaitingState(ctx));
	}
	
	@Override
	public void areaSelected(int x, int y, int width, int height) {
		// get a valid name
		String name = ctx.createGizmoName(type);
		
		try {
			ctx.getModel().addGizmo(type, name, x, y, x+width, y+height);
		} 
		catch (DuplicateNameException e) {
			assert false : "Got a duplicate name! should be impossible";
		}
		catch (OverlappingItemException e) {
			ctx.getView().setInformationText("Cannot place here. A gizmo already occupies this area!");
			return;
		}
		catch (ManufactureException e) {
			ctx.getView().setInformationText("Problem with the factory!  Cannot manufacture!");
			return;
		}
		catch (OutOfBoundsException e) {
			ctx.getView().setInformationText("Invalid placement!  Would exceed board boundaries!");
			return;
		}
		
		ctx.setState(new WaitingState(ctx));
	}
	
	@Override
	public String getName() {
		return "ADD_GIZMO";
	}
}
