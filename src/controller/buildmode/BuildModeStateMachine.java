package controller.buildmode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import view.BuildView;
import view.IBuildView;
import model.IGameBoard;
import controller.GridComponentListener;
import controller.IController;
import controller.StateController;

public class BuildModeStateMachine implements IController, GridComponentListener, ActionListener {

	private StateController ctx;
	private BuildModeState currentState;
	private IGameBoard model;
	private IBuildView view;
	
	/**
	 * Construct a BuildModeStateMachine with the StateController and GameBoard
	 * @param ctx a state controller to switch context.
	 * @param model the model to build on.
	 */
	public BuildModeStateMachine(StateController ctx, IGameBoard model) {
		this.ctx = ctx;
		this.model = model;
		this.view = new BuildView(model);
		currentState = new InitState(this, ctx);
		
		// tell the model to send all information
		model.fireNotification();
		
		// start the machine
		currentState.begin();
	}
	
	/**
	 * Swap to the next state
	 * @param newState the next state to use
	 */
	public void setState(BuildModeState newState) {
		System.out.println("Moving from state: \"" + currentState.getName() + "\" to \"" + newState.getName() + "\".");
		currentState.end();
		currentState = newState;
		currentState.begin();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		currentState.actionPerformed(e);
	}
	
	@Override
	public void selected(double x, double y) {
		currentState.selected(x, y);
	}
	
	/**
	 * Convenience method to get the state controller
	 * @return the state controller
	 */
	public StateController getStateController() {
		return ctx;
	}
	
	/**
	 * Convenience method to get the model
	 * @return the model
	 */
	public IGameBoard getModel() {
		return model;
	}
	
	/**
	 * Convenience method to get the view
	 * @return the view
	 */
	public IBuildView getView() {
		return view;
	}

	/**
	 * Creates a unique gizmo name
	 * @param type the type of the gizmo
	 * @return a unique string to add to model
	 */
	public String createGizmoName(String type) {
		String genStr = "";
		Random rng = new Random();
		
		while (true) {
			genStr = type + rng.nextInt(400);
			
			// check if unique
			if (!model.hasGizmo(genStr) && !model.hasGizmoBall(genStr)) {
				break;
			}
		}
		
		return genStr;
	}

	@Override
	public void areaSelected(int x, int y, int width, int height) {
		currentState.areaSelected(x, y, width, height);
	}

	@Override
	public void setModel(IGameBoard gb) {
		model = gb;
		view.setModel(model);
		model.fireNotification();
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}
}
