package controller.buildmode;

import exceptions.NoSuchItemException;

public class RemoveGizmoState extends BuildModeState {

	private BuildModeStateMachine ctx;
	private int x, y;
	
	public RemoveGizmoState(BuildModeStateMachine ctx, int x, int y) {
		this.ctx = ctx;
		this.x = x;
		this.y = y;
	}

	@Override
	public String getName() {
		return "REMOVE_GIZMO";
	}

	@Override
	public void begin() {
		System.out.printf("Deleting Gizmo at (%d, %d)\n", x, y);

		try {
			String name = ctx.getModel().gizmoNameAt(x, y);
			ctx.getModel().removeGizmo(name);
			ctx.getView().setInformationText("Gizmo " + name + " was removed from the board");
		} catch (NoSuchItemException e){
			ctx.getView().setInformationText("Model has no such gizmo!");
			ctx.setState(new WaitingState(ctx));
			return;
		}
		ctx.setState(new WaitingState(ctx));
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

}
