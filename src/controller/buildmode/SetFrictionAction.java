package controller.buildmode;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SetFrictionAction implements ChangeListener{
	private double value = 0.025;

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider slider = (JSlider)e.getSource();
		if (!slider.getValueIsAdjusting()) {
			value = slider.getValue();
			value = (0.01 * value) / 2;	//adjustment since sliders can't handle doubles
		}

	}
	
	public double getValue() {
		return value;
	}

}
