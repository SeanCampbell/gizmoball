package controller.buildmode;

import model.IGameBoard;
import parsing.ILoader;
import parsing.Loader;

public class LoadGameState extends BuildModeState {

	private BuildModeStateMachine ctx;
	
	public LoadGameState(BuildModeStateMachine ctx) {
		this.ctx = ctx;
	}
	
	@Override
	public void begin() {
		// get the file name
		String file = ctx.getView().getLoadFile();
		
		// check for user cancel operation
		if (file != null) {
			ILoader loader = new Loader();
			IGameBoard gb = ctx.getModel();
			gb.clearBoard();
			loader.load(file, gb);
			if(loader.getCountParsingErrors()!=0){
				this.ctx.getView().showErrorDialog(loader.getMessageParsingErrors());
			}
			/*if(loader.getCountGameBoardErrors()!=0){
				this.ctx.getView().showErrorDialog(loader.getMessageGameBoardErrors());
			}*/
			// reset the model
			ctx.getStateController().setModel(loader.getGameBoard());
		}
		ctx.setState(new WaitingState(ctx));
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getName() {
		return "LOAD_GAME";
	}

}
