package controller.buildmode;

import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SetGravityAction implements ChangeListener {
	private int value = 25;

	@Override
	public void stateChanged(ChangeEvent e) {
		JSlider slider = (JSlider)e.getSource();
		if (!slider.getValueIsAdjusting()) {
			value = slider.getValue();
		}
	}
	
	public int getValue() {
		return value;
	}

}
