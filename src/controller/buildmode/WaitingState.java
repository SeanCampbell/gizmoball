package controller.buildmode;

import java.awt.event.ActionEvent;

public class WaitingState extends BuildModeState {

	private BuildModeStateMachine ctx;
	
	public WaitingState(BuildModeStateMachine ctx) {
		this.ctx = ctx;
	}
	
	@Override
	public void begin() {
		// disable drag mode
		ctx.getView().setDragMode(false);
		
		// set to information panel
		ctx.getView().setShowModificationButtons(false);
		
		// clear information panel
		//ctx.getView().setInformationText("");
	}
	
	@Override
	public void end() {
		// do nothing
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("ADDGIZMO")) {
			ctx.setState(new AddGizmoState(ctx, e));
		}
		else if (e.getActionCommand().equals("ADDGIZMOBALL")) {
			ctx.setState(new AddGizmoBallState(ctx, e));
		}
		else if (e.getActionCommand().equals("LOADGAME")) {
			ctx.setState(new LoadGameState(ctx));
		}
		else if (e.getActionCommand().equals("SAVEGAME")) {
			ctx.setState(new SaveGameState(ctx));
		}
	}

	@Override
	public void selected(double x_, double y_) {
		// first we check for a gizmoball
		if (ctx.getModel().hasGizmoBallAt(x_, y_)) {
			// move into gizmoball selected state
			ctx.setState(new GizmoBallSelectedState(ctx, x_, y_));
		}
		
		// ok, we only care about the grids now
		int x = (int)Math.floor(x_);
		int y = (int)Math.floor(y_);
		
		// check if there is a gizmo here
		if (!ctx.getModel().hasGizmoAt(x, y)) {
			// nothing here, we dont care
			return;
		}
		
		// move into gizmo selected state
		ctx.setState(new GizmoSelectedState(ctx, x, y));
	}
	
	@Override
	public String getName() {
		return "WAITING";
	}
}
