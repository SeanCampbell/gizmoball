package controller.buildmode;

import java.awt.event.ActionEvent;

import exceptions.DuplicateNameException;
import exceptions.OutOfBoundsException;
import exceptions.OverlappingItemException;

public class AddGizmoBallState extends BuildModeState {

	private BuildModeStateMachine ctx;
	private ActionEvent event;
	private String type;
	
	public AddGizmoBallState(BuildModeStateMachine ctx, ActionEvent e) {
		this.ctx = ctx;
		this.event = e;
	}

	@Override
	public void begin() {
		if (!(event.getSource() instanceof AddGizmoAction)) {
			// TODO: what do we do here? go back to waiting state?
			throw new IllegalArgumentException();
		}
		
		AddGizmoAction act = (AddGizmoAction)event.getSource();
		type = act.getGizmoType();
		
		// set to non drag mode
		ctx.getView().setDragMode(false);
		
		// set to show information
		ctx.getView().setShowModificationButtons(false);
		ctx.getView().setInformationText("");
		ctx.getView().setGizmoCursor(type);
	}
	
	@Override
	public void end() {
		// reset the cursor
		ctx.getView().setGizmoCursor(null);
	}
	
	@Override
	public void selected(double x, double y) {
		System.out.println("selected on grid: " + x + ", " + y);
		
		// get a valid name
		String name = ctx.createGizmoName("GizmoBall");
		System.out.println("adding gizmo: " + " name: " + name);
		
		try {
			ctx.getModel().addGizmoBall(name, x, y, 0, 0);
		}  
		catch (DuplicateNameException e) {
			assert false : "Got a duplicate name! should be impossible";
		}
		catch (OverlappingItemException e) {
			ctx.getView().setInformationText("Cannot place here. A gizmo already occupies this area!");
			return;
		}
		catch (OutOfBoundsException e) {
			ctx.getView().setInformationText("Cannot place here. Outwith playing area!");
			return;
		}
		
		// gizmo added!
		ctx.setState(new WaitingState(ctx));
	}
	
	@Override
	public String getName() {
		return "ADD_GIZMOBALL";
	}

}
