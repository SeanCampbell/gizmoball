package controller.buildmode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import controller.GridComponentListener;

public abstract class BuildModeState implements GridComponentListener, ActionListener {
	
	public abstract void begin();
	public abstract void end();
	public abstract String getName();
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// do nothing
	}
	
	@Override
	public void selected(double x, double y) {
		// do nothing
	}
	
	@Override
	public void areaSelected(int x, int y, int width, int height) {
		// do nothing
	}
}
