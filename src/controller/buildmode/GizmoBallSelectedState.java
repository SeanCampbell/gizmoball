package controller.buildmode;

import java.awt.event.ActionEvent;

import model.IDimension;
import model.gizmos.Rectangle;
import exceptions.NoSuchItemException;

public class GizmoBallSelectedState extends BuildModeState {

	private final BuildModeStateMachine ctx;
	private final String name;
	
	public GizmoBallSelectedState(BuildModeStateMachine ctx, double x, double y) {
		this.ctx = ctx;
		String name = null;
		
		try {
			name = ctx.getModel().gizmoBallNameAt(x, y);
		} 
		catch (NoSuchItemException e) {
			assert false : "Received invalid item from selection!";
			this.ctx.getView().setInformationText("Received invalid item from selection!");
		}
		
		// set the name of this gizmoball
		this.name = name;
	}
	
	@Override
	public void begin() {
		// show modification buttons
		this.ctx.getView().setShowModificationButtons(true);
		this.ctx.getView().setInformationText("");
		try {
			IDimension dim = this.ctx.getModel().getGizmoBallDimensionByName(name);
			Rectangle r = dim.getBoundaryInfo();
			System.out.println("r.getmin: " + (r.getMaxx()-r.getMinx()));
			this.ctx.getView().setSelected(r.getMinx(), r.getMiny(), r.getMaxx()-r.getMinx(), r.getMaxy()-r.getMiny());
		} catch (NoSuchItemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void end() {
		ctx.getView().removeSelection();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand().equals("DELETEGIZMO")) {
			ctx.setState(new RemoveGizmoBallState(ctx, name));
		}
		else if (e.getActionCommand().equals("MOVEGIZMO")) {
			ctx.setState(new MoveGizmoBallState(ctx, name));
		}
		else {
			ctx.getView().setInformationText("This operation cannot be performed upon a GizmoBall!");
			ctx.setState(new WaitingState(ctx));
		}
	}
	
	@Override
	public void selected(double x, double y) {
		ctx.setState(new WaitingState(ctx));
	}
	
	@Override
	public String getName() {
		return "GIZMOBALL_SELECTED";
	}

}
