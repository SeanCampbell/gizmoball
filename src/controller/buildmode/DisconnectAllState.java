package controller.buildmode;

import exceptions.NoSuchItemException;

public class DisconnectAllState extends BuildModeState{
	private BuildModeStateMachine ctx;
	private int x, y;
	
	public DisconnectAllState(BuildModeStateMachine ctx, int x, int y) {
		this.ctx = ctx;
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void begin() {
		System.out.printf("Removing all Gizmo connections at (%d, %d)\n", x, y);
		try {
			String name = ctx.getModel().gizmoNameAt(x, y);
			ctx.getModel().disconnect(name);
			ctx.getView().setInformationText("Gizmo " + name + " was disconnected from everything!");
		} catch (NoSuchItemException e){
			ctx.getView().setInformationText("Model has no such gizmo!");
		}
		ctx.setState(new WaitingState(ctx));
	}	

	@Override
	public void end() {
	}

	@Override
	public String getName() {
		return "DISCONNECT";
	}
}