package controller.buildmode;

import exceptions.NoSuchItemException;

public class ConnectGizmoState extends BuildModeState {

	private BuildModeStateMachine ctx;
	private int x, y;
	private String gizmo;
	
	public ConnectGizmoState(BuildModeStateMachine ctx, int x, int y) {
		this.ctx = ctx;
		this.x = x;
		this.y = y;
		try {
			this.gizmo = ctx.getModel().gizmoNameAt(x, y);
		} catch (NoSuchItemException e) {
			ctx.getView().setInformationText("Error with connect gizmo state.");
		}
	}
	
	@Override
	public void begin() {
		// TODO Auto-generated method stub

		ctx.getView().setShowModificationButtons(false);
		ctx.getView().setInformationText("Select another gizmo to connect to.");
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selected(double x_, double y_) {
		int x = (int)Math.floor(x_);
		int y = (int)Math.floor(y_);
		System.out.println("Connect gizmo (" + this.x + ", " + this.y + ") to (" + x + ", " + y + ")");
		
		String trigger = "";
		try {
			trigger = ctx.getModel().gizmoNameAt(x, y);
			System.out.println("trying to connect: " + trigger + " : " + gizmo);
			
			ctx.getModel().connect(trigger, gizmo);
		} catch (NoSuchItemException e) {
			// no gizmo at this location
			ctx.getView().setInformationText("There is no gizmo at (" + x + "," + y + ")");
			return;
		}
		

		ctx.getView().setInformationText(gizmo + " at (" + this.x + "," + this.y + ") connected "
				+ "to " + trigger +  " at (" + x + "," + y + ")");
		ctx.setState(new WaitingState(ctx));
	}
	
	@Override
	public String getName() {
		return "CONNECT_GIZMO";
	}

}
