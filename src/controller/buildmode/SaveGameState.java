package controller.buildmode;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.JOptionPane;

public class SaveGameState extends BuildModeState {

	private BuildModeStateMachine ctx;
	
	public SaveGameState(BuildModeStateMachine ctx) {
		this.ctx = ctx;
	}
	
	@Override
	public void begin() {
		// get the file name
		final java.io.File file = this.ctx.getView().getSaveFile();
		
		// check for user cancel operation
		if (file != null) {
			// fetch the string to write to file
			final String toWrite = ctx.getModel().saveBoard();
			try{
				java.io.BufferedWriter br = null;
				try{
				br = new BufferedWriter(new PrintWriter(file));
				br.write(toWrite);
				} catch(IOException e){
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Error when writing to file!");
					return;
				}
				finally{
					br.close();
				}
			} catch(Exception e){
				JOptionPane.showMessageDialog(null, "Error when writing to file!");
			}
		}
		ctx.setState(new WaitingState(ctx));
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getName() {
		return "SAVE_GAME";
	}

}

