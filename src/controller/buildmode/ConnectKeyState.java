package controller.buildmode;

import exceptions.NoSuchItemException;

public class ConnectKeyState extends BuildModeState {

	private BuildModeStateMachine ctx;
	private String name;
	
	public ConnectKeyState(BuildModeStateMachine ctx, int x, int y) {
		this.ctx = ctx;

		try {
			name = ctx.getModel().gizmoNameAt(x, y);
		}
		catch (NoSuchItemException e) {

			System.out.println("2)");	
		}
	}
	
	@Override
	public void begin() {
		// get action
		String action = ctx.getView().getKeyTriggerType();
		
		// check if to cancel
		if (action == null) {
			ctx.setState(new WaitingState(ctx));
			return;
		}
		
		// ok now we get the get to bind key
		int key = ctx.getView().getKeyBind();
		
		try {
			// now lets assign it
			switch (action) {
			case "PRESS":
				ctx.getModel().connect(key, name, false);
				System.out.println("presed");
				break;
				
			case "RELEASE":
				ctx.getModel().connect(key, name, true);
				System.out.println("RELASEd");
				break;

			case "BOTH":
				ctx.getModel().connect(key, name, false);
				ctx.getModel().connect(key, name, true);
			}
		}
		catch (NoSuchItemException e) {
			System.out.println("1)");
		}
		
		System.out.println("GOT ACTION: " + action);
		ctx.setState(new WaitingState(ctx));
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		return "CONNECT_KEY";
	}

}
