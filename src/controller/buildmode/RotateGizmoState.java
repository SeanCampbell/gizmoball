package controller.buildmode;

import exceptions.NoSuchItemException;

public class RotateGizmoState extends BuildModeState {

	private BuildModeStateMachine ctx;
	private int x, y;
	
	public RotateGizmoState(BuildModeStateMachine ctx, int x, int y) {
		this.ctx = ctx;
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void begin() {
		System.out.printf("Rotating Gizmo at (%d, %d)\n", x, y);
		try {
			String name = ctx.getModel().gizmoNameAt(x, y);
			ctx.getModel().rotateGizmo(name);
		} catch (NoSuchItemException e){
			ctx.getView().setInformationText("Model has no such gizmo!");
			ctx.setState(new WaitingState(ctx));
			return;
		} catch (UnsupportedOperationException e) {
			ctx.getView().setInformationText("Cannot rotate this type of gizmo!");
			ctx.setState(new WaitingState(ctx));
			return;
		}
		
		ctx.setState(new GizmoSelectedState(ctx, x, y));
	}
	
	@Override
	public void end() {
		// do nothing
	}
	
	@Override
	public String getName() {
		return "ROTATE_GIZMO";
	}

}
