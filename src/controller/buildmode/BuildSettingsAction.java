package controller.buildmode;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.FrictionSliderBar;
import view.GravitySliderBar;
import model.IGameBoard;

public class BuildSettingsAction implements ActionListener {

	private IGameBoard model;
	
	public BuildSettingsAction(IGameBoard model) {
		this.model = model;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Clear Board")) {
			model.clearBoard();
		}
		else if(e.getActionCommand().equals("Set Gravity")) {
			new GravitySliderBar(model);
		}
		else if(e.getActionCommand().equals("Set Friction")) {
			new FrictionSliderBar(model);
		}
		
	}
	

}
