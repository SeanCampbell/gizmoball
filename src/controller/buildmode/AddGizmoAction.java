package controller.buildmode;

public interface AddGizmoAction {	
	/**
	 * Returns the type of the gizmo as a string.
	 * @return the type of gizmo.
	 */
	String getGizmoType();
}
