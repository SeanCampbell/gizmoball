package controller.buildmode;

import exceptions.NoSuchItemException;
import exceptions.OutOfBoundsException;
import exceptions.OverlappingItemException;

public class MoveGizmoBallState extends BuildModeState {
	private BuildModeStateMachine ctx;
	private String name;
	
	public MoveGizmoBallState(BuildModeStateMachine ctx, String name){
		this.ctx = ctx;
		this.name = name;
	}
	
	@Override
	public void begin() {
		// set to show information
		ctx.getView().setShowModificationButtons(false);
		
		ctx.getView().setInformationText("Select new location");
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getName() {
		return "GIZMOBALL_MOVE";
	}
	
	@Override
	public void selected(double x_, double y_) {
		try {
			System.out.printf("Moving gizmoball %s to (%f, %f)\n",name,x_,y_);
			ctx.getModel().moveGizmoBall(name, x_, y_);
		}
		catch (NoSuchItemException e) {
			ctx.getView().setInformationText("Model has no such gizmoball!");
			ctx.setState(new WaitingState(ctx));
		}
		catch (OverlappingItemException e) {
			ctx.getView().setInformationText("Cannot place here. An item already occupies this area!");
			return;
		}
		catch (OutOfBoundsException e) {
			ctx.getView().setInformationText("Cannot place here. Outwith playing area!");
			return;
		}
		
		// gizmoball moved!
		ctx.getView().setInformationText(name + " moved to (" + x_ + "," + y_ + ")");
		ctx.setState(new WaitingState(ctx));
	}

}
