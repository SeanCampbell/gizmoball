package controller.buildmode;

import java.awt.event.ActionEvent;

import exceptions.NoSuchItemException;
import model.IDimension;
import model.gizmos.Rectangle;

public class GizmoSelectedState extends BuildModeState {

	private BuildModeStateMachine ctx;
	private int x, y;
	
	GizmoSelectedState(BuildModeStateMachine ctx, int x, int y) {
		this.ctx = ctx;
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void begin() {
		// show modification buttons
		this.ctx.getView().setShowModificationButtons(true);
		ctx.getView().setInformationText("");
		String name;
		try {
			name = this.ctx.getModel().gizmoNameAt(x, y);
			IDimension dim = this.ctx.getModel().getGizmoDimensionByName(name);
			Rectangle r = dim.getBoundaryInfo();
			this.ctx.getView().setSelected((int)r.getMinx(), (int)r.getMiny(), (int)(r.getMaxx()-r.getMinx()), (int)(r.getMaxy()-r.getMiny()));
		} catch (NoSuchItemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void end() {
		// do nothing
		ctx.getView().removeSelection();
	}
		
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getActionCommand().equals("ROTATEGIZMO")) {
			ctx.setState(new RotateGizmoState(ctx, x, y));
		}
		else if (e.getActionCommand().equals("DELETEGIZMO")) {
			ctx.setState(new RemoveGizmoState(ctx, x, y));
		}
		else if (e.getActionCommand().equals("MOVEGIZMO")) {
			ctx.setState(new MoveGizmoState(ctx, x, y));
		}
		else if (e.getActionCommand().equals("CONNECTTRIGGER")) {
			ctx.setState(new ConnectGizmoState(ctx, x, y));
		}
		else if (e.getActionCommand().equals("CONNECTKEYTRIGGER")) {
			ctx.setState(new ConnectKeyState(ctx, x, y));
		}
		else if (e.getActionCommand().equals("DISCONNECTTRIGGER") ||
				e.getActionCommand().equals("DISCONNECTKEYTRIGGER")) {
			ctx.setState(new DisconnectAllState(ctx, x, y));
		}
	}
	
	@Override
	public String getName() {
		return "GIZMO_SELECTED";
	}
	
	@Override
	public void selected(double x_, double y_){
		int x = (int)Math.floor(x_);
		int y = (int)Math.floor(y_);
		
		if (ctx.getModel().hasGizmoAt(x, y)){
			this.x = x;
			this.y = y;
		}
		else {
			ctx.setState(new WaitingState(ctx));
		}
	}
}
