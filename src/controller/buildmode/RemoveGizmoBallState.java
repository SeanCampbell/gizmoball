package controller.buildmode;

import exceptions.NoSuchItemException;

public class RemoveGizmoBallState extends BuildModeState {

	private final BuildModeStateMachine ctx;
	private final String name;
	
	public RemoveGizmoBallState(BuildModeStateMachine ctx, String name) {
		this.ctx = ctx;
		this.name = name;
		
		assert ctx.getModel().hasGizmoBall(name) : "State passed invalid name!";
	}
	
	@Override
	public void begin() {
		try {
			ctx.getModel().removeGizmoBall(name);
			ctx.getView().setInformationText("Gizmoball " + name + " was removed from the board");
		} 
		catch (NoSuchItemException e) {
			assert false : "handed invalid gizmoball!";
			ctx.getView().setInformationText("No gizmoball here!");
		}
		
		ctx.setState(new WaitingState(ctx));
	}

	@Override
	public void end() {
		// do nothing
	}

	@Override
	public String getName() {
		return "REMOVE_GIZMOBALL";
	}

}
