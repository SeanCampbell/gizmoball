package controller.buildmode;

import controller.StateController;

public class InitState extends BuildModeState {
	
	private BuildModeStateMachine ctx;
	
	public InitState(BuildModeStateMachine ctx, StateController state) {
		this.ctx = ctx;
		
		// register components with the view
		ctx.getView().addGizmoButtonListener(ctx);
		ctx.getView().addGridComponentListener(ctx);
		ctx.getView().addGizmoModificationButtonListener(ctx);
		ctx.getView().setLoadSaveButtonListener(ctx);
		ctx.getView().addContextSwitchListener(state);
	}

	@Override
	public void begin() {
		// go to waiting state
		ctx.setState(new WaitingState(ctx));
	}
	
	@Override
	public void end() {
		// do nothing
	}
		
	@Override
	public String getName() {
		return "INIT";
	}
}
