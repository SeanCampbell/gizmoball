package controller.buildmode;

import exceptions.NoSuchItemException;
import exceptions.OutOfBoundsException;
import exceptions.OverlappingItemException;

public class MoveGizmoState extends BuildModeState {

	private BuildModeStateMachine ctx;
	private int x, y;
	
	public MoveGizmoState(BuildModeStateMachine ctx, int x, int y) {
		this.ctx = ctx;
		this.x = x;
		this.y = y;
	}
	
	@Override
	public void begin() {
		// set to show information
		ctx.getView().setShowModificationButtons(false);
		
		ctx.getView().setInformationText("Select new location");
	}

	@Override
	public void end() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getName() {
		return "MOVE_GIZMO";
	}

	@Override
	public void selected(double x_, double y_) {
		int x = (int)Math.floor(x_);
		int y = (int)Math.floor(y_);
		String name = "";
		try {
			name = ctx.getModel().gizmoNameAt(this.x, this.y);
			System.out.printf("Moving gizmo %s to (%d, %d)\n",name,x,y);
			ctx.getModel().moveGizmo(name, x, y);
		}
		catch (NoSuchItemException e) {
			ctx.getView().setInformationText("Model has no such gizmo!");
			ctx.setState(new WaitingState(ctx));
		}
		catch (OverlappingItemException e) {
			ctx.getView().setInformationText("Cannot place here. A gizmo already occupies this area!");
			return;
		}
		catch (OutOfBoundsException e) {
			ctx.getView().setInformationText("Cannot place here. Outwith playing area!");
			return;
		}
		
		// gizmo moved!
		ctx.getView().setInformationText(name + " moved to (" + x + "," + y + ")");
		ctx.setState(new WaitingState(ctx));
	}
}
