package controller;

public interface ContextSwitchListener {
	
	// swap to the alternative state
	void switchState();
	void resetState();
}
