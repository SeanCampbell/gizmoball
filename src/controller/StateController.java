package controller;

import controller.buildmode.BuildModeStateMachine;
import controller.runmode.RunModeStateMachine;
import model.IGameBoard;

public class StateController implements ContextSwitchListener {
	
	private IController currentCtrl;
	private IGameBoard model;
	private IGameBoard runModel;
	private State currentState;
	
	public StateController(IGameBoard model) {
		this.model = model;
		currentCtrl = new BuildModeStateMachine(this, model);
		currentState = State.BUILD_MODE;
	}
	
	public void switchState() {
		
		switch (currentState) {
		case BUILD_MODE:
			runModel = model;
			currentCtrl.stop();
			currentCtrl = new RunModeStateMachine(this, runModel);
			currentState = State.RUN_MODE;
			break;
			
		case RUN_MODE:
			currentCtrl.stop();
			currentCtrl = new BuildModeStateMachine(this, model);
			currentState = State.BUILD_MODE;
			break;
		}
	}
	
	public void setModel(IGameBoard model) {
		this.model = model;
		currentCtrl.setModel(model);
	}
	
	/**
	 * Method to reset the state of the gameboard so that it contains no gizmos.
	 */
	public void resetState() {
		model.clearBoard();
	}
	
	enum State {
		BUILD_MODE,
		RUN_MODE
	}
}
