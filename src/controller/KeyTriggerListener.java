package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import model.ITriggerManager;

public class KeyTriggerListener implements KeyListener {

	private ITriggerManager tm;
	
	public KeyTriggerListener(ITriggerManager tm){
		this.tm = tm;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		String s = KeyEvent.getKeyText(e.getExtendedKeyCode());
		System.out.printf("%s pressed\n", s);
		tm.onKeyEvent(e.getKeyCode(), false);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		String s = KeyEvent.getKeyText(e.getExtendedKeyCode());
		System.out.printf("%s released\n", s);
		tm.onKeyEvent(e.getKeyCode(), true);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// We don't really care at run mode runtime
	}
}