package controller;

import shapes.LogicalPoint;

public interface DragEventListener {
	void onDragBegin(LogicalPoint pt);
	void onDragEnd(LogicalPoint pt);
	void onDragUpdate(LogicalPoint pt);
}
