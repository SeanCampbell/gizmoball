package parsing;

import model.IGameBoard;

/**
 * The basic methods which are expected to exist in any implementation
 * of {@link ILoader}.
 * It is expected that instances will be best-effort loaders.
 */
public interface ILoader {
	/**
	 * This is the central method of the class used to load a gizmoball game-board file.
	 * @param fileName the absolute file path
	 * @param gb a concrete instance of IGameBoard; no assumption made of contents of gb
	 */
	void load(final String fileName, final IGameBoard gb);
	/**
	 * Get the number of errors which occurred while parsing.
	 * @return number of parsing errors
	 */
	int getCountParsingErrors();
	/**
	 * Get detailed information of all errors which occurred while parsing.
	 * @return a string detailing all parsing errors recovered from
	 */
	String getMessageParsingErrors();
	/**
	 * Get the number of errors which occurred while generating the game-board.
	 * @return number of game-board errors
	 */
	int getCountGameBoardErrors();
	/**
	 * Get detailed information of all errors which occurred while generating
	 * the game-board.
	 * @return a string detailing all game-board errors recovered from
	 */
	String getMessageGameBoardErrors();
	/**
	 * Fetch the GameBoard which has been generated upon load.
	 * Requires: Call to load before calling.
	 * @return an instance of IGameBoard which has been loaded from file
	 */
	IGameBoard getGameBoard();
}