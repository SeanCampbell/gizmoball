// Generated from C:\JavaLib\GizmoFile.g4 by ANTLR 4.1
package parsing;

import model.IGameBoard;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;


/**
 * This class provides an implementation of {@link GizmoFileListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class GizmoFileBaseListener implements GizmoFileListener {
	private static final String ERR = "Gameboard generation failure due to line: ";
	
	private StringBuilder error_builder;
	private int error_count;
	private IGameBoard gb;
	
	@Override 
	public void enterMakestandardcommand(@NotNull GizmoFileParser.MakestandardcommandContext ctx) { 
		try{
			final String type = ctx.GIZMO().getText(), name = ctx.IDENTIFIER().getText();
			final int x = Integer.parseInt(ctx.intpair().INTEGER(0).getSymbol().getText()), 
					y = Integer.parseInt(ctx.intpair().INTEGER(1).getSymbol().getText());
			this.gb.addGizmo(type, name, x, y);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		} 
	}

	@Override public void exitMakestandardcommand(@NotNull GizmoFileParser.MakestandardcommandContext ctx) { }

	@Override
	public void enterMovestandardcommand(@NotNull GizmoFileParser.MovestandardcommandContext ctx) {
		try{
			final String name = ctx.IDENTIFIER().getSymbol().getText();
			final int x = Integer.parseInt(ctx.intpair().INTEGER(0).getSymbol().getText()), 
					y = Integer.parseInt(ctx.intpair().INTEGER(1).getSymbol().getText());
			this.gb.moveGizmo(name, x, y);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
	}

	@Override public void exitMovestandardcommand(@NotNull GizmoFileParser.MovestandardcommandContext ctx) { }


	@Override
	public void enterMoveballcommand(@NotNull GizmoFileParser.MoveballcommandContext ctx) {
		try{
			final String name = ctx.IDENTIFIER().getSymbol().getText();
			final double x = Double.parseDouble(ctx.floatpair().FLOAT(0).getSymbol().getText()),
					y = Double.parseDouble(ctx.floatpair().FLOAT(1).getSymbol().getText());
			this.gb.moveGizmoBall(name, x, y);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
	}

	@Override public void exitMoveballcommand(@NotNull GizmoFileParser.MoveballcommandContext ctx) { }

	@Override
	public void enterFriccommand(@NotNull GizmoFileParser.FriccommandContext ctx) {
		try{
			final double mu = Double.parseDouble(ctx.floatpair().FLOAT(0).getSymbol().getText()),
					mu2 = Double.parseDouble(ctx.floatpair().FLOAT(1).getSymbol().getText());
			this.gb.setFriction(mu, mu2);
		} catch(Exception e){
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
	}

	@Override public void exitFriccommand(@NotNull GizmoFileParser.FriccommandContext ctx) { }
	

	@Override
	public void enterMakeabscommand(@NotNull GizmoFileParser.MakeabscommandContext ctx) {
		try{
			final String type = ctx.ABSORBER().getSymbol().getText(),
					name = ctx.IDENTIFIER().getSymbol().getText();
			final int x = Integer.parseInt(ctx.intpair(0).INTEGER(0).getSymbol().getText()), 
					y = Integer.parseInt(ctx.intpair(0).INTEGER(1).getSymbol().getText());
			final int dx = Integer.parseInt(ctx.intpair(1).INTEGER(0).getSymbol().getText()), 
					dy = Integer.parseInt(ctx.intpair(1).INTEGER(1).getSymbol().getText());
			this.gb.addGizmo(type, name, x, y, dx, dy);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
		
	}

	@Override public void exitMakeabscommand(@NotNull GizmoFileParser.MakeabscommandContext ctx) { }

	@Override
	public void enterRemovecommand(@NotNull GizmoFileParser.RemovecommandContext ctx) {
		try{
			final String name = ctx.IDENTIFIER().getSymbol().getText();
			if(this.gb.hasGizmo(name))
				this.gb.removeGizmo(name);
			else 
				this.gb.removeGizmoBall(name);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
		
	}

	@Override public void exitRemovecommand(@NotNull GizmoFileParser.RemovecommandContext ctx) { }

	@Override public void enterKeyid(@NotNull GizmoFileParser.KeyidContext ctx) { }

	@Override public void exitKeyid(@NotNull GizmoFileParser.KeyidContext ctx) { }

	@Override public void enterFloatpair(@NotNull GizmoFileParser.FloatpairContext ctx) { }

	@Override public void exitFloatpair(@NotNull GizmoFileParser.FloatpairContext ctx) { }

	@Override public void enterIntpair(@NotNull GizmoFileParser.IntpairContext ctx) { }

	@Override public void exitIntpair(@NotNull GizmoFileParser.IntpairContext ctx) { }

	@Override
	public void enterRotatecommand(@NotNull GizmoFileParser.RotatecommandContext ctx) { 
		try{
			final String name = ctx.IDENTIFIER().getSymbol().getText();
			this.gb.rotateGizmo(name);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
	}

	@Override public void exitRotatecommand(@NotNull GizmoFileParser.RotatecommandContext ctx) { }


	@Override 
	public void enterFile(@NotNull GizmoFileParser.FileContext ctx) { 
		this.error_builder = new StringBuilder();
		this.error_count = 0;
	}

	@Override public void exitFile(@NotNull GizmoFileParser.FileContext ctx) { }


	@Override
	public void enterKeyconnectcommand(@NotNull GizmoFileParser.KeyconnectcommandContext ctx) {
		try{
			final String name = ctx.IDENTIFIER().getSymbol().getText();
			final int key = Integer.parseInt(ctx.keyid().INTEGER().getSymbol().getText());
			final String direction = ctx.keyid().DIRECTION().getSymbol().getText();
			boolean dir;
			// Not good, I know but the lexer is too clever for me and doesn't maintain a reference
			// to multiple values defined under the same literal.
			if(direction.equals("UP"))
				dir = false;
			else
				dir = true;
			this.gb.connect(key, name, dir);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
	}

	@Override public void exitKeyconnectcommand(@NotNull GizmoFileParser.KeyconnectcommandContext ctx) { }


	@Override
	public void enterGravcommand(@NotNull GizmoFileParser.GravcommandContext ctx) {
		try{
			final double grav = Double.parseDouble(ctx.FLOAT().getSymbol().getText());
			this.gb.setGravity(grav);
		}  catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
	}

	@Override public void exitGravcommand(@NotNull GizmoFileParser.GravcommandContext ctx) { }


	@Override
	public void enterGizconnectcommand(@NotNull GizmoFileParser.GizconnectcommandContext ctx) {
		try{
			final String giz1 = ctx.IDENTIFIER(0).getSymbol().getText(),
					giz2 = ctx.IDENTIFIER(1).getSymbol().getText();
			this.gb.connect(giz1, giz2);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
	}

	@Override public void exitGizconnectcommand(@NotNull GizmoFileParser.GizconnectcommandContext ctx) { }

	@Override
	public void enterMakeballcommand(@NotNull GizmoFileParser.MakeballcommandContext ctx) {
		try{
			final String name = ctx.IDENTIFIER().getSymbol().getText();
			final double x = Double.parseDouble(ctx.floatpair(0).FLOAT(0).getSymbol().getText()), 
					y = Double.parseDouble(ctx.floatpair(0).FLOAT(1).getSymbol().getText());
			final double vx = Double.parseDouble(ctx.floatpair(1).FLOAT(0).getSymbol().getText()), 
					vy = Double.parseDouble(ctx.floatpair(1).FLOAT(1).getSymbol().getText());
			this.gb.addGizmoBall(name, x, y, vx, vy);
		} catch(Exception e) {
			this.error_builder.append(GizmoFileBaseListener.ERR + ctx.getStart().getLine() + "\n");
			this.error_builder.append(e.getMessage() + "\n");
			this.error_count++;
		}
	}

	@Override public void exitMakeballcommand(@NotNull GizmoFileParser.MakeballcommandContext ctx) { }

	@Override public void enterEveryRule(@NotNull ParserRuleContext ctx) { }

	@Override public void exitEveryRule(@NotNull ParserRuleContext ctx) { }

	@Override public void visitTerminal(@NotNull TerminalNode node) { }

	@Override public void visitErrorNode(@NotNull ErrorNode node) {
	}
	
	@Override
	public IGameBoard getGameBoard() {
		return this.gb;
	}
	@Override
	public void setGameBoard(IGameBoard gb) {
		this.gb = gb;
	}
	@Override
	public int getErrorCount() {
		return this.error_count;
	}
	@Override
	public String getErrorMessages() {
		return this.error_builder.toString();
	}
}