package parsing;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
/**
 * A simple listener employed by the Parser to recognise the occurrence of
 * syntax errors.  Error information is detailed since it is capable of
 * identifying the offending line number and crucially exactly what the 
 * parser was expecting.
 */
public class ErrorListener extends BaseErrorListener {
    protected StringBuilder errMessage;
	protected int errorCount;
	
	public ErrorListener(){
		this.errMessage = new StringBuilder();
		this.errorCount = 0;
    }
    
    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine,
    		String msg, RecognitionException e) {
        this.errorCount++;
        this.errMessage.append(msg + " on line:" + line + "\n");
     }
}