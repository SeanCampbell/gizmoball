/*
 * Basic grammar file written in Antlr4.
 * This can generate the parser and lexer for Java directly.
 */
 
grammar GizmoFile;

options{
    language = Java;
}

file :    (makestandardcommand  |
           makeabscommand       |
           makeballcommand      |
           movestandardcommand  |
           moveballcommand  	|
           rotatecommand        |
           removecommand        |
           keyconnectcommand    |
           gizconnectcommand    |
           friccommand          |
           gravcommand      )*  ;

makestandardcommand: GIZMO IDENTIFIER intpair;

makeabscommand: ABSORBER IDENTIFIER intpair intpair;

makeballcommand: BALL IDENTIFIER floatpair floatpair;

movestandardcommand: MOVE IDENTIFIER intpair;

moveballcommand: MOVE IDENTIFIER floatpair;

rotatecommand: ROTATE IDENTIFIER;

removecommand: REMOVE IDENTIFIER;

gizconnectcommand: CONNECT IDENTIFIER IDENTIFIER;

keyconnectcommand: KEYCONNECT keyid IDENTIFIER;

gravcommand : GRAVITY FLOAT;

friccommand : FRICTION floatpair;

intpair : INTEGER INTEGER;

floatpair : FLOAT FLOAT;

keyid : KEY INTEGER DIRECTION;

//Lexicon
//Literals
ABSORBER : 'Absorber';

BALL : 'Ball';

CONNECT : 'Connect';

FRICTION : 'Friction';

GIZMO : ('Square'         | 
          'Circle'        | 
          'Triangle'      | 
          'RightFlipper'  |
          'LeftFlipper'   |
          'GravityWell'    );

GRAVITY : 'Gravity';

KEYCONNECT : 'KeyConnect';

MOVE : 'Move';

ROTATE : 'Rotate';

REMOVE : 'Remove';

KEY : 'key';

DIRECTION: ('up' | 'down');

//Useful constructs
FLOAT : (DIGIT)+'.'(DIGIT)+;

INTEGER : (DIGIT)+;

IDENTIFIER : (LETTER_OR_DIGIT_OR_UNDER)+;

SPACE : [' ' | '\t']+  -> skip;

END : '\r'?'\n' -> skip;

fragment LETTER_OR_DIGIT_OR_UNDER : LETTER | DIGIT | '_';

fragment LETTER : 'a'..'z' | 'A'..'Z';

fragment DIGIT  : '0'..'9';