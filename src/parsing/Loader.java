package parsing;

import model.IGameBoard;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

/**
 * This is a best attempt loader; it employs {@link GizmoFileBaseListener, @link ErrorListener}.
 * It is best attempt in that it recovers in-line from any parsing exceptions which
 * arise, i.e. syntax errors, and continues through game-board exceptions which arise.
 * {@link GizmoFileBaseListener} possesses methods which allow error reporting with respect to
 * the game-board.
 * {@link ErrorListener} possesses protected members which allow error reporting with respect to
 * the parser itself.  The line numbers supplied may be off by a single line in the case of a
 * missing token.
 */
public class Loader implements ILoader{
	private final ParseTreeWalker walker;
	private final GizmoFileBaseListener gfl;
	private final ErrorListener el;

	public Loader(){
		this.walker = new ParseTreeWalker();
		this.gfl = new GizmoFileBaseListener();
		this.el = new ErrorListener();
	}

	public void load(final String fileName, final IGameBoard gb){
		try {
			java.io.Reader reader = new java.io.InputStreamReader(new java.io.FileInputStream(new java.io.File(fileName)));
			try{
				ANTLRInputStream input = new ANTLRInputStream(reader);
			
				GizmoFileLexer lexer = new GizmoFileLexer(input);
				
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				
				GizmoFileParser parser = new GizmoFileParser(tokens);
				// Add custom error listener to allow syntax error reporting to controller
				parser.removeErrorListeners();
				parser.addErrorListener(this.el);
				ParseTree tree = parser.file();
				
				gfl.setGameBoard(gb);
					
				walker.walk(gfl, tree);
			} finally{
				reader.close();
			}
		} catch(java.io.FileNotFoundException e){
			System.out.println("Couldn't find the file: " + fileName);
		} catch (java.io.IOException e) {
			e.printStackTrace();
		}
	}
	
	public IGameBoard getGameBoard(){
		return gfl.getGameBoard();
	}
	
	public int getCountGameBoardErrors(){
		return gfl.getErrorCount();
	}
	
	public String getMessageGameBoardErrors(){
		return gfl.getErrorMessages();
	}

	@Override
	public int getCountParsingErrors() {
		return this.el.errorCount;
	}

	@Override
	public String getMessageParsingErrors() {
		return this.el.errMessage.toString();
	}
}
