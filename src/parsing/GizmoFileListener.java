// Generated from C:\JavaLib\GizmoFile.g4 by ANTLR 4.1
package parsing;
import model.IGameBoard;

import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link GizmoFileParser}.
 */
public interface GizmoFileListener extends ParseTreeListener {
	/**
	 * Fetch an instance of the GameBoard which has been generated after Parsing.
	 * (Invariant: This method can only be called after a call to Load)
	 * @return an IGameBoard
	 */
	IGameBoard getGameBoard();
	/**
	 * Set the instance of IGameBoard which the Parser is to employ.
	 * @param gb the IGameBoard instance
	 */
	void setGameBoard(IGameBoard gb);
	/**
	 * Fetch the number of errors which occurred in performing operations
	 * upon the GameBoard.
	 * @return the number of errors which occurred on the last load attempt
	 */
	int getErrorCount();
	/**
	 * Fetch a useful message which can inform a user on the errors which were
	 * encountered in generating the GameBoard as well as the suspected reasons
	 * for said errors.
	 * @return a String containing the error messages which were generated on
	 * the last load attempt
	 */
	String getErrorMessages();
	/**
	 * Make the standard gizmo types; this includes SquareBumper, TriangleBumper, CircleBumper,
	 * LeftFlipper and RightFlipper.
	 * Context holds information on the type of gizmo, the desired name and an integer pair.
	 */
	void enterMakestandardcommand(@NotNull GizmoFileParser.MakestandardcommandContext ctx) throws RecognitionException;
	void exitMakestandardcommand(@NotNull GizmoFileParser.MakestandardcommandContext ctx);

	/**
	 * Move any type of non-ball gizmo; this includes SquareBumper, TriangleBumper, CircleBumper,
	 * LeftFlipper, RightFlipper and Absorber.
	 * Context holds the name of the gizmo to move and an integer pair.
	 */
	void enterMovestandardcommand(@NotNull GizmoFileParser.MovestandardcommandContext ctx);
	void exitMovestandardcommand(@NotNull GizmoFileParser.MovestandardcommandContext ctx);

	/**
	 * Move any type of gizmo-ball.
	 * Context holds the name of the gizmo to move and a float pair.
	 */
	void enterMoveballcommand(@NotNull GizmoFileParser.MoveballcommandContext ctx);
	void exitMoveballcommand(@NotNull GizmoFileParser.MoveballcommandContext ctx);

	/**
	 * Set the values of friction for the game-board.
	 * Context holds two floats.
	 */
	void enterFriccommand(@NotNull GizmoFileParser.FriccommandContext ctx);
	void exitFriccommand(@NotNull GizmoFileParser.FriccommandContext ctx);

	/**
	 * Make an absorber.  (Segregated since Lexer would interpret SquareGizmo X 1 2 3 4 as valid)
	 * Context holds the name of the absorber and two integer pairs.
	 */
	void enterMakeabscommand(@NotNull GizmoFileParser.MakeabscommandContext ctx);
	void exitMakeabscommand(@NotNull GizmoFileParser.MakeabscommandContext ctx);

	/**
	 * Remove a gizmo or gizmoball from the gameboard.
	 * Context holds the name of the item to remove.
	 */
	void enterRemovecommand(@NotNull GizmoFileParser.RemovecommandContext ctx);
	void exitRemovecommand(@NotNull GizmoFileParser.RemovecommandContext ctx);

	/**
	 * Context holds an integer associated with a key-board key and a String denoting the direction,
	 * namely a value of 'up' or 'down'.
	 */
	void enterKeyid(@NotNull GizmoFileParser.KeyidContext ctx);
	void exitKeyid(@NotNull GizmoFileParser.KeyidContext ctx);

	/**
	 * Context holds two floating point numbers.
	 */
	void enterFloatpair(@NotNull GizmoFileParser.FloatpairContext ctx);
	void exitFloatpair(@NotNull GizmoFileParser.FloatpairContext ctx);

	/**
	 * Context holds two integer numbers.
	 */
	void enterIntpair(@NotNull GizmoFileParser.IntpairContext ctx);
	void exitIntpair(@NotNull GizmoFileParser.IntpairContext ctx);

	/**
	 * Rotate a gizmo located on the game-board.
	 * Context holds the name of the gizmo to rotate.
	 */
	void enterRotatecommand(@NotNull GizmoFileParser.RotatecommandContext ctx);
	void exitRotatecommand(@NotNull GizmoFileParser.RotatecommandContext ctx);

	/**
	 * Entry to the file.  Suggested usage to define variable which can be used throughout
	 * traversing the tree as (invariant) this rule is called first.
	 */
	void enterFile(@NotNull GizmoFileParser.FileContext ctx);
	/**
	 * Exit from file.  Optional usage for reporting.
	 */
	void exitFile(@NotNull GizmoFileParser.FileContext ctx);

	/**
	 * Connect a specific key to a specific gizmo.
	 * Context holds a key-id and a gizmo name.
	 */
	void enterKeyconnectcommand(@NotNull GizmoFileParser.KeyconnectcommandContext ctx);
	void exitKeyconnectcommand(@NotNull GizmoFileParser.KeyconnectcommandContext ctx);

	/**
	 * Set the values of gravity for the game-board.
	 * Context holds a single floating point number.
	 */
	void enterGravcommand(@NotNull GizmoFileParser.GravcommandContext ctx);
	void exitGravcommand(@NotNull GizmoFileParser.GravcommandContext ctx);

	/**
	 * Connect two specific gizmo together.
	 * Context holds the identifier of the first gizmo to act as the source and
	 * the identifier of another gizmo whose action is to be triggered due to action on source.
	 */
	void enterGizconnectcommand(@NotNull GizmoFileParser.GizconnectcommandContext ctx);
	void exitGizconnectcommand(@NotNull GizmoFileParser.GizconnectcommandContext ctx);

	/**
	 * Make the standard gizmo-ball.
	 * Context holds information on the the desired name, a float pair specifying the initial
	 * co-ordinates and a float pair holding the initial velocity.
	 */
	void enterMakeballcommand(@NotNull GizmoFileParser.MakeballcommandContext ctx);
	void exitMakeballcommand(@NotNull GizmoFileParser.MakeballcommandContext ctx);
}