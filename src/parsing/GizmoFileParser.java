// Generated from C:\JavaLib\GizmoFile.g4 by ANTLR 4.1
package parsing;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GizmoFileParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		ABSORBER=1, BALL=2, CONNECT=3, FRICTION=4, GIZMO=5, GRAVITY=6, KEYCONNECT=7, 
		MOVE=8, ROTATE=9, REMOVE=10, KEY=11, DIRECTION=12, FLOAT=13, INTEGER=14, 
		IDENTIFIER=15, SPACE=16, END=17;
	public static final String[] tokenNames = {
		"<INVALID>", "'Absorber'", "'Ball'", "'Connect'", "'Friction'", "GIZMO", 
		"'Gravity'", "'KeyConnect'", "'Move'", "'Rotate'", "'Remove'", "'key'", 
		"DIRECTION", "FLOAT", "INTEGER", "IDENTIFIER", "SPACE", "END"
	};
	public static final int
		RULE_file = 0, RULE_makestandardcommand = 1, RULE_makeabscommand = 2, 
		RULE_makeballcommand = 3, RULE_movestandardcommand = 4, RULE_moveballcommand = 5, 
		RULE_rotatecommand = 6, RULE_removecommand = 7, RULE_gizconnectcommand = 8, 
		RULE_keyconnectcommand = 9, RULE_gravcommand = 10, RULE_friccommand = 11, 
		RULE_intpair = 12, RULE_floatpair = 13, RULE_keyid = 14;
	public static final String[] ruleNames = {
		"file", "makestandardcommand", "makeabscommand", "makeballcommand", "movestandardcommand", 
		"moveballcommand", "rotatecommand", "removecommand", "gizconnectcommand", 
		"keyconnectcommand", "gravcommand", "friccommand", "intpair", "floatpair", 
		"keyid"
	};

	@Override
	public String getGrammarFileName() { return "GizmoFile.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public GizmoFileParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class FileContext extends ParserRuleContext {
		public MovestandardcommandContext movestandardcommand(int i) {
			return getRuleContext(MovestandardcommandContext.class,i);
		}
		public List<KeyconnectcommandContext> keyconnectcommand() {
			return getRuleContexts(KeyconnectcommandContext.class);
		}
		public RemovecommandContext removecommand(int i) {
			return getRuleContext(RemovecommandContext.class,i);
		}
		public List<RemovecommandContext> removecommand() {
			return getRuleContexts(RemovecommandContext.class);
		}
		public List<FriccommandContext> friccommand() {
			return getRuleContexts(FriccommandContext.class);
		}
		public MakeabscommandContext makeabscommand(int i) {
			return getRuleContext(MakeabscommandContext.class,i);
		}
		public List<MovestandardcommandContext> movestandardcommand() {
			return getRuleContexts(MovestandardcommandContext.class);
		}
		public List<RotatecommandContext> rotatecommand() {
			return getRuleContexts(RotatecommandContext.class);
		}
		public List<MakeballcommandContext> makeballcommand() {
			return getRuleContexts(MakeballcommandContext.class);
		}
		public MoveballcommandContext moveballcommand(int i) {
			return getRuleContext(MoveballcommandContext.class,i);
		}
		public GravcommandContext gravcommand(int i) {
			return getRuleContext(GravcommandContext.class,i);
		}
		public List<MakestandardcommandContext> makestandardcommand() {
			return getRuleContexts(MakestandardcommandContext.class);
		}
		public MakestandardcommandContext makestandardcommand(int i) {
			return getRuleContext(MakestandardcommandContext.class,i);
		}
		public List<MoveballcommandContext> moveballcommand() {
			return getRuleContexts(MoveballcommandContext.class);
		}
		public MakeballcommandContext makeballcommand(int i) {
			return getRuleContext(MakeballcommandContext.class,i);
		}
		public GizconnectcommandContext gizconnectcommand(int i) {
			return getRuleContext(GizconnectcommandContext.class,i);
		}
		public List<GravcommandContext> gravcommand() {
			return getRuleContexts(GravcommandContext.class);
		}
		public FriccommandContext friccommand(int i) {
			return getRuleContext(FriccommandContext.class,i);
		}
		public List<GizconnectcommandContext> gizconnectcommand() {
			return getRuleContexts(GizconnectcommandContext.class);
		}
		public KeyconnectcommandContext keyconnectcommand(int i) {
			return getRuleContext(KeyconnectcommandContext.class,i);
		}
		public RotatecommandContext rotatecommand(int i) {
			return getRuleContext(RotatecommandContext.class,i);
		}
		public List<MakeabscommandContext> makeabscommand() {
			return getRuleContexts(MakeabscommandContext.class);
		}
		public FileContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_file; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterFile(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitFile(this);
		}
	}

	public final FileContext file() throws RecognitionException {
		FileContext _localctx = new FileContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_file);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ABSORBER) | (1L << BALL) | (1L << CONNECT) | (1L << FRICTION) | (1L << GIZMO) | (1L << GRAVITY) | (1L << KEYCONNECT) | (1L << MOVE) | (1L << ROTATE) | (1L << REMOVE))) != 0)) {
				{
				setState(41);
				switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
				case 1:
					{
					setState(30); makestandardcommand();
					}
					break;

				case 2:
					{
					setState(31); makeabscommand();
					}
					break;

				case 3:
					{
					setState(32); makeballcommand();
					}
					break;

				case 4:
					{
					setState(33); movestandardcommand();
					}
					break;

				case 5:
					{
					setState(34); moveballcommand();
					}
					break;

				case 6:
					{
					setState(35); rotatecommand();
					}
					break;

				case 7:
					{
					setState(36); removecommand();
					}
					break;

				case 8:
					{
					setState(37); keyconnectcommand();
					}
					break;

				case 9:
					{
					setState(38); gizconnectcommand();
					}
					break;

				case 10:
					{
					setState(39); friccommand();
					}
					break;

				case 11:
					{
					setState(40); gravcommand();
					}
					break;
				}
				}
				setState(45);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MakestandardcommandContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(GizmoFileParser.IDENTIFIER, 0); }
		public IntpairContext intpair() {
			return getRuleContext(IntpairContext.class,0);
		}
		public TerminalNode GIZMO() { return getToken(GizmoFileParser.GIZMO, 0); }
		public MakestandardcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_makestandardcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterMakestandardcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitMakestandardcommand(this);
		}
	}

	public final MakestandardcommandContext makestandardcommand() throws RecognitionException {
		MakestandardcommandContext _localctx = new MakestandardcommandContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_makestandardcommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46); match(GIZMO);
			setState(47); match(IDENTIFIER);
			setState(48); intpair();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MakeabscommandContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(GizmoFileParser.IDENTIFIER, 0); }
		public List<IntpairContext> intpair() {
			return getRuleContexts(IntpairContext.class);
		}
		public IntpairContext intpair(int i) {
			return getRuleContext(IntpairContext.class,i);
		}
		public TerminalNode ABSORBER() { return getToken(GizmoFileParser.ABSORBER, 0); }
		public MakeabscommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_makeabscommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterMakeabscommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitMakeabscommand(this);
		}
	}

	public final MakeabscommandContext makeabscommand() throws RecognitionException {
		MakeabscommandContext _localctx = new MakeabscommandContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_makeabscommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50); match(ABSORBER);
			setState(51); match(IDENTIFIER);
			setState(52); intpair();
			setState(53); intpair();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MakeballcommandContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(GizmoFileParser.IDENTIFIER, 0); }
		public FloatpairContext floatpair(int i) {
			return getRuleContext(FloatpairContext.class,i);
		}
		public List<FloatpairContext> floatpair() {
			return getRuleContexts(FloatpairContext.class);
		}
		public TerminalNode BALL() { return getToken(GizmoFileParser.BALL, 0); }
		public MakeballcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_makeballcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterMakeballcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitMakeballcommand(this);
		}
	}

	public final MakeballcommandContext makeballcommand() throws RecognitionException {
		MakeballcommandContext _localctx = new MakeballcommandContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_makeballcommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(55); match(BALL);
			setState(56); match(IDENTIFIER);
			setState(57); floatpair();
			setState(58); floatpair();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MovestandardcommandContext extends ParserRuleContext {
		public TerminalNode MOVE() { return getToken(GizmoFileParser.MOVE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(GizmoFileParser.IDENTIFIER, 0); }
		public IntpairContext intpair() {
			return getRuleContext(IntpairContext.class,0);
		}
		public MovestandardcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_movestandardcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterMovestandardcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitMovestandardcommand(this);
		}
	}

	public final MovestandardcommandContext movestandardcommand() throws RecognitionException {
		MovestandardcommandContext _localctx = new MovestandardcommandContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_movestandardcommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(60); match(MOVE);
			setState(61); match(IDENTIFIER);
			setState(62); intpair();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MoveballcommandContext extends ParserRuleContext {
		public TerminalNode MOVE() { return getToken(GizmoFileParser.MOVE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(GizmoFileParser.IDENTIFIER, 0); }
		public FloatpairContext floatpair() {
			return getRuleContext(FloatpairContext.class,0);
		}
		public MoveballcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_moveballcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterMoveballcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitMoveballcommand(this);
		}
	}

	public final MoveballcommandContext moveballcommand() throws RecognitionException {
		MoveballcommandContext _localctx = new MoveballcommandContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_moveballcommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(64); match(MOVE);
			setState(65); match(IDENTIFIER);
			setState(66); floatpair();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RotatecommandContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(GizmoFileParser.IDENTIFIER, 0); }
		public TerminalNode ROTATE() { return getToken(GizmoFileParser.ROTATE, 0); }
		public RotatecommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rotatecommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterRotatecommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitRotatecommand(this);
		}
	}

	public final RotatecommandContext rotatecommand() throws RecognitionException {
		RotatecommandContext _localctx = new RotatecommandContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_rotatecommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(68); match(ROTATE);
			setState(69); match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class RemovecommandContext extends ParserRuleContext {
		public TerminalNode REMOVE() { return getToken(GizmoFileParser.REMOVE, 0); }
		public TerminalNode IDENTIFIER() { return getToken(GizmoFileParser.IDENTIFIER, 0); }
		public RemovecommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_removecommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterRemovecommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitRemovecommand(this);
		}
	}

	public final RemovecommandContext removecommand() throws RecognitionException {
		RemovecommandContext _localctx = new RemovecommandContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_removecommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(71); match(REMOVE);
			setState(72); match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GizconnectcommandContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER(int i) {
			return getToken(GizmoFileParser.IDENTIFIER, i);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(GizmoFileParser.IDENTIFIER); }
		public TerminalNode CONNECT() { return getToken(GizmoFileParser.CONNECT, 0); }
		public GizconnectcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_gizconnectcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterGizconnectcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitGizconnectcommand(this);
		}
	}

	public final GizconnectcommandContext gizconnectcommand() throws RecognitionException {
		GizconnectcommandContext _localctx = new GizconnectcommandContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_gizconnectcommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(74); match(CONNECT);
			setState(75); match(IDENTIFIER);
			setState(76); match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyconnectcommandContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(GizmoFileParser.IDENTIFIER, 0); }
		public KeyidContext keyid() {
			return getRuleContext(KeyidContext.class,0);
		}
		public TerminalNode KEYCONNECT() { return getToken(GizmoFileParser.KEYCONNECT, 0); }
		public KeyconnectcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyconnectcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterKeyconnectcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitKeyconnectcommand(this);
		}
	}

	public final KeyconnectcommandContext keyconnectcommand() throws RecognitionException {
		KeyconnectcommandContext _localctx = new KeyconnectcommandContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_keyconnectcommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(78); match(KEYCONNECT);
			setState(79); keyid();
			setState(80); match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class GravcommandContext extends ParserRuleContext {
		public TerminalNode FLOAT() { return getToken(GizmoFileParser.FLOAT, 0); }
		public TerminalNode GRAVITY() { return getToken(GizmoFileParser.GRAVITY, 0); }
		public GravcommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_gravcommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterGravcommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitGravcommand(this);
		}
	}

	public final GravcommandContext gravcommand() throws RecognitionException {
		GravcommandContext _localctx = new GravcommandContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_gravcommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(82); match(GRAVITY);
			setState(83); match(FLOAT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FriccommandContext extends ParserRuleContext {
		public FloatpairContext floatpair() {
			return getRuleContext(FloatpairContext.class,0);
		}
		public TerminalNode FRICTION() { return getToken(GizmoFileParser.FRICTION, 0); }
		public FriccommandContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_friccommand; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterFriccommand(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitFriccommand(this);
		}
	}

	public final FriccommandContext friccommand() throws RecognitionException {
		FriccommandContext _localctx = new FriccommandContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_friccommand);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(85); match(FRICTION);
			setState(86); floatpair();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IntpairContext extends ParserRuleContext {
		public TerminalNode INTEGER(int i) {
			return getToken(GizmoFileParser.INTEGER, i);
		}
		public List<TerminalNode> INTEGER() { return getTokens(GizmoFileParser.INTEGER); }
		public IntpairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_intpair; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterIntpair(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitIntpair(this);
		}
	}

	public final IntpairContext intpair() throws RecognitionException {
		IntpairContext _localctx = new IntpairContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_intpair);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(88); match(INTEGER);
			setState(89); match(INTEGER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FloatpairContext extends ParserRuleContext {
		public List<TerminalNode> FLOAT() { return getTokens(GizmoFileParser.FLOAT); }
		public TerminalNode FLOAT(int i) {
			return getToken(GizmoFileParser.FLOAT, i);
		}
		public FloatpairContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_floatpair; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterFloatpair(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitFloatpair(this);
		}
	}

	public final FloatpairContext floatpair() throws RecognitionException {
		FloatpairContext _localctx = new FloatpairContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_floatpair);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(91); match(FLOAT);
			setState(92); match(FLOAT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class KeyidContext extends ParserRuleContext {
		public TerminalNode KEY() { return getToken(GizmoFileParser.KEY, 0); }
		public TerminalNode DIRECTION() { return getToken(GizmoFileParser.DIRECTION, 0); }
		public TerminalNode INTEGER() { return getToken(GizmoFileParser.INTEGER, 0); }
		public KeyidContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_keyid; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).enterKeyid(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof GizmoFileListener ) ((GizmoFileListener)listener).exitKeyid(this);
		}
	}

	public final KeyidContext keyid() throws RecognitionException {
		KeyidContext _localctx = new KeyidContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_keyid);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(94); match(KEY);
			setState(95); match(INTEGER);
			setState(96); match(DIRECTION);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\23e\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4"+
		"\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\3\2\3\2\3\2\7\2,\n\2\f\2\16\2/\13\2\3\3\3\3\3\3\3\3\3\4\3\4\3\4"+
		"\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3"+
		"\b\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\r\3\r"+
		"\3\r\3\16\3\16\3\16\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\2\21\2\4\6"+
		"\b\n\f\16\20\22\24\26\30\32\34\36\2\2`\2-\3\2\2\2\4\60\3\2\2\2\6\64\3"+
		"\2\2\2\b9\3\2\2\2\n>\3\2\2\2\fB\3\2\2\2\16F\3\2\2\2\20I\3\2\2\2\22L\3"+
		"\2\2\2\24P\3\2\2\2\26T\3\2\2\2\30W\3\2\2\2\32Z\3\2\2\2\34]\3\2\2\2\36"+
		"`\3\2\2\2 ,\5\4\3\2!,\5\6\4\2\",\5\b\5\2#,\5\n\6\2$,\5\f\7\2%,\5\16\b"+
		"\2&,\5\20\t\2\',\5\24\13\2(,\5\22\n\2),\5\30\r\2*,\5\26\f\2+ \3\2\2\2"+
		"+!\3\2\2\2+\"\3\2\2\2+#\3\2\2\2+$\3\2\2\2+%\3\2\2\2+&\3\2\2\2+\'\3\2\2"+
		"\2+(\3\2\2\2+)\3\2\2\2+*\3\2\2\2,/\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\3\3\2"+
		"\2\2/-\3\2\2\2\60\61\7\7\2\2\61\62\7\21\2\2\62\63\5\32\16\2\63\5\3\2\2"+
		"\2\64\65\7\3\2\2\65\66\7\21\2\2\66\67\5\32\16\2\678\5\32\16\28\7\3\2\2"+
		"\29:\7\4\2\2:;\7\21\2\2;<\5\34\17\2<=\5\34\17\2=\t\3\2\2\2>?\7\n\2\2?"+
		"@\7\21\2\2@A\5\32\16\2A\13\3\2\2\2BC\7\n\2\2CD\7\21\2\2DE\5\34\17\2E\r"+
		"\3\2\2\2FG\7\13\2\2GH\7\21\2\2H\17\3\2\2\2IJ\7\f\2\2JK\7\21\2\2K\21\3"+
		"\2\2\2LM\7\5\2\2MN\7\21\2\2NO\7\21\2\2O\23\3\2\2\2PQ\7\t\2\2QR\5\36\20"+
		"\2RS\7\21\2\2S\25\3\2\2\2TU\7\b\2\2UV\7\17\2\2V\27\3\2\2\2WX\7\6\2\2X"+
		"Y\5\34\17\2Y\31\3\2\2\2Z[\7\20\2\2[\\\7\20\2\2\\\33\3\2\2\2]^\7\17\2\2"+
		"^_\7\17\2\2_\35\3\2\2\2`a\7\r\2\2ab\7\20\2\2bc\7\16\2\2c\37\3\2\2\2\4"+
		"+-";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}