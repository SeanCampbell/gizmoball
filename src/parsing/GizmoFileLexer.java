// Generated from C:\JavaLib\GizmoFile.g4 by ANTLR 4.1
package parsing;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class GizmoFileLexer extends Lexer {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		ABSORBER=1, BALL=2, CONNECT=3, FRICTION=4, GIZMO=5, GRAVITY=6, KEYCONNECT=7, 
		MOVE=8, ROTATE=9, REMOVE=10, KEY=11, DIRECTION=12, FLOAT=13, INTEGER=14, 
		IDENTIFIER=15, SPACE=16, END=17;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"<INVALID>",
		"'Absorber'", "'Ball'", "'Connect'", "'Friction'", "GIZMO", "'Gravity'", 
		"'KeyConnect'", "'Move'", "'Rotate'", "'Remove'", "'key'", "DIRECTION", 
		"FLOAT", "INTEGER", "IDENTIFIER", "SPACE", "END"
	};
	public static final String[] ruleNames = {
		"ABSORBER", "BALL", "CONNECT", "FRICTION", "GIZMO", "GRAVITY", "KEYCONNECT", 
		"MOVE", "ROTATE", "REMOVE", "KEY", "DIRECTION", "FLOAT", "INTEGER", "IDENTIFIER", 
		"SPACE", "END", "LETTER_OR_DIGIT_OR_UNDER", "LETTER", "DIGIT"
	};


	public GizmoFileLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "GizmoFile.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 15: SPACE_action((RuleContext)_localctx, actionIndex); break;

		case 16: END_action((RuleContext)_localctx, actionIndex); break;
		}
	}
	private void END_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1: skip();  break;
		}
	}
	private void SPACE_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0: skip();  break;
		}
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\2\23\u00e0\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5"+
		"\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6\u0081\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3"+
		"\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00b3\n\r\3\16\6\16\u00b6\n\16\r\16"+
		"\16\16\u00b7\3\16\3\16\6\16\u00bc\n\16\r\16\16\16\u00bd\3\17\6\17\u00c1"+
		"\n\17\r\17\16\17\u00c2\3\20\6\20\u00c6\n\20\r\20\16\20\u00c7\3\21\6\21"+
		"\u00cb\n\21\r\21\16\21\u00cc\3\21\3\21\3\22\5\22\u00d2\n\22\3\22\3\22"+
		"\3\22\3\22\3\23\3\23\3\23\5\23\u00db\n\23\3\24\3\24\3\25\3\25\2\26\3\3"+
		"\1\5\4\1\7\5\1\t\6\1\13\7\1\r\b\1\17\t\1\21\n\1\23\13\1\25\f\1\27\r\1"+
		"\31\16\1\33\17\1\35\20\1\37\21\1!\22\2#\23\3%\2\1\'\2\1)\2\1\3\2\4\6\2"+
		"\13\13\"\"))~~\4\2C\\c|\u00ea\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t"+
		"\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2"+
		"\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2"+
		"\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\3+\3\2\2\2\5\64\3\2\2\2\79\3\2\2\2\t"+
		"A\3\2\2\2\13\u0080\3\2\2\2\r\u0082\3\2\2\2\17\u008a\3\2\2\2\21\u0095\3"+
		"\2\2\2\23\u009a\3\2\2\2\25\u00a1\3\2\2\2\27\u00a8\3\2\2\2\31\u00b2\3\2"+
		"\2\2\33\u00b5\3\2\2\2\35\u00c0\3\2\2\2\37\u00c5\3\2\2\2!\u00ca\3\2\2\2"+
		"#\u00d1\3\2\2\2%\u00da\3\2\2\2\'\u00dc\3\2\2\2)\u00de\3\2\2\2+,\7C\2\2"+
		",-\7d\2\2-.\7u\2\2./\7q\2\2/\60\7t\2\2\60\61\7d\2\2\61\62\7g\2\2\62\63"+
		"\7t\2\2\63\4\3\2\2\2\64\65\7D\2\2\65\66\7c\2\2\66\67\7n\2\2\678\7n\2\2"+
		"8\6\3\2\2\29:\7E\2\2:;\7q\2\2;<\7p\2\2<=\7p\2\2=>\7g\2\2>?\7e\2\2?@\7"+
		"v\2\2@\b\3\2\2\2AB\7H\2\2BC\7t\2\2CD\7k\2\2DE\7e\2\2EF\7v\2\2FG\7k\2\2"+
		"GH\7q\2\2HI\7p\2\2I\n\3\2\2\2JK\7U\2\2KL\7s\2\2LM\7w\2\2MN\7c\2\2NO\7"+
		"t\2\2O\u0081\7g\2\2PQ\7E\2\2QR\7k\2\2RS\7t\2\2ST\7e\2\2TU\7n\2\2U\u0081"+
		"\7g\2\2VW\7V\2\2WX\7t\2\2XY\7k\2\2YZ\7c\2\2Z[\7p\2\2[\\\7i\2\2\\]\7n\2"+
		"\2]\u0081\7g\2\2^_\7T\2\2_`\7k\2\2`a\7i\2\2ab\7j\2\2bc\7v\2\2cd\7H\2\2"+
		"de\7n\2\2ef\7k\2\2fg\7r\2\2gh\7r\2\2hi\7g\2\2i\u0081\7t\2\2jk\7N\2\2k"+
		"l\7g\2\2lm\7h\2\2mn\7v\2\2no\7H\2\2op\7n\2\2pq\7k\2\2qr\7r\2\2rs\7r\2"+
		"\2st\7g\2\2t\u0081\7t\2\2uv\7I\2\2vw\7t\2\2wx\7c\2\2xy\7x\2\2yz\7k\2\2"+
		"z{\7v\2\2{|\7{\2\2|}\7Y\2\2}~\7g\2\2~\177\7n\2\2\177\u0081\7n\2\2\u0080"+
		"J\3\2\2\2\u0080P\3\2\2\2\u0080V\3\2\2\2\u0080^\3\2\2\2\u0080j\3\2\2\2"+
		"\u0080u\3\2\2\2\u0081\f\3\2\2\2\u0082\u0083\7I\2\2\u0083\u0084\7t\2\2"+
		"\u0084\u0085\7c\2\2\u0085\u0086\7x\2\2\u0086\u0087\7k\2\2\u0087\u0088"+
		"\7v\2\2\u0088\u0089\7{\2\2\u0089\16\3\2\2\2\u008a\u008b\7M\2\2\u008b\u008c"+
		"\7g\2\2\u008c\u008d\7{\2\2\u008d\u008e\7E\2\2\u008e\u008f\7q\2\2\u008f"+
		"\u0090\7p\2\2\u0090\u0091\7p\2\2\u0091\u0092\7g\2\2\u0092\u0093\7e\2\2"+
		"\u0093\u0094\7v\2\2\u0094\20\3\2\2\2\u0095\u0096\7O\2\2\u0096\u0097\7"+
		"q\2\2\u0097\u0098\7x\2\2\u0098\u0099\7g\2\2\u0099\22\3\2\2\2\u009a\u009b"+
		"\7T\2\2\u009b\u009c\7q\2\2\u009c\u009d\7v\2\2\u009d\u009e\7c\2\2\u009e"+
		"\u009f\7v\2\2\u009f\u00a0\7g\2\2\u00a0\24\3\2\2\2\u00a1\u00a2\7T\2\2\u00a2"+
		"\u00a3\7g\2\2\u00a3\u00a4\7o\2\2\u00a4\u00a5\7q\2\2\u00a5\u00a6\7x\2\2"+
		"\u00a6\u00a7\7g\2\2\u00a7\26\3\2\2\2\u00a8\u00a9\7m\2\2\u00a9\u00aa\7"+
		"g\2\2\u00aa\u00ab\7{\2\2\u00ab\30\3\2\2\2\u00ac\u00ad\7w\2\2\u00ad\u00b3"+
		"\7r\2\2\u00ae\u00af\7f\2\2\u00af\u00b0\7q\2\2\u00b0\u00b1\7y\2\2\u00b1"+
		"\u00b3\7p\2\2\u00b2\u00ac\3\2\2\2\u00b2\u00ae\3\2\2\2\u00b3\32\3\2\2\2"+
		"\u00b4\u00b6\5)\25\2\u00b5\u00b4\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b5"+
		"\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\u00b9\3\2\2\2\u00b9\u00bb\7\60\2\2"+
		"\u00ba\u00bc\5)\25\2\u00bb\u00ba\3\2\2\2\u00bc\u00bd\3\2\2\2\u00bd\u00bb"+
		"\3\2\2\2\u00bd\u00be\3\2\2\2\u00be\34\3\2\2\2\u00bf\u00c1\5)\25\2\u00c0"+
		"\u00bf\3\2\2\2\u00c1\u00c2\3\2\2\2\u00c2\u00c0\3\2\2\2\u00c2\u00c3\3\2"+
		"\2\2\u00c3\36\3\2\2\2\u00c4\u00c6\5%\23\2\u00c5\u00c4\3\2\2\2\u00c6\u00c7"+
		"\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8 \3\2\2\2\u00c9"+
		"\u00cb\t\2\2\2\u00ca\u00c9\3\2\2\2\u00cb\u00cc\3\2\2\2\u00cc\u00ca\3\2"+
		"\2\2\u00cc\u00cd\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce\u00cf\b\21\2\2\u00cf"+
		"\"\3\2\2\2\u00d0\u00d2\7\17\2\2\u00d1\u00d0\3\2\2\2\u00d1\u00d2\3\2\2"+
		"\2\u00d2\u00d3\3\2\2\2\u00d3\u00d4\7\f\2\2\u00d4\u00d5\3\2\2\2\u00d5\u00d6"+
		"\b\22\3\2\u00d6$\3\2\2\2\u00d7\u00db\5\'\24\2\u00d8\u00db\5)\25\2\u00d9"+
		"\u00db\7a\2\2\u00da\u00d7\3\2\2\2\u00da\u00d8\3\2\2\2\u00da\u00d9\3\2"+
		"\2\2\u00db&\3\2\2\2\u00dc\u00dd\t\3\2\2\u00dd(\3\2\2\2\u00de\u00df\4\62"+
		";\2\u00df*\3\2\2\2\f\2\u0080\u00b2\u00b7\u00bd\u00c2\u00c7\u00cc\u00d1"+
		"\u00da";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}