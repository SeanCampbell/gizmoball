package testing;

import java.lang.reflect.Field;

import junit.framework.Assert;

/**
 * Provides access to private members in classes.
 */
@SuppressWarnings("deprecation")
public class PrivateAccessor {
	public static Object getPrivateField (Class<? extends Object> o, String fieldName) {
    // Check validity of the arguments
    Assert.assertNotNull(o);
    Assert.assertNotNull(fieldName);
    // Attempt to fetch the private field
    final Field fields[] = o.getDeclaredFields();
    for (int i = 0; i < fields.length; ++i) {
      if (fieldName.equals(fields[i].getName())) {
        try {
          fields[i].setAccessible(true);
          return fields[i].get(o);
        } catch (IllegalAccessException ex) {
          Assert.fail ("IllegalAccessException accessing " +
            fieldName);
        }
      }
    }
    // Couldn't find the field
    return null;
  }
}
