package testing;

import static org.junit.Assert.*;

import java.awt.Color;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import model.GizmoFactory;
import model.GizmoTrigger;
import model.IGizmoBall;
import model.gizmos.Rectangle;
import model.gizmos.IGizmo;

import org.jbox2d.dynamics.World;
import org.junit.Test;

import shapes.IGeometry;
import shapes.LogicalPoint;

public class GizmoFactoryJUnit {
	private static final String TEST = "testgizmo";
	private static final String INVALID = "notreal";
	private static final Color GIZCOLOR = Color.BLACK;
	
	static {
		GizmoFactory.getInstance().registerGizmo(GizmoFactoryJUnit.TEST, TestGizmo.class);
	}
	// Dummy architecture; must implement IGizmo to employ the factory successfully.
	public class TestGizmo implements IGizmo{
		private Color color;
		private Integer myInt;
		
		public TestGizmo(int someInt) {
			this.myInt = someInt;
			this.color = GizmoFactoryJUnit.GIZCOLOR;
		}
		
		public int getMyInt() {
			return this.myInt;
		}


		@Override
		public Color getColor() {
			return this.color;
		}

		@Override
		public Collection<IGeometry> getGeometryList() {
			return null;
		}

		@Override
		public String getName() {
			return null;
		}

		@Override
		public void rotate() {
			return;
		}

		@Override
		public void move(int x, int y) {
			return;
		}

		@Override
		public void doAction() {
			return;
		}

		@Override
		public String getSaveString() {
			return null;
		}

		@Override
		public Rectangle getBoundaryInfo() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isVariableSize() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public double getLength() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public double getWidth() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public LogicalPoint getOrigin() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void addTrigger(GizmoTrigger g) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void removeTrigger() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void updateTrigger() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void addPhysics(World world) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void collide(IGizmoBall ball) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void tick(double deltaT) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void destroyPhysics(World world) {
			// TODO Auto-generated method stub
		}

		@Override
		public Collection<IGizmoBall> hasGizmoBalls() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	
	@Test(expected = InstantiationException.class) 
	public void testInvalidName() throws InstantiationException, IllegalAccessException {
		GizmoFactory.getInstance().createGizmo(GizmoFactoryJUnit.INVALID);
		fail("Did not handle unrecognised name");
	}
	
	@Test(expected = InstantiationException.class) 
	public void testValidNameInvalidConstruction() throws InstantiationException, IllegalAccessException {
		GizmoFactory.getInstance().createGizmo(GizmoFactoryJUnit.TEST);
		fail("Expected exception: InstantiationException");
	}
	
	@Test
	public void testValidNameValidConstruction() {
		try {
			IGizmo giz = GizmoFactory.getInstance().createGizmo(GizmoFactoryJUnit.TEST, this, 1);
			assertNotNull(giz);
			// Ensure property created by the factory when creating the gizmo.
			assertEquals(GizmoFactoryJUnit.GIZCOLOR,giz.getColor());
		}
		catch (InstantiationException e) {
			fail("Got exception: InstantiationException");
		} 
		catch (IllegalAccessException e) {
			fail("Got exception: IllegalAccessException");
		} 
		catch (NoSuchMethodException e) {
			fail("Got exception: NoSuchMethodException");
		} 
		catch (SecurityException e) {
			fail("Got exception: SecurityException");
		} 
		catch (IllegalArgumentException e) {
			fail("Got exception: IllegalArgumentException");
		} 
		catch (InvocationTargetException e) {
			fail("Got exception: InvocationTargetException");
		}
	}
}
