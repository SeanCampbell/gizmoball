package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import model.GizmoFactory;
import model.IGizmoFactory;
import model.gizmos.Flipper;
import model.gizmos.IGizmo;
import model.gizmos.LeftFlipper;

import org.junit.BeforeClass;
import org.junit.Test;

import shapes.GeometryCircle;
import shapes.GeometryPolygon;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class LeftFlipperJUnit {
	private static final int[][] ARGS = {{3, 1}, {10, 4}, {0, 0}};
	private static final String[] NAMES = {"LF1", "LF2", "LF3", "LF4", "LF5"};
	private static final IGizmoFactory FACTORY = GizmoFactory.getInstance();

	private static String LEFT_FLIPPER_NAME;
	private static double CENTRE_X, CENTRE_Y, WIDTH, LENGTH, OFFSET, 
	LEFT_INIT_RAD, RADIUS;

	private static IGizmo lf1, lf2, lf3, lf4;

	private static int orientation;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LEFT_FLIPPER_NAME = (String)PrivateAccessor.getPrivateField(LeftFlipper.class, "NAME");
		CENTRE_X = (Double)PrivateAccessor.getPrivateField(Flipper.class, "CENTRE_X");
		CENTRE_Y = (Double)PrivateAccessor.getPrivateField(Flipper.class, "CENTRE_Y");
		WIDTH = (Double)PrivateAccessor.getPrivateField(Flipper.class, "WIDTH");
		LENGTH = (Double)PrivateAccessor.getPrivateField(Flipper.class, "LENGTH");
		OFFSET = (Double)PrivateAccessor.getPrivateField(Flipper.class, "OFFSET");
		LEFT_INIT_RAD = (Double)PrivateAccessor.getPrivateField(Flipper.class, "LEFT_INIT_RAD");
		RADIUS = (Double)PrivateAccessor.getPrivateField(Flipper.class, "RADIUS");

		lf1 = FACTORY.createGizmo(LEFT_FLIPPER_NAME, NAMES[0], ARGS[0][0], ARGS[0][1]);
		lf2 = FACTORY.createGizmo(LEFT_FLIPPER_NAME, NAMES[1], ARGS[0][0], ARGS[0][1]);
		lf3 = FACTORY.createGizmo(LEFT_FLIPPER_NAME, NAMES[2], ARGS[1][0], ARGS[1][1]);
		lf4 = FACTORY.createGizmo(LEFT_FLIPPER_NAME, NAMES[3], ARGS[2][0], ARGS[2][1]);
	}

	private Collection<IGeometry> createFlipper(double x, double y, int orientation, double angle, LogicalPoint pivot, LogicalPoint unitCentre) {
		List<LogicalPoint> points = new ArrayList<>();
		points.add(pivot.rotate(unitCentre, orientation));
		points.add(rotate(new LogicalPoint(x + OFFSET, y
				+ LENGTH + OFFSET), unitCentre,
				pivot, orientation, angle));
		points.add(rotate(new LogicalPoint(x, y + OFFSET),
				unitCentre, pivot, orientation, angle));
		points.add(rotate(new LogicalPoint(x, y + OFFSET
				+ LENGTH), unitCentre, pivot,
				orientation, angle));
		points.add(rotate(new LogicalPoint(x + WIDTH, y
				+ OFFSET + LENGTH), unitCentre,
				pivot, orientation, angle));
		points.add(rotate(new LogicalPoint(x + WIDTH, y
				+ OFFSET), unitCentre, pivot,
				orientation, angle));

		List<IGeometry> geo = new ArrayList<>();
		geo.add(new GeometryCircle(points.get(0), RADIUS));
		geo.add(new GeometryCircle(points.get(1), RADIUS));
		geo.add(new GeometryPolygon(points.subList(2, points.size())));
		return geo;
	}

	private LogicalPoint rotate(final LogicalPoint point,
			final LogicalPoint centre, final LogicalPoint pivot,
			final int orientation, final double angle) {
		LogicalPoint temp = point.rotate(pivot, angle);
		temp = temp.rotate(centre, orientation);
		return temp;
	}

	/**
	 * Asserts the the correct geometries are produced.
	 */
	@Test
	public void testGeos() {
		Collection<IGeometry> actual = lf1.getGeometryList();
		LogicalPoint pivot = new LogicalPoint(OFFSET + ARGS[0][0], OFFSET + ARGS[0][1]);
		LogicalPoint unitCentre = new LogicalPoint(CENTRE_X + ARGS[0][0], CENTRE_Y + ARGS[0][1]);
		Collection<IGeometry> expected = this.createFlipper(ARGS[0][0], ARGS[0][1], orientation, LEFT_INIT_RAD, pivot, unitCentre);

		assertEquals(actual.size(), expected.size());
		assertTrue(actual.containsAll(expected));
		assertTrue(expected.containsAll(actual));
	}

	/**
	 * Asserts that the flipper can move.
	 */
	@Test
	public void testMove() {
		lf2.move(5, 7);
		Collection<IGeometry> geoL = lf2.getGeometryList();
		LogicalPoint pivot = new LogicalPoint(OFFSET + 5, OFFSET + 7);
		LogicalPoint unitCentre = new LogicalPoint(CENTRE_X + 5, CENTRE_Y + 7);
		Collection<IGeometry> geo = createFlipper(5, 7, orientation, LEFT_INIT_RAD, pivot, unitCentre);

		assertEquals(geoL.size(), geo.size());
		assertTrue(geoL.containsAll(geo));
		assertTrue(geo.containsAll(geoL));
	}

	/**
	 * Tests that a flipper can rotate and then produce the correct geometries.
	 */
	@Test
	public void testRotation() {
		lf1.rotate();
		Collection<IGeometry> actual = lf1.getGeometryList();

		int thisOrient = orientation;
		thisOrient = (orientation + 1) % 4;

		LogicalPoint pivot = new LogicalPoint(OFFSET + ARGS[0][0], OFFSET +ARGS[0][1]);
		LogicalPoint unitCentre = new LogicalPoint(CENTRE_X + ARGS[0][0], CENTRE_Y +ARGS[0][1]);
		Collection<IGeometry> expected = this.createFlipper(ARGS[0][0], ARGS[0][1], thisOrient, 0, pivot, unitCentre);

		assertEquals(actual.size(), expected.size());
		assertTrue(actual.containsAll(expected));
		assertTrue(expected.containsAll(actual));
	}
	
	/**
	 * Tests the tick method.
	 */
	@Test
	public void testTick() {
		lf4.tick(0.02);
		@SuppressWarnings("unused")
		Collection<IGeometry> actual = lf4.getGeometryList();
		
		int thisOrient = orientation;

		LogicalPoint pivot = new LogicalPoint(OFFSET + ARGS[1][0], OFFSET +ARGS[1][1]);
		LogicalPoint unitCentre = new LogicalPoint(CENTRE_X + ARGS[1][0], CENTRE_Y +ARGS[1][1]);
		@SuppressWarnings("unused")
		Collection<IGeometry> expected = this.createFlipper(ARGS[1][0], ARGS[1][1], thisOrient, 0, pivot, unitCentre);

		
	}
	
	/**
	 * Checks the save string is valid.
	 */
	@Test
	public void testSaveString() {
		String actual = lf3.getSaveString();
		StringBuilder stringBuild = new StringBuilder();
		stringBuild.append(LEFT_FLIPPER_NAME + " " + NAMES[2] + " " + ARGS[1][0] + " " + ARGS[1][1] + "\n");
		assertEquals(actual, stringBuild.toString());
		
		lf3.rotate();
		actual = lf3.getSaveString();
		stringBuild.append("Rotate" + NAMES[2] + "\n");
		assertEquals(actual, stringBuild.toString());
	}
	
	/**
	 * Tests the width of the flipper.
	 */
	@Test
	public void testWidth() {
		assertEquals(WIDTH, lf1.getWidth(), 0.01);
	}
	
	/**
	 * Tests the height of the flipper.
	 */
	@Test 
	public void testHeight() {
		assertEquals(LENGTH, lf1.getLength(), 0.01);
	}
	
	/**
	 * Asserts that flippers cannot vary in size.
	 */
	@Test
	public void testVariableSize() {
		assertFalse(lf1.isVariableSize());
		assertFalse(lf2.isVariableSize());
		assertFalse(lf3.isVariableSize());
	}
	
	/**
	 * Asserts that we return the correct origin value.
	 * The only time the origin value should change is after a move.
	 */
	@Test
	public void testOrigin() {
		assertEquals(lf4.getOrigin(), new LogicalPoint(ARGS[2][0], ARGS[2][1]));
		lf4.move(15, 15);
		assertEquals(lf4.getOrigin(), new LogicalPoint(15,15));
	
	}

}
