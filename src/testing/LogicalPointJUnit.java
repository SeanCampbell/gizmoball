package testing;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import shapes.LogicalPoint;

public class LogicalPointJUnit {
	private static final double[][] ARGS = {{1.0, 1.0},{1.5, 1.5}};
	private static double ROTATIONS[][] = new double[][]{{1,0},{0,1},{-1,0},{0,-1}};
	// Change if you alter ARGS.
	private static final boolean ALTEREDNUMBERS = false;
	private LogicalPoint refPoint, rotatePoint;
	
	@BeforeClass
	public static void setUpBeforeClass() {
	}

	@Before
	public void setUp() throws Exception{
		this.refPoint = new LogicalPoint(LogicalPointJUnit.ARGS[0][0], LogicalPointJUnit.ARGS[0][1]);
		this.rotatePoint = new LogicalPoint(LogicalPointJUnit.ARGS[1][0], LogicalPointJUnit.ARGS[1][1]);
	}
	
	@After
	public void tearDown() throws Exception {
		this.refPoint = null;
		this.rotatePoint = null;
	}
	
	@Test
	public void testEquals() throws Exception{
		testObjectProperties();
		testExamples();
	}
	
	private void testObjectProperties() throws Exception{
		//Test valid object is not null.
		assertFalse("Error: Object of " + this.refPoint.getClass().toString() + " equal to null.",
						this.refPoint.equals(null));
		//Test reflexive property.
		assertTrue("Error: "+this.refPoint.getClass().toString()+" is not reflexive.",
					this.refPoint.equals(this.refPoint));
		//Test symmetric.
		LogicalPoint temp = new LogicalPoint(LogicalPointJUnit.ARGS[0][0], LogicalPointJUnit.ARGS[0][1]);
		assertEquals("Error: "+this.refPoint.getClass().toString()+" is not symmetric.",this.refPoint,temp);
		//Test transitive.
		LogicalPoint temp2 = new LogicalPoint(LogicalPointJUnit.ARGS[0][0], LogicalPointJUnit.ARGS[0][1]);
		if(this.refPoint.equals(temp)&temp.equals(temp2))
					assertEquals("Error: "+this.refPoint.getClass().toString()+" is not transitive.",
							this.refPoint,temp2);
		//Test consistency.
		assertTrue("Error: "+this.refPoint.getClass().toString()+" is not consistent.",
						this.refPoint.equals(temp)==this.refPoint.equals(temp));
	}
	
	private void testExamples() throws Exception{
		LogicalPoint temp = new LogicalPoint(LogicalPointJUnit.ARGS[0][0], LogicalPointJUnit.ARGS[0][1]);
		assertEquals(this.refPoint,temp);
		assertFalse(this.refPoint.equals(this.rotatePoint));
	}

	@Test
	public void testRotation() {
		// No shift should occur for the first point.
		LogicalPoint np = this.rotatePoint.rotate(this.refPoint, LogicalPointJUnit.ROTATIONS[0][0], LogicalPointJUnit.ROTATIONS[0][1]);
		assertEquals(np, this.rotatePoint);
		// From this point, did the example by hand/in head for explicit values to valdiate computation.
		if(!LogicalPointJUnit.ALTEREDNUMBERS){
			LogicalPoint np2 = rotatePoint.rotate(this.refPoint, LogicalPointJUnit.ROTATIONS[1][0], LogicalPointJUnit.ROTATIONS[1][1]);
			assertEquals(np2, new LogicalPoint(0.5, 1.5));
			LogicalPoint np3 = rotatePoint.rotate(this.refPoint, LogicalPointJUnit.ROTATIONS[2][0], LogicalPointJUnit.ROTATIONS[2][1]);
			assertEquals(np3, new LogicalPoint(0.5, 0.5));
			LogicalPoint np4 = rotatePoint.rotate(this.refPoint, LogicalPointJUnit.ROTATIONS[3][0], LogicalPointJUnit.ROTATIONS[3][1]);
			assertEquals(np4, new LogicalPoint(1.5, 0.5));
		}
	}
	
	@Test
	public void testToString(){
		StringBuilder builder = new StringBuilder();
		builder.append(Math.round(Math.round(this.refPoint.getX())) +
				" " + Math.round(Math.round(this.refPoint.getY())));
		assertEquals(this.refPoint.toString(), builder.toString());
		assertFalse(this.rotatePoint.toString().equals(builder.toString()));
	}
}
