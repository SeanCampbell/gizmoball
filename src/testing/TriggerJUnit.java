package testing;

import static org.junit.Assert.*;
import model.IGizmoBall;
import model.TriggerManager;
import model.gizmos.AbsGizmo;
import model.gizmos.IGizmo;
import model.gizmos.Rectangle;

import org.jbox2d.dynamics.World;
import org.junit.BeforeClass;
import org.junit.Test;

import shapes.LogicalPoint;

public class TriggerJUnit {

	static TriggerManager tm;
	static IGizmo t1, t2, t3;
	static int kc;
	
	@BeforeClass
	public static void setUpBeforeClass() {
		tm = new TriggerManager();
		TriggerJUnit tj = new TriggerJUnit();
		t1 = tj.new TriggerTester("t1");
		t2 = tj.new TriggerTester("t2");
		t3 = tj.new TriggerTester("t3");
		kc = 10;
	}

	@Test
	public void testGizTrig() {
//		assertFalse(tm.onCollision("t1"));
		assertFalse(tm.disconnect(t1, t2));
		assertTrue(tm.connect(t1, t2));
		assertTrue(tm.connect(t1, t3));
//		assertTrue(tm.onCollision("t1"));
		assertFalse(tm.connect(t1, t2));
		assertTrue(tm.disconnect(t1, t2));
//		assertTrue(tm.onCollision("t1"));
		assertTrue(tm.disconnect(t1, t3));
//		assertFalse(tm.onCollision("t1"));
	}

	@Test
	public void testKeyTrig() {
		assertFalse(tm.onKeyEvent(kc, true));
		assertFalse(tm.disconnect(kc, t2, true));
		assertTrue(tm.connect(kc, t2, true));
		assertTrue(tm.connect(kc, t3, true));
		assertTrue(tm.onKeyEvent(kc, true));
		assertFalse(tm.connect(kc, t2, true));
		assertTrue(tm.disconnect(kc, t2, true));
		assertTrue(tm.onKeyEvent(kc, true));
		assertTrue(tm.disconnect(kc, t3, true));
		assertFalse(tm.onKeyEvent(kc, true));
	}

	public class TriggerTester extends AbsGizmo {

		public TriggerTester(String name) {
			super(name);
		}

		@Override
		public void move(int x, int y) {
			// TODO Auto-generated method stub

		}

		@Override
		public void doAction() {
			// TODO Auto-generated method stub

		}

		@Override
		public String getSaveString() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Rectangle getBoundaryInfo() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean isVariableSize() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public double getLength() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public double getWidth() {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void collide(IGizmoBall ball) {
			// TODO Auto-generated method stub

		}

		@Override
		public LogicalPoint getOrigin() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public void addPhysics(World world) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void destroyPhysics(World world) {
			// TODO Auto-generated method stub
			
		}
	}
}

