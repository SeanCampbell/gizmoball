package testing;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.BeforeClass;
import org.junit.Test;

import shapes.IPolygon;
import shapes.LogicalPoint;
import shapes.GeometryPolygon;

public class PolygonJUnit {
	private static final double[][] ARGS = {{5.0, 5.0}, {5.0, 4.0}, {6.0, 4.0},
											{0.0, 0.0},{3.0, 0.0},{0.0, 2.0}, {3.0, 2.0}};
	private static LogicalPoint t1,t2,t3,r1,r2,r3,r4;
	private static IPolygon triangle, rectangle;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Collection<LogicalPoint> points = new ArrayList<>(), points2 = new ArrayList<>();
		PolygonJUnit.t1 = new LogicalPoint(PolygonJUnit.ARGS[0][0], PolygonJUnit.ARGS[0][1]);
		PolygonJUnit.t2 = new LogicalPoint(PolygonJUnit.ARGS[1][0], PolygonJUnit.ARGS[1][1]);
		PolygonJUnit.t3 = new LogicalPoint(PolygonJUnit.ARGS[2][0], PolygonJUnit.ARGS[2][1]);
		PolygonJUnit.r1 = new LogicalPoint(PolygonJUnit.ARGS[3][0], PolygonJUnit.ARGS[3][1]);
		PolygonJUnit.r2 = new LogicalPoint(PolygonJUnit.ARGS[4][0], PolygonJUnit.ARGS[4][1]);
		PolygonJUnit.r3 = new LogicalPoint(PolygonJUnit.ARGS[5][0], PolygonJUnit.ARGS[5][1]);
		PolygonJUnit.r4 = new LogicalPoint(PolygonJUnit.ARGS[6][0], PolygonJUnit.ARGS[6][1]);
		
		points.add(t1);
		points.add(t2);
		points.add(t3);
		PolygonJUnit.triangle = new GeometryPolygon(points);
		
		points2.add(r1);
		points2.add(r2);
		points2.add(r3);
		points2.add(r4);
		PolygonJUnit.rectangle = new GeometryPolygon(points2);
	}

	/**
	 * Asserts that polygons are not ovals
	 */
	@Test
	public void nullOvalTest(){
		assertNull(PolygonJUnit.triangle.getCircle());
		assertNull(PolygonJUnit.rectangle.getCircle());
	}
	
	/**
	 * Test that the list of points retrieved is correct
	 */
	@Test
	public void pointsTest(){
		Collection<LogicalPoint> tList = new ArrayList<LogicalPoint>();
		tList.add(PolygonJUnit.t1);
		tList.add(PolygonJUnit.t2);
		tList.add(PolygonJUnit.t3);
		assertEquals(tList, PolygonJUnit.triangle.getPoints());
		
		Collection<LogicalPoint> rList = new ArrayList<LogicalPoint>();
		rList.add(PolygonJUnit.r1);
		rList.add(PolygonJUnit.r2);
		rList.add(PolygonJUnit.r3);
		rList.add(PolygonJUnit.r4);
		assertEquals(rList, PolygonJUnit.rectangle.getPoints());
	}
	
	@Test
	public void testEquals() throws Exception{
		testObjectProperties();
		testExamples();
	}
	
	private void testObjectProperties() throws Exception{
		//Test valid object is not null.
		assertFalse("Error: Object of " + PolygonJUnit.triangle.getClass().toString() + " equal to null.",
						PolygonJUnit.triangle.equals(null));
		//Test reflexive property.
		assertTrue("Error: "+ PolygonJUnit.triangle.getClass().toString()+" is not reflexive.",
				 PolygonJUnit.triangle.equals( PolygonJUnit.triangle));
		//Test symmetric.
		IPolygon temp = new GeometryPolygon(new ArrayList<LogicalPoint>() {private static final long serialVersionUID = 1L;
		{add(PolygonJUnit.t1); add(PolygonJUnit.t2); add(PolygonJUnit.t3);}});
		assertEquals("Error: "+ PolygonJUnit.triangle.getClass().toString()+" is not symmetric.", PolygonJUnit.triangle, temp);
		//Test transitive.
		IPolygon temp2 = new GeometryPolygon(new ArrayList<LogicalPoint>() {private static final long serialVersionUID = 1L;
		{add(PolygonJUnit.t1); add(PolygonJUnit.t2); add(PolygonJUnit.t3);}});
		if(PolygonJUnit.triangle.equals(temp)&temp.equals(temp2))
					assertEquals("Error: "+ PolygonJUnit.triangle.getClass().toString()+" is not transitive.",
							 PolygonJUnit.triangle,temp2);
		//Test consistency.
		assertTrue("Error: "+ PolygonJUnit.triangle.getClass().toString()+" is not consistent.",
				 PolygonJUnit.triangle.equals(temp)== PolygonJUnit.triangle.equals(temp));
	}
	
	private void testExamples() throws Exception{
		// Check equality
		IPolygon eq = new GeometryPolygon(new ArrayList<LogicalPoint>() {private static final long serialVersionUID = 1L;
			{add(PolygonJUnit.t1); add(PolygonJUnit.t2); add(PolygonJUnit.t3);}});
		assertEquals(eq, PolygonJUnit.triangle);
		
		// Check inequality if lists ordered differently
		IPolygon neq = new GeometryPolygon(new ArrayList<LogicalPoint>() { private static final long serialVersionUID = 1L;
			{add(PolygonJUnit.t3); add(PolygonJUnit.t2); add(PolygonJUnit.t1);}});
		assertFalse(PolygonJUnit.triangle.equals(neq));
		
		// Check inequality if different point
		PolygonJUnit.t1 = new LogicalPoint(PolygonJUnit.ARGS[0][0] + 1.0, PolygonJUnit.ARGS[0][1]);
		IPolygon neq2 = new GeometryPolygon(new ArrayList<LogicalPoint>() { private static final long serialVersionUID = 1L;
		{add(PolygonJUnit.t1); add(PolygonJUnit.t2); add(PolygonJUnit.t3);}});
		assertFalse(PolygonJUnit.triangle.equals(neq2));
	}	
}
