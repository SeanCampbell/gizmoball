package testing;

import static org.junit.Assert.*;

import java.util.Arrays;

import junit.framework.*;
import model.GameBoard;
import model.GizmoBall;
import model.GizmoFactory;
import model.GizmoSettings;
import model.IGameBoard;
import model.gizmos.Absorber;
import model.gizmos.CircleBumper;
import model.gizmos.LeftFlipper;
import model.gizmos.RightFlipper;
import model.gizmos.SquareBumper;
import model.gizmos.TriangleBumper;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Assume;

import exceptions.DuplicateNameException;
import exceptions.OverlappingItemException;
import exceptions.OutOfBoundsException;
import exceptions.NoSuchItemException;
/**
 * TODO
 * - Physics tests
 */
public class GameBoardJUnit {
	private static final String[] IDS = {"C1", "S1", "A1", "RF1", "LF1", "T1", "B1", "B2"};
	private static final int[][] CO_ORDS = {{0,0}, {1,1}, {0, 18, 19, 19}, {3, 14}, {6, 14}, {5, 6}};
	private static final double[][] B_CO_ORDS = {{0.25, 0.25}, {3.5, 3.5}, {18.5, 18.5}, {0.0, 0.0}};
	private static final double[][] B_VELS = {{0.0, 0.0},{0.0, -50.0}, {-10.0, -10.0}};
	private static final double MAX_LENGTH = GizmoSettings.getGameBoardLength(), 
			MAX_WIDTH = GizmoSettings.getGameBoardWidth();
	private static final double PHYS_VALUE = 50.0;
	private static final double DELTA = 0.0000000001;
	
	private static String CIRC, SQ, AB, RF, LF, TRI, BALL;
	private static IGameBoard gb;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// Access the private members via reflection for unit testing.  Don't wish to expose.
		GameBoardJUnit.CIRC = (String)PrivateAccessor.getPrivateField(CircleBumper.class, "NAME");
		GameBoardJUnit.SQ = (String)PrivateAccessor.getPrivateField(SquareBumper.class, "NAME");
		GameBoardJUnit.AB= (String)PrivateAccessor.getPrivateField(Absorber.class, "NAME");
		GameBoardJUnit.RF = (String)PrivateAccessor.getPrivateField(RightFlipper.class, "NAME");
		GameBoardJUnit.LF = (String)PrivateAccessor.getPrivateField(LeftFlipper.class, "NAME");
		GameBoardJUnit.TRI = (String)PrivateAccessor.getPrivateField(TriangleBumper.class, "NAME");
		GameBoardJUnit.BALL = (String)PrivateAccessor.getPrivateField(GizmoBall.class, "NAME");
	}
	
	@Before
	public void setUp() throws Exception {
		GameBoardJUnit.gb = new GameBoard(GizmoFactory.getInstance());
	}
	
	/**
	 * 	Add Gizmo tests.  Considered cases:
	 * - valid addition of all gizmos
	 * - invalid addition of a gizmo outwith the game-board specific size
	 * - invalid addition of a gizmo with a duplicate name
	 * - invalid addition of a gizmo on top of another gizmo with equal boundaries
	 * - invalid addition of a gizmo on top of another gizmo with different boundaries
	 */
	
	/**
	 * Test adding gizmos to random locations on the game-board which do not cause overlap
	 * @throws Exception
	 */
	@Test
	public void testAddGizmoValid() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.AB, GameBoardJUnit.IDS[2], GameBoardJUnit.CO_ORDS[2][0],
				GameBoardJUnit.CO_ORDS[2][1], GameBoardJUnit.CO_ORDS[2][2], GameBoardJUnit.CO_ORDS[2][3]);
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[2][0], GameBoardJUnit.CO_ORDS[2][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.RF, GameBoardJUnit.IDS[3], GameBoardJUnit.CO_ORDS[3][0],
				GameBoardJUnit.CO_ORDS[3][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[3][0], GameBoardJUnit.CO_ORDS[3][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.LF, GameBoardJUnit.IDS[4], GameBoardJUnit.CO_ORDS[4][0],
				GameBoardJUnit.CO_ORDS[4][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[4][0], GameBoardJUnit.CO_ORDS[4][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.TRI, GameBoardJUnit.IDS[5], GameBoardJUnit.CO_ORDS[5][0],
				GameBoardJUnit.CO_ORDS[5][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[5][0], GameBoardJUnit.CO_ORDS[5][1]));
	}
	/**
	 * Expected {@link OutOfBoundsException.class}
	 * Test adding a gizmo out-with the playing area
	 * @throws Exception
	 */
	@Test(expected=OutOfBoundsException.class)
	public void testAddGizmoOutwithBoundary() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], 
				Math.round(Math.round(GameBoardJUnit.MAX_LENGTH + 5.0)),
				Math.round(Math.round(GameBoardJUnit.MAX_WIDTH + 5.0)));
	}
	/**
	 * Expected {@link DuplicateNameException.class}
	 * Test adding two gizmos to the game-board with the exact same name (loader case)
	 * @throws Exception
	 */
	@Test(expected=DuplicateNameException.class)
	public void testAddDoubleName() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]);
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
	}
	/**
	 * Expected {@link OverlappingGizmoException.class}
	 * Test adding a gizmo on top of another gizmo which possesses the same boundaries
	 * @throws Exception
	 */
	@Test(expected=OverlappingItemException.class)
	public void testAddOverlappingSame() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.RF, GameBoardJUnit.IDS[3], GameBoardJUnit.CO_ORDS[3][0],
				GameBoardJUnit.CO_ORDS[3][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[3][0],
				GameBoardJUnit.CO_ORDS[3][1]);
	}
	/**
	 * Expected {@link OverlappingGizmoException.class}
	 * Test adding a gizmo on top of another gizmo which possesses different boundaries
	 * @throws Exception
	 */
	@Test(expected=OverlappingItemException.class)
	public void testAddOverlappingDifferent() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
	}
	/**
	 * Test fetching gizmo names when added to particular positions.
	 */
	@Test
	public void testHasGizmoNameValid() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		assertTrue(GameBoardJUnit.gb.hasGizmo(GameBoardJUnit.IDS[0]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[0]);
		Assume.assumeTrue(!GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		assertFalse(GameBoardJUnit.gb.hasGizmo(GameBoardJUnit.IDS[0]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[1]);
		Assume.assumeTrue(!GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		assertFalse(GameBoardJUnit.gb.hasGizmo(GameBoardJUnit.IDS[1]));
	}
	
	/**
	 * Move Gizmo tests.  Considered cases:
	 * - Assume valid addition of gizmos; move all gizmos to valid locations
	 * - Assert that a gizmo cannot be added on top of another gizmo
	 * - Assert that a non-existent gizmo cannot be moved
	 * - Assert that a gizmo cannot be added outwith the boundaries of the playing area
	 */
	/**
	 * Move a Gizmo to a valid location
	 * @throws Exception
	 */
	@Test
	public void testMoveGizmoValid() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		GameBoardJUnit.gb.moveGizmo(GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
	}
	/**
	 * Expected {@link OverlappingGizmoException.class}
	 * Attempt to move a non-existent gizmo
	 * @throws Exception
	 */
	@Test(expected=OverlappingItemException.class)
	public void testMoveGizmoInvalid() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		GameBoardJUnit.gb.moveGizmo(GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
	}
	/**
	 * Expected {@link NoSuchItemException.class}
	 * Assert a non-existent item cannot be moved
	 * @throws Exception
	 */
	@Test(expected=NoSuchItemException.class)
	public void testMoveGizmoNoSuchItem() throws Exception{
		GameBoardJUnit.gb.moveGizmo(GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
	}
	/**
	 * Expected {@link OutOfBoundsException.class}
	 * Test a gizmo cannot be added outwith the game board boundaries
	 * @throws Exception
	 */
	@Test(expected=OutOfBoundsException.class)
	public void testMoveGizmoOutOfBounds() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GizmoSettings.getGameBoardWidth(),
				GizmoSettings.getGameBoardLength());
	}
	/**
	 * Remove Gizmo tests.  Considered cases:
	 * - Assume valid addition of all gizmos; assumption validity leads to asserting valid removals
	 * - Assume valid addition and removal of a gizmo; assert second removal throws exception
	 */
	/**
	 * Assume valid addition of gizmos and removals of gizmos at the same points
	 * @throws Exception
	 */
	@Test
	public void testRemoveGizmoValid() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[0]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[1]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.AB, GameBoardJUnit.IDS[2], GameBoardJUnit.CO_ORDS[2][0],
				GameBoardJUnit.CO_ORDS[2][1], GameBoardJUnit.CO_ORDS[2][2], GameBoardJUnit.CO_ORDS[2][3]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[2][0], GameBoardJUnit.CO_ORDS[2][1]));
		System.out.println("HERE1");
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[2]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[2][0], GameBoardJUnit.CO_ORDS[2][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.RF, GameBoardJUnit.IDS[3], GameBoardJUnit.CO_ORDS[3][0],
				GameBoardJUnit.CO_ORDS[3][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[3][0], GameBoardJUnit.CO_ORDS[3][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[3]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[3][0], GameBoardJUnit.CO_ORDS[3][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.LF, GameBoardJUnit.IDS[4], GameBoardJUnit.CO_ORDS[4][0],
				GameBoardJUnit.CO_ORDS[4][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[4][0], GameBoardJUnit.CO_ORDS[4][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[4]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[4][0], GameBoardJUnit.CO_ORDS[4][1]));
		System.out.println("HERE1");
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.TRI, GameBoardJUnit.IDS[5], GameBoardJUnit.CO_ORDS[5][0],
				GameBoardJUnit.CO_ORDS[5][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[5][0], GameBoardJUnit.CO_ORDS[5][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[5]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[5][0], GameBoardJUnit.CO_ORDS[5][1]));
	}
	/**
	 * Expected {@link NoSuchItemException.class}
	 * Assume valid addition and removal of a gizmo; assert invalid re-removal of the same gizmo
	 * @throws Exception
	 */
	@Test(expected=NoSuchItemException.class)
	public void testRemoveGizmoInvalid() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[0]);
		Assume.assumeTrue(!GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[0]);
	}
	
	/**
	 * Rotate gizmo tests.  Considered tests:
	 * - Assume valid addition of gizmos, rotate the gizmos and assert the gizmo boundary has not altered
	 * - Attempt to rotate a gizmo which does not support rotations
	 * - Attempt to rotate a gizmo which has not been added to the game-board
	 */
	
	/**
	 * Assume valid addition of gizmos, rotate the gizmos and assert the gizmo boundary has not altered
	 * @throws Exception
	 */
	@Test
	public void testRotateGizmoValid() throws Exception{		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.TRI, GameBoardJUnit.IDS[5], GameBoardJUnit.CO_ORDS[5][0],
				GameBoardJUnit.CO_ORDS[5][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[5][0], GameBoardJUnit.CO_ORDS[5][1]));
		
		GameBoardJUnit.gb.rotateGizmo(GameBoardJUnit.IDS[5]);
		assertTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[5][0], GameBoardJUnit.CO_ORDS[5][1]));
	}
	/**
	 * Expected {@link UnsupportedOperationException.class}
	 * Attempt to rotate a gizmo which does not support rotations
	 * @throws Exception
	 */
	@Test(expected=UnsupportedOperationException.class)
	public void testRotateGizmoInvalidOp() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.rotateGizmo(GameBoardJUnit.IDS[0]);
	}
	/**
	 * Expected {@link NoSuchItemException.class}
	 * Assert cannot rotate a gizmo which has not been added to the game board
	 * @throws Exception
	 */
	@Test(expected=NoSuchItemException.class)
	public void testRotateGizmoInvalidName() throws Exception{
		GameBoardJUnit.gb.rotateGizmo(GameBoardJUnit.IDS[0]);
	}
	
	/**
	 * This is a crucial method since the View is capable of receiving information
	 * on the dimensions of the gizmos which will have to be rendered on screen.
	 * Simply test that all of the dimensions are non-null values.
	 */
	@Test
	public void testInitDimensions() throws Exception{
		assertNotNull(gb.getGizmoDimensions("Square"));
		assertNotNull(gb.getGizmoDimensions("LeftFlipper"));
		assertNotNull(gb.getGizmoDimensions("RightFlipper"));
		assertNotNull(gb.getGizmoDimensions("Absorber"));
		assertNotNull(gb.getGizmoDimensions("Circle"));
		assertNotNull(gb.getGizmoDimensions("Triangle"));
		assertNotNull(gb.getGizmoDimensions("Ball"));
	}
	/**
	 * Add GizmoBall tests.  Considered cases:
	 * - Adding GizmoBalls to valid co-ordinates
	 * - A GizmoBall cannot be added over a gizmo
	 * - A GizmoBall cannot be added to a location such that a radius would go out-with the boundaries
	 */
	
	/**
	 * Test GizmoBalls are successfully added to valid locations
	 * @throws Exception
	 */
	@Test
	public void testAddGizmoBallsValid() throws Exception{
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[7], GameBoardJUnit.B_CO_ORDS[1][0], 
				GameBoardJUnit.B_CO_ORDS[1][1], GameBoardJUnit.B_VELS[1][0], GameBoardJUnit.B_VELS[1][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[1][0], GameBoardJUnit.B_CO_ORDS[1][1]));
	}
	/**
	 * Expected {@link OverlappingGizmoException.class}
	 * Test GizmoBalls cannot be added on top of Gizmos
	 * @throws Exception
	 */
	@Test(expected=OverlappingItemException.class)
	public void testAddGizmoBallsOverGizmos() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
	}
	
	/**
	 * Expected {@link OutOfBoundsException.class}
	 * Test GizmoBalls cannot be added to the side of a gameboard
	 * @throws Exception
	 */
	@Test(expected=OutOfBoundsException.class)
	public void testAddGizmoBallsAtBoundary() throws Exception{
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[3][0], 
				GameBoardJUnit.B_CO_ORDS[3][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
	}
	/**
	 * Move GizmoBall tests.  Considered cases:
	 * - A GizmoBall can be moved to a valid location.
	 * - A GizmoBall cannot be moved onto a location where there is another GizmoBall.
	 * - A GizmoBall cannot be moved onto a location where there is another Gizmo.
	 */
	/**
	 * Test a GizmoBall can be moved to a valid place on the game-board.
	 * @throws Exception
	 */
	@Test
	public void moveGizmoBallTestValid() throws Exception{
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
		GameBoardJUnit.gb.moveGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[1][0], 
				GameBoardJUnit.B_CO_ORDS[1][1]);
		assertTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[1][0], GameBoardJUnit.B_CO_ORDS[1][1]));
		assertFalse(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
	}
	/**
	 * Expected {@link OverlappingGizmoException.class}
	 * Test a GizmoBall cannot be moved directly on top of another GizmoBall.
	 * @throws Exception
	 */
	@Test(expected=OverlappingItemException.class)
	public void moveGizmoBallTestGizmoBallOverlap() throws Exception{
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[7], GameBoardJUnit.B_CO_ORDS[1][0], 
				GameBoardJUnit.B_CO_ORDS[1][1], GameBoardJUnit.B_VELS[1][0], GameBoardJUnit.B_VELS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[1][0], GameBoardJUnit.B_CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.moveGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[1][0], 
				GameBoardJUnit.B_CO_ORDS[1][1]);
	}
	/**
	 * Expected {@link OverlappingGizmoException.class}
	 * Test a GizmoBall cannot be moved such that it would overlap with a Gizmo.
	 * @throws Exception
	 */
	@Test(expected=OverlappingItemException.class)
	public void moveGizmoBallTestGizmoOverlap() throws Exception{
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.moveGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
	}
	
	/**
	 * Remove GizmoBall test cases.  Considered cases:
	 * - Removing GizmoBalls which are assumed to be added
	 * - A GizmoBall which has not been added cannot be removed
	 */
	/**
	 * Test GizmoBalls assumed to have been added to a game-board can be removed
	 * @throws Exception
	 */
	@Test
	public void testRemoveGizmoBallValid() throws Exception{
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[7], GameBoardJUnit.B_CO_ORDS[1][0], 
				GameBoardJUnit.B_CO_ORDS[1][1], GameBoardJUnit.B_VELS[1][0], GameBoardJUnit.B_VELS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[1][0], GameBoardJUnit.B_CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.removeGizmoBall(GameBoardJUnit.IDS[6]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.removeGizmoBall(GameBoardJUnit.IDS[7]);
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
	}
	/**
	 * Expected {@link NoSuchItemException.class}
	 * Test a GizmoBall which has not been added to the game-board cannot be removed
	 * @throws Exception
	 */
	@Test(expected=NoSuchItemException.class)
	public void testRemoveGizmoBallNoSuchItem() throws Exception{
		GameBoardJUnit.gb.removeGizmoBall(GameBoardJUnit.IDS[6]);
	}
	/**
	 * Save game-board tests.  Considered tests:
	 * - Gizmos added to the game-board can be written in the desired formal syntax to file
	 * - An empty game-board can be written in the desired formal syntax to file
	 * @throws Exception
	 */
	/**
	 * Test gizmos are saved in the desired syntax and gravity and friction settings are appended
	 * @throws Exception
	 */
	@Test
	public void testSaveGameBoard() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.AB, GameBoardJUnit.IDS[2], GameBoardJUnit.CO_ORDS[2][0],
				GameBoardJUnit.CO_ORDS[2][1], GameBoardJUnit.CO_ORDS[2][2], GameBoardJUnit.CO_ORDS[2][3]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[2][0], GameBoardJUnit.CO_ORDS[2][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.RF, GameBoardJUnit.IDS[3], GameBoardJUnit.CO_ORDS[3][0],
				GameBoardJUnit.CO_ORDS[3][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[3][0], GameBoardJUnit.CO_ORDS[3][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.LF, GameBoardJUnit.IDS[4], GameBoardJUnit.CO_ORDS[4][0],
				GameBoardJUnit.CO_ORDS[4][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[4][0], GameBoardJUnit.CO_ORDS[4][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.TRI, GameBoardJUnit.IDS[5], GameBoardJUnit.CO_ORDS[5][0],
				GameBoardJUnit.CO_ORDS[5][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[5][0], GameBoardJUnit.CO_ORDS[5][1]));
		StringBuilder builder = new StringBuilder();
		// Append the circle
		builder.append(GameBoardJUnit.CIRC + " " + GameBoardJUnit.IDS[0] + " " + GameBoardJUnit.CO_ORDS[0][0] + " " +
				GameBoardJUnit.CO_ORDS[0][1] + "\n");
		// Append the square
		builder.append(GameBoardJUnit.SQ + " " + GameBoardJUnit.IDS[1] + " " + GameBoardJUnit.CO_ORDS[1][0] + " " +
				GameBoardJUnit.CO_ORDS[1][1] + "\n");
		// Append the absorber
		builder.append(GameBoardJUnit.AB + " " + GameBoardJUnit.IDS[2] + " " + GameBoardJUnit.CO_ORDS[2][0] + " " +
				 GameBoardJUnit.CO_ORDS[2][1] + " " + GameBoardJUnit.CO_ORDS[2][2] + " " +
						GameBoardJUnit.CO_ORDS[2][3] +"\n");
		// Append the right flipper
		builder.append(GameBoardJUnit.RF + " " + GameBoardJUnit.IDS[3] + " " + GameBoardJUnit.CO_ORDS[3][0] + " " +
				GameBoardJUnit.CO_ORDS[3][1] + "\n");
		// Append the left flipper
		builder.append(GameBoardJUnit.LF + " " + GameBoardJUnit.IDS[4] + " " + GameBoardJUnit.CO_ORDS[4][0] + " " +
				GameBoardJUnit.CO_ORDS[4][1] + "\n");
		// Append the triangle
		builder.append(GameBoardJUnit.TRI + " " + GameBoardJUnit.IDS[5] + " " + GameBoardJUnit.CO_ORDS[5][0] + " " +
				GameBoardJUnit.CO_ORDS[5][1] + "\n");
		// Append gravity
		builder.append(GizmoSettings.saveGravity());
		// Append friction
		builder.append(GizmoSettings.saveFriction());
		// Print for a manual inspection, if necessary.
		System.out.println(GameBoardJUnit.gb.saveBoard());
		System.out.println( builder.toString());
		// Order may not be identical so have to split on whitespace and sort.
		String[] splitActual = GameBoardJUnit.gb.saveBoard().split("\\W+");
		String[] splitExpected = builder.toString().split("\\W+");
		
		Arrays.sort(splitActual);
		Arrays.sort(splitExpected);
		
		assertTrue(Arrays.equals(splitExpected, splitActual));
	}
	/**
	 * Test saving an empty board saves both the gravity and friction setings
	 */
	@Test
	public void testClearBoardSave() {
		GameBoardJUnit.gb.clearBoard();
		assertEquals(GameBoardJUnit.gb.saveBoard(), (GizmoSettings.saveGravity() + GizmoSettings.saveFriction()));
	}
	/**
	 * Test the call to clearing the board of gizmos.
	 * @throws Exception
	 */
	@Test
	public void testClearBoard() throws Exception {
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.AB, GameBoardJUnit.IDS[2], GameBoardJUnit.CO_ORDS[2][0],
				GameBoardJUnit.CO_ORDS[2][1], GameBoardJUnit.CO_ORDS[2][2], GameBoardJUnit.CO_ORDS[2][3]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[2][0], GameBoardJUnit.CO_ORDS[2][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.RF, GameBoardJUnit.IDS[3], GameBoardJUnit.CO_ORDS[3][0],
				GameBoardJUnit.CO_ORDS[3][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[3][0], GameBoardJUnit.CO_ORDS[3][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.LF, GameBoardJUnit.IDS[4], GameBoardJUnit.CO_ORDS[4][0],
				GameBoardJUnit.CO_ORDS[4][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[4][0], GameBoardJUnit.CO_ORDS[4][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.TRI, GameBoardJUnit.IDS[5], GameBoardJUnit.CO_ORDS[5][0],
				GameBoardJUnit.CO_ORDS[5][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[5][0], GameBoardJUnit.CO_ORDS[5][1]));
		
		GameBoardJUnit.gb.clearBoard();
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[2][0], GameBoardJUnit.CO_ORDS[2][1]));
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[3][0], GameBoardJUnit.CO_ORDS[3][1]));
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[4][0], GameBoardJUnit.CO_ORDS[4][1]));
		assertFalse(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[5][0], GameBoardJUnit.CO_ORDS[5][1]));
	}
	
	/**
	 * Test setting the gravity of the board.
	 */
	@Test
	public void testSetGravity(){
		GameBoardJUnit.gb.setGravity(GameBoardJUnit.PHYS_VALUE);
		assertEquals(GameBoardJUnit.PHYS_VALUE, GizmoSettings.getGravity(), GameBoardJUnit.DELTA);
	}
	
	/**
	 * Test setting the friction values of the board.
	 */
	@Test
	public void testSetFriction(){
		GameBoardJUnit.gb.setFriction(GameBoardJUnit.PHYS_VALUE, GameBoardJUnit.PHYS_VALUE);
		assertEquals(GameBoardJUnit.PHYS_VALUE, GizmoSettings.getMu(), GameBoardJUnit.DELTA);
		assertEquals(GameBoardJUnit.PHYS_VALUE, GizmoSettings.getMu2(), GameBoardJUnit.DELTA);
	}
	
	/**
	 * Test gizmos can be connected to other gizmos and themselves
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testGizmoGizmoConnectionUnique() throws Exception {
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		Assert.assertTrue(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[1]));
		Assert.assertTrue(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]));
	}
	
	/**
	 * Test gizmos cannot be connected to the same gizmo twice
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testGizmoGizmoConnectionRepeat() throws Exception {
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		Assert.assertTrue(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[1]));
		Assert.assertTrue(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]));
		Assert.assertFalse(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]));	
	}
	/**
	 * Test a gizmo which does not exist cannot be connected to itself
	 * @throws Exception
	 */
	@Test(expected=NoSuchItemException.class)
	public void testGizmoGizmoConnectionNoSuchItem() throws Exception{
		GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]);	
	}
	
	/**
	 * Test gizmos can be disconnected from other gizmos where the connection aspect is assumed to be functional
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testGizmoGizmoDisconnectionUnique() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		Assume.assumeTrue(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[1]));
		Assume.assumeTrue(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]));
		
		Assert.assertTrue(GameBoardJUnit.gb.disconnect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[1]));
		Assert.assertTrue(GameBoardJUnit.gb.disconnect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]));
	}
	/**
	 * Test gizmos cannot be disconnected from the same gizmo twice
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@Test
	public void testGizmoGizmoDisconnectionRepeat() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.SQ, GameBoardJUnit.IDS[1], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[1][0], GameBoardJUnit.CO_ORDS[1][1]));
		
		Assume.assumeTrue(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[1]));
		Assume.assumeTrue(GameBoardJUnit.gb.connect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]));
		
		Assert.assertTrue(GameBoardJUnit.gb.disconnect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[1]));
		Assert.assertTrue(GameBoardJUnit.gb.disconnect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]));
		Assert.assertFalse(GameBoardJUnit.gb.disconnect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]));	
	}
	/**
	 * Test a gizmo which does not exist cannot be disconnected
	 * @throws Exception
	 */
	@Test(expected=NoSuchItemException.class)
	public void testGizmoGizmoDisconnectionNoSuchItem() throws Exception{
		GameBoardJUnit.gb.disconnect(GameBoardJUnit.IDS[0], GameBoardJUnit.IDS[0]);	
	}
	
	/**
	 * Test name fetching from the model.  Considered case:
	 * - the gizmo is perfectly valid and exists within the model
	 * - the gizmo has been added and removed and should no longer be able to be referenced
	 * - the gizmoball is perfectly valid and exists within the model
	 */
	@Test
	public void testGizmoNameFetch() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		
		assertEquals(GameBoardJUnit.IDS[0],GameBoardJUnit.gb.gizmoNameAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
	}
	
	@Test(expected=NoSuchItemException.class)
	public void testGizmoNameFetchInvalid() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[0][0],
				GameBoardJUnit.CO_ORDS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		GameBoardJUnit.gb.removeGizmo(GameBoardJUnit.IDS[0]);
		Assume.assumeTrue(!GameBoardJUnit.gb.hasGizmoAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]));
		GameBoardJUnit.gb.gizmoNameAt(GameBoardJUnit.CO_ORDS[0][0], GameBoardJUnit.CO_ORDS[0][1]);
	}
	
	@Test
	public void testGizmoBallNameFetch() throws Exception{
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
		Assume.assumeTrue(GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
		assertEquals(GameBoardJUnit.IDS[6], GameBoardJUnit.gb.gizmoBallNameAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
	}
	
	@Test(expected=NoSuchItemException.class)
	public void testGizmoBallNameFetchInvalid() throws Exception{
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
		GameBoardJUnit.gb.hasGizmoBallAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]);
		assertEquals(GameBoardJUnit.IDS[6], GameBoardJUnit.gb.gizmoBallNameAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]));
		GameBoardJUnit.gb.removeGizmoBall(GameBoardJUnit.IDS[6]);
		GameBoardJUnit.gb.gizmoBallNameAt(GameBoardJUnit.B_CO_ORDS[0][0], GameBoardJUnit.B_CO_ORDS[0][1]);
	}
	
	/**
	 * Miscellaneous tests.  Calling for coverage and asserting no
	 * unexpected exceptions will be thrown.
	 */
	@Test
	public void testGetLooks() throws Exception{
		assertNotNull(GameBoardJUnit.gb.getGizmoLooks());
		assertNotNull(GameBoardJUnit.gb.getGizmoBallLooks());
	}
	
	@Test
	public void testGetGizmoDimension() throws Exception{
		GameBoardJUnit.gb.getGizmoDimensions(GameBoardJUnit.CIRC);
		GameBoardJUnit.gb.getGizmoDimensions(GameBoardJUnit.BALL);
	}
	
	@Test
	public void testGetGizmoDimensionByName() throws Exception{
		GameBoardJUnit.gb.addGizmo(GameBoardJUnit.CIRC, GameBoardJUnit.IDS[0], GameBoardJUnit.CO_ORDS[1][0],
				GameBoardJUnit.CO_ORDS[1][1]);

		assertNotNull(GameBoardJUnit.gb.getGizmoDimensionByName(GameBoardJUnit.IDS[0]));
		
		GameBoardJUnit.gb.addGizmoBall(GameBoardJUnit.IDS[6], GameBoardJUnit.B_CO_ORDS[0][0], 
				GameBoardJUnit.B_CO_ORDS[0][1], GameBoardJUnit.B_VELS[0][0], GameBoardJUnit.B_VELS[0][1]);
		GameBoardJUnit.gb.getGizmoBallDimensionByName(GameBoardJUnit.IDS[6]);
	}
}
