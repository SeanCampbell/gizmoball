package testing;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import model.GizmoFactory;
import model.IGizmoFactory;
import model.gizmos.Flipper;
import model.gizmos.IGizmo;
import model.gizmos.RightFlipper;

import org.junit.BeforeClass;
import org.junit.Test;

import shapes.GeometryCircle;
import shapes.GeometryPolygon;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class RightFlipperJUnit {
	private static final int[][] ARGS = {{3, 1}, {10, 4}, {0, 0}};
	private static final String[] NAMES = {"RF1", "RF2", "RF3", "RF4", "RF5"};
	private static final IGizmoFactory FACTORY = GizmoFactory.getInstance();

	private static String RIGHT_FLIPPER_NAME;
	@SuppressWarnings("unused")
	private static Color COLOR;
	private static double CENTRE_X, CENTRE_Y, WIDTH, LENGTH, OFFSET, 
	RIGHT_INIT_RAD, RADIUS;
	private static int RIGHT_INIT_OR;

	@SuppressWarnings("unused")
	private static IGizmo rf1, rf2, rf3, rf4, rf5;

	private static int orientation;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		RIGHT_FLIPPER_NAME = (String)PrivateAccessor.getPrivateField(RightFlipper.class, "NAME");
		COLOR = (Color)PrivateAccessor.getPrivateField(Flipper.class, "DEFAULT");
		CENTRE_X = (Double)PrivateAccessor.getPrivateField(Flipper.class, "CENTRE_X");
		CENTRE_Y = (Double)PrivateAccessor.getPrivateField(Flipper.class, "CENTRE_Y");
		WIDTH = (Double)PrivateAccessor.getPrivateField(Flipper.class, "WIDTH");
		LENGTH = (Double)PrivateAccessor.getPrivateField(Flipper.class, "LENGTH");
		OFFSET = (Double)PrivateAccessor.getPrivateField(Flipper.class, "OFFSET");
		RIGHT_INIT_RAD = (Double)PrivateAccessor.getPrivateField(Flipper.class, "RIGHT_INIT_RAD");
		RIGHT_INIT_OR = (Integer)PrivateAccessor.getPrivateField(Flipper.class, "RIGHT_INIT_OR");
		RADIUS = (Double)PrivateAccessor.getPrivateField(Flipper.class, "RADIUS");

		rf1 = FACTORY.createGizmo(RIGHT_FLIPPER_NAME, NAMES[0], ARGS[0][0], ARGS[0][1]);
		rf2 = FACTORY.createGizmo(RIGHT_FLIPPER_NAME, NAMES[1], ARGS[0][0], ARGS[0][1]);
		rf3 = FACTORY.createGizmo(RIGHT_FLIPPER_NAME, NAMES[2], ARGS[1][0], ARGS[1][1]);
		rf4 = FACTORY.createGizmo(RIGHT_FLIPPER_NAME, NAMES[3], ARGS[2][0], ARGS[2][1]);
		rf5 = FACTORY.createGizmo(RIGHT_FLIPPER_NAME, NAMES[4], ARGS[1][0], ARGS[1][1]);
	}

	private Collection<IGeometry> createFlipper(double x, double y, int orientation, double angle, LogicalPoint pivot, LogicalPoint unitCentre) {
		List<LogicalPoint> points = new ArrayList<>();
		points.add(pivot.rotate(unitCentre, orientation));
		points.add(rotate(new LogicalPoint(x + OFFSET, y
				+ LENGTH + OFFSET), unitCentre,
				pivot, orientation, angle));
		points.add(rotate(new LogicalPoint(x, y + OFFSET),
				unitCentre, pivot, orientation, angle));
		points.add(rotate(new LogicalPoint(x, y + OFFSET
				+ LENGTH), unitCentre, pivot,
				orientation, angle));
		points.add(rotate(new LogicalPoint(x + WIDTH, y
				+ OFFSET + LENGTH), unitCentre,
				pivot, orientation, angle));
		points.add(rotate(new LogicalPoint(x + WIDTH, y
				+ OFFSET), unitCentre, pivot,
				orientation, angle));

		List<IGeometry> geo = new ArrayList<>();
		geo.add(new GeometryCircle(points.get(0), RADIUS));
		geo.add(new GeometryCircle(points.get(1), RADIUS));
		geo.add(new GeometryPolygon(points.subList(2, points.size())));
		return geo;
	}

	private LogicalPoint rotate(final LogicalPoint point,
			final LogicalPoint centre, final LogicalPoint pivot,
			final int orientation, final double angle) {
		LogicalPoint temp = point.rotate(pivot, angle);
		temp = temp.rotate(centre, orientation);
		return temp;
	}

	/**
	 * Asserts the the correct geometries are produced.
	 */
	@Test
	public void testGeos() {
		Collection<IGeometry> actual = rf1.getGeometryList();
		
		LogicalPoint pivot = new LogicalPoint(OFFSET + ARGS[0][0], OFFSET + ARGS[0][1]);
		LogicalPoint unitCentre = new LogicalPoint(CENTRE_X + ARGS[0][0], CENTRE_Y +ARGS[0][1]);
		Collection<IGeometry> expected = this.createFlipper(ARGS[0][0], ARGS[0][1], RIGHT_INIT_OR, RIGHT_INIT_RAD, pivot, unitCentre);

		assertEquals(actual.size(), expected.size());
		assertTrue(actual.containsAll(expected));
		assertTrue(expected.containsAll(actual));
	}

	/**
	 * Asserts that the flipper can move.
	 */
	@Test
	public void testMove() {
		rf2.move(5, 7);
		Collection<IGeometry> geoL = rf2.getGeometryList();
		LogicalPoint pivot = new LogicalPoint(OFFSET + 5, OFFSET + 7);
		LogicalPoint unitCentre = new LogicalPoint(CENTRE_X + 5, CENTRE_Y + 7);
		Collection<IGeometry> geo = createFlipper(5, 7, RIGHT_INIT_OR, RIGHT_INIT_RAD, pivot, unitCentre);

		assertEquals(geoL.size(), geo.size());
		assertTrue(geoL.containsAll(geo));
		assertTrue(geo.containsAll(geoL));
	}

	/**
	 * Tests that a flipper can rotate and then produce the correct geometries.
	 */
	@Test
	public void testRotation() {
		rf1.rotate();
		Collection<IGeometry> actual = rf1.getGeometryList();

		int thisOrient = (RIGHT_INIT_OR + 1) % 4;

		LogicalPoint pivot = new LogicalPoint(OFFSET + ARGS[0][0], OFFSET +ARGS[0][1]);
		LogicalPoint unitCentre = new LogicalPoint(CENTRE_X + ARGS[0][0], CENTRE_Y +ARGS[0][1]);
		Collection<IGeometry> expected = this.createFlipper(ARGS[0][0], ARGS[0][1], thisOrient, RIGHT_INIT_RAD, pivot, unitCentre);

		assertEquals(actual.size(), expected.size());
		assertTrue(actual.containsAll(expected));
		assertTrue(expected.containsAll(actual));
	}
	
	/**
	 * Tests the tick method.
	 */
	@Test
	public void testTick() {
		rf4.tick(0.02);
		@SuppressWarnings("unused")
		Collection<IGeometry> actual = rf4.getGeometryList();
		
		int thisOrient = orientation;

		LogicalPoint pivot = new LogicalPoint(OFFSET + ARGS[1][0], OFFSET +ARGS[1][1]);
		LogicalPoint unitCentre = new LogicalPoint(CENTRE_X + ARGS[1][0], CENTRE_Y +ARGS[1][1]);
		@SuppressWarnings("unused")
		Collection<IGeometry> expected = this.createFlipper(ARGS[1][0], ARGS[1][1], thisOrient, 0, pivot, unitCentre);

		
	}
	
	/**
	 * Checks the save string is valid.
	 */
	@Test
	public void testSaveString() {
		String actual = rf3.getSaveString();
		StringBuilder stringBuild = new StringBuilder();
		stringBuild.append(RIGHT_FLIPPER_NAME + " " + NAMES[2] + " " + ARGS[1][0] + " " + ARGS[1][1] + "\n");
		assertEquals(actual, stringBuild.toString());
		
		rf3.rotate();
		actual = rf3.getSaveString();
		stringBuild.append("Rotate" + NAMES[2] + "\n");
		assertEquals(actual, stringBuild.toString());
	}
	
	/**
	 * Tests the width of the flipper.
	 */
	@Test
	public void testWidth() {
		assertEquals(WIDTH, rf1.getWidth(), 0.01);
	}
	
	/**
	 * Tests the height of the flipper.
	 */
	@Test 
	public void testHeight() {
		assertEquals(LENGTH, rf1.getLength(), 0.01);
	}
	
	/**
	 * Asserts that flippers cannot vary in size.
	 */
	@Test
	public void testVariableSize() {
		assertFalse(rf1.isVariableSize());
		assertFalse(rf2.isVariableSize());
		assertFalse(rf3.isVariableSize());
	}
	
	/**
	 * Asserts that we return the correct origin value.
	 * The only time the origin value should change is after a move.
	 */
	@Test
	public void testOrigin() {
		assertEquals(rf4.getOrigin(), new LogicalPoint(ARGS[2][0], ARGS[2][1]));
		rf4.move(15, 15);
		assertEquals(rf4.getOrigin(), new LogicalPoint(15,15));
	
	}

}
