package testing;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.Collection;
import java.util.Iterator;

import model.GizmoFactory;
import model.IGizmoFactory;
import model.gizmos.CircleBumper;
import model.gizmos.IGizmo;

import org.junit.BeforeClass;
import org.junit.Test;

import shapes.GeometryCircle;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class CircleBumperJUnit {	
	private static final int[][] ARGS = {{3, 1}, {1, 3}, {0, 0}};
	private static final String[] NAMES = {"C1", "C2"};
	private static final IGizmoFactory FACTORY = GizmoFactory.getInstance();
	
	private static String BUMPER_NAME;
	private static Color COLOR1, COLOR2;
	private static double OFFSET, RADIUS;
	
	private static IGizmo circ1, circ2;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// Access the private members via reflection for unit testing.  Don't wish to expose.
		CircleBumperJUnit.BUMPER_NAME = (String)PrivateAccessor.getPrivateField(CircleBumper.class, "NAME");
		CircleBumperJUnit.COLOR1 = (Color)PrivateAccessor.getPrivateField(CircleBumper.class, "DEFAULT");
		CircleBumperJUnit.COLOR2 = (Color)PrivateAccessor.getPrivateField(CircleBumper.class, "ONACTION");
		CircleBumperJUnit.OFFSET = (Double)PrivateAccessor.getPrivateField(CircleBumper.class, "OFFSET");
		CircleBumperJUnit.RADIUS = (Double)PrivateAccessor.getPrivateField(CircleBumper.class, "RADIUS");
		
		CircleBumperJUnit.circ1 = CircleBumperJUnit.FACTORY.createGizmo(CircleBumperJUnit.BUMPER_NAME,
				CircleBumperJUnit.NAMES[0], CircleBumperJUnit.ARGS[0][0], CircleBumperJUnit.ARGS[0][1]);
		CircleBumperJUnit.circ2 = CircleBumperJUnit.FACTORY.createGizmo(CircleBumperJUnit.BUMPER_NAME, 
				CircleBumperJUnit.NAMES[1], CircleBumperJUnit.ARGS[1][0], CircleBumperJUnit.ARGS[1][1]);				
	}

	/**
	 * Asserts that the circle Gizmo throws an exception upon rotation.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testRotation(){
		CircleBumperJUnit.circ1.rotate();
	}
	
	
	/**
	 * Asserts that the shapes are created correctly for a CircleBumper.
	 */
	@Test
	public void testAbsList(){
		Collection<IGeometry> actual = CircleBumperJUnit.circ1.getGeometryList();
		IGeometry circ = new GeometryCircle(new LogicalPoint(CircleBumperJUnit.ARGS[0][0] + CircleBumperJUnit.OFFSET,
				CircleBumperJUnit.ARGS[0][1] + CircleBumperJUnit.OFFSET), CircleBumperJUnit.RADIUS);
		assertEquals(1, actual.size());
		Iterator<IGeometry> iter = actual.iterator();
		assertEquals(circ, iter.next());
		assertFalse(iter.hasNext());
	}
	
	/**
	 * Asserts the color changes when an action occurs.
	 */
	@Test
	public void testAction(){
		assertEquals(CircleBumperJUnit.COLOR1, CircleBumperJUnit.circ1.getColor());
		circ1.doAction();
		assertFalse(CircleBumperJUnit.circ1.getColor().equals(CircleBumperJUnit.COLOR1));
		assertEquals(CircleBumperJUnit.COLOR2, CircleBumperJUnit.circ1.getColor());
		circ1.doAction();
		assertEquals(CircleBumperJUnit.COLOR1, CircleBumperJUnit.circ1.getColor());
	}
	
	/**
	 * Asserts the position changes accordingly.
	 */
	@Test
	public void testMove(){
		CircleBumperJUnit.circ2.move(CircleBumperJUnit.ARGS[2][0], CircleBumperJUnit.ARGS[2][1]);
		Collection<IGeometry> actual = CircleBumperJUnit.circ2.getGeometryList();
		IGeometry circ = new GeometryCircle(new LogicalPoint(CircleBumperJUnit.ARGS[2][0] + CircleBumperJUnit.OFFSET,
				CircleBumperJUnit.ARGS[2][1] + CircleBumperJUnit.OFFSET), CircleBumperJUnit.RADIUS);
		assertEquals(1, actual.size());
		Iterator<IGeometry> iter = actual.iterator();
		assertEquals(circ, iter.next());
		assertFalse(iter.hasNext());
	}
	
	/**
	 * Checks the save string is valid.
	 */
	@Test
	public void testSaveString(){
		StringBuilder builder = new StringBuilder();
		LogicalPoint p = new LogicalPoint(CircleBumperJUnit.ARGS[0][0], CircleBumperJUnit.ARGS[0][1]);
		builder.append(CircleBumperJUnit.BUMPER_NAME + " " +
					CircleBumperJUnit.NAMES[0] + " " + p.toString() + "\n");
		assertEquals(builder.toString(), CircleBumperJUnit.circ1.getSaveString());
	}
	
	@Test
	public void testEquals() throws Exception{
		testObjectProperties();
		testExamples();
	}
	
	private void testObjectProperties() throws Exception{
		//Test valid object is not null.
		assertFalse("Error: Object of " + CircleBumperJUnit.circ1.getClass().toString() + " equal to null.",
						CircleBumperJUnit.circ1.equals(null));
		//Test reflexive property.
		assertTrue("Error: "+ CircleBumperJUnit.circ1.getClass().toString()+" is not reflexive.",
				 CircleBumperJUnit.circ1.equals( CircleBumperJUnit.circ1));
		//Test symmetric.
		IGizmo temp = CircleBumperJUnit.FACTORY.createGizmo(CircleBumperJUnit.BUMPER_NAME,
				CircleBumperJUnit.NAMES[0], CircleBumperJUnit.ARGS[0][0], CircleBumperJUnit.ARGS[0][1]);
		assertEquals("Error: "+ CircleBumperJUnit.circ1.getClass().toString()+" is not symmetric.", CircleBumperJUnit.circ1, temp);
		//Test transitive.
		IGizmo temp2 = CircleBumperJUnit.FACTORY.createGizmo(CircleBumperJUnit.BUMPER_NAME,
				CircleBumperJUnit.NAMES[0], CircleBumperJUnit.ARGS[0][0], CircleBumperJUnit.ARGS[0][1]);
		if(CircleBumperJUnit.circ1.equals(temp)&temp.equals(temp2))
					assertEquals("Error: "+ CircleBumperJUnit.circ1.getClass().toString()+" is not transitive.",
							 CircleBumperJUnit.circ1,temp2);
		//Test consistency.
		assertTrue("Error: "+ CircleBumperJUnit.circ1.getClass().toString()+" is not consistent.",
				 CircleBumperJUnit.circ1.equals(temp)== CircleBumperJUnit.circ1.equals(temp));
	}
	
	private void testExamples() throws Exception{
		// Check equality; for gizmos equality should be dependant only upon the name!
		IGizmo eq = CircleBumperJUnit.FACTORY.createGizmo(CircleBumperJUnit.BUMPER_NAME,
				CircleBumperJUnit.NAMES[0], CircleBumperJUnit.ARGS[0][0], CircleBumperJUnit.ARGS[0][1]);
		assertEquals(eq, CircleBumperJUnit.circ1);
		IGizmo eq2 = CircleBumperJUnit.FACTORY.createGizmo(CircleBumperJUnit.BUMPER_NAME,
				CircleBumperJUnit.NAMES[0], CircleBumperJUnit.ARGS[1][0], CircleBumperJUnit.ARGS[1][1]);
		assertEquals(eq2, CircleBumperJUnit.circ1);
		IGizmo neq = CircleBumperJUnit.FACTORY.createGizmo(CircleBumperJUnit.BUMPER_NAME,
				CircleBumperJUnit.NAMES[1], CircleBumperJUnit.ARGS[0][0], CircleBumperJUnit.ARGS[0][1]);
		assertFalse(CircleBumperJUnit.circ1.equals(neq));
	}
}
