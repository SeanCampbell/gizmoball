package testing;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import model.GizmoFactory;
import model.IGizmoFactory;
import model.gizmos.IGizmo;
import model.gizmos.SquareBumper;

import org.junit.BeforeClass;
import org.junit.Test;

import shapes.GeometryPolygon;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class SquareBumperJUnit {
	private static final int[][] ARGS = {{3, 1}, {1, 3}, {0, 0}};
	private static final String[] NAMES = {"S1", "S2"};
	private static final IGizmoFactory FACTORY = GizmoFactory.getInstance();
	
	private static String BUMPER_NAME;
	private static Color COLOR1, COLOR2;
	private static double OFFSET;	
	
	private static IGizmo sq1, sq2;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// Access the private members via reflection for unit testing.  Don't wish to expose.
		SquareBumperJUnit.BUMPER_NAME = (String)PrivateAccessor.getPrivateField(SquareBumper.class, "NAME");
		SquareBumperJUnit.COLOR1 = (Color)PrivateAccessor.getPrivateField(SquareBumper.class, "DEFAULT");
		SquareBumperJUnit.COLOR2 = (Color)PrivateAccessor.getPrivateField(SquareBumper.class, "ONACTION");
		SquareBumperJUnit.OFFSET = (Double)PrivateAccessor.getPrivateField(SquareBumper.class, "OFFSET");
		
		SquareBumperJUnit.sq1 = SquareBumperJUnit.FACTORY.createGizmo(SquareBumperJUnit.BUMPER_NAME,
				SquareBumperJUnit.NAMES[0], SquareBumperJUnit.ARGS[0][0], SquareBumperJUnit.ARGS[0][1]);
		SquareBumperJUnit.sq2 = SquareBumperJUnit.FACTORY.createGizmo(SquareBumperJUnit.BUMPER_NAME, 
				SquareBumperJUnit.NAMES[1], SquareBumperJUnit.ARGS[1][0], SquareBumperJUnit.ARGS[1][1]);				
	}

	/**
	 * Asserts that the square Gizmo throws an exception upon rotation.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testRotation(){
		SquareBumperJUnit.sq1.rotate();
	}
	
	/**
	 * Asserts that the shapes are created correctly for a CircleBumper.
	 */
	@Test
	public void testAbsList(){
		Collection<IGeometry> actual = SquareBumperJUnit.sq1.getGeometryList();
		IGeometry expected = this.createSquare(SquareBumperJUnit.ARGS[0][0], SquareBumperJUnit.ARGS[0][1]);
		assertEquals(1, actual.size());
		Iterator<IGeometry> iter = actual.iterator();
		assertEquals(expected, iter.next());
		assertFalse(iter.hasNext());
	}
	
	private IGeometry createSquare(final int x, final int y){
		Collection<LogicalPoint> points = new ArrayList<>();
		points.add(new LogicalPoint(x, y));
		points.add(new LogicalPoint(x + SquareBumperJUnit.OFFSET, y));
		points.add(new LogicalPoint(x +SquareBumperJUnit.OFFSET, y + SquareBumperJUnit.OFFSET));
		points.add(new LogicalPoint(x, y + SquareBumperJUnit.OFFSET));
		IGeometry geom = new GeometryPolygon(points);
		return geom;
	}
	
	/**
	 * Asserts the color changes when an action occurs.
	 */
	@Test
	public void testAction(){
		assertEquals(SquareBumperJUnit.COLOR1, SquareBumperJUnit.sq1.getColor());
		sq1.doAction();
		assertFalse(SquareBumperJUnit.sq1.getColor().equals(SquareBumperJUnit.COLOR1));
		assertEquals(SquareBumperJUnit.COLOR2, SquareBumperJUnit.sq1.getColor());
		sq1.doAction();
		assertEquals(SquareBumperJUnit.COLOR1, SquareBumperJUnit.sq1.getColor());
	}
	
	/**
	 * Asserts the position changes accordingly.
	 */
	@Test
	public void testMove(){
		SquareBumperJUnit.sq2.move(SquareBumperJUnit.ARGS[2][0], SquareBumperJUnit.ARGS[2][1]);
		Collection<IGeometry> actual = SquareBumperJUnit.sq2.getGeometryList();
		IGeometry expected =  this.createSquare(SquareBumperJUnit.ARGS[2][0], SquareBumperJUnit.ARGS[2][1]);
		assertEquals(1, actual.size());
		Iterator<IGeometry> iter = actual.iterator();
		assertEquals(expected, iter.next());
		assertFalse(iter.hasNext());
	}
	
	/**
	 * Checks the save string is valid.
	 */
	@Test
	public void testSaveString(){
		StringBuilder builder = new StringBuilder();
		LogicalPoint p = new LogicalPoint(SquareBumperJUnit.ARGS[0][0], SquareBumperJUnit.ARGS[0][1]);
		builder.append(SquareBumperJUnit.BUMPER_NAME + " " +
					SquareBumperJUnit.NAMES[0] + " " + p.toString() + "\n");
		assertEquals(builder.toString(), SquareBumperJUnit.sq1.getSaveString());
	}
	
	@Test
	public void testEquals() throws Exception{
		testObjectProperties();
		testExamples();
	}
	
	private void testObjectProperties() throws Exception{
		//Test valid object is not null.
		assertFalse("Error: Object of " + SquareBumperJUnit.sq1.getClass().toString() + " equal to null.",
						SquareBumperJUnit.sq1.equals(null));
		//Test reflexive property.
		assertTrue("Error: "+ SquareBumperJUnit.sq1.getClass().toString()+" is not reflexive.",
				 SquareBumperJUnit.sq1.equals( SquareBumperJUnit.sq1));
		//Test symmetric.
		IGizmo temp = SquareBumperJUnit.FACTORY.createGizmo(SquareBumperJUnit.BUMPER_NAME,
				SquareBumperJUnit.NAMES[0], SquareBumperJUnit.ARGS[0][0], SquareBumperJUnit.ARGS[0][1]);
		assertEquals("Error: "+ SquareBumperJUnit.sq1.getClass().toString()+" is not symmetric.", SquareBumperJUnit.sq1, temp);
		//Test transitive.
		IGizmo temp2 = SquareBumperJUnit.FACTORY.createGizmo(SquareBumperJUnit.BUMPER_NAME,
				SquareBumperJUnit.NAMES[0], SquareBumperJUnit.ARGS[0][0], SquareBumperJUnit.ARGS[0][1]);
		if(SquareBumperJUnit.sq1.equals(temp)&temp.equals(temp2))
					assertEquals("Error: "+ SquareBumperJUnit.sq1.getClass().toString()+" is not transitive.",
							 SquareBumperJUnit.sq1,temp2);
		//Test consistency.
		assertTrue("Error: "+ SquareBumperJUnit.sq1.getClass().toString()+" is not consistent.",
				 SquareBumperJUnit.sq1.equals(temp)== SquareBumperJUnit.sq1.equals(temp));
	}
	
	private void testExamples() throws Exception{
		// Check equality; for gizmos equality should be dependant only upon the name!
		IGizmo eq = SquareBumperJUnit.FACTORY.createGizmo(SquareBumperJUnit.BUMPER_NAME,
				SquareBumperJUnit.NAMES[0], SquareBumperJUnit.ARGS[0][0], SquareBumperJUnit.ARGS[0][1]);
		assertEquals(eq, SquareBumperJUnit.sq1);
		IGizmo eq2 = SquareBumperJUnit.FACTORY.createGizmo(SquareBumperJUnit.BUMPER_NAME,
				SquareBumperJUnit.NAMES[0], SquareBumperJUnit.ARGS[1][0], SquareBumperJUnit.ARGS[1][1]);
		assertEquals(eq2, SquareBumperJUnit.sq1);
		IGizmo neq = SquareBumperJUnit.FACTORY.createGizmo(SquareBumperJUnit.BUMPER_NAME,
				SquareBumperJUnit.NAMES[1], SquareBumperJUnit.ARGS[0][0], SquareBumperJUnit.ARGS[0][1]);
		assertFalse(SquareBumperJUnit.sq1.equals(neq));
	}
}
