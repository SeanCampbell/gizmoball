package testing;

import static org.junit.Assert.*;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import model.GizmoFactory;
import model.IGizmoFactory;
import model.gizmos.IGizmo;
import model.gizmos.TriangleBumper;

import org.junit.BeforeClass;
import org.junit.Test;

import shapes.GeometryPolygon;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class TriangleBumperJUnit {
	private static final int[][] ARGS = {{3, 1}, {1, 3}, {0, 0}};
	private static final String[] NAMES = {"T1", "T2"};
	private static final IGizmoFactory FACTORY = GizmoFactory.getInstance();
	
	private static String BUMPER_NAME;
	private static Color COLOR1, COLOR2;
	private static double OFFSET;	
	
	private static IGizmo tr1, tr2;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// Access the private members via reflection for unit testing.  Don't wish to expose.
		TriangleBumperJUnit.BUMPER_NAME = (String)PrivateAccessor.getPrivateField(TriangleBumper.class, "NAME");
		TriangleBumperJUnit.COLOR1 = (Color)PrivateAccessor.getPrivateField(TriangleBumper.class, "DEFAULT");
		TriangleBumperJUnit.COLOR2 = (Color)PrivateAccessor.getPrivateField(TriangleBumper.class, "ONACTION");
		TriangleBumperJUnit.OFFSET = (Double)PrivateAccessor.getPrivateField(TriangleBumper.class, "OFFSET");
		
		TriangleBumperJUnit.tr1 = TriangleBumperJUnit.FACTORY.createGizmo(TriangleBumperJUnit.BUMPER_NAME,
				TriangleBumperJUnit.NAMES[0], TriangleBumperJUnit.ARGS[0][0], TriangleBumperJUnit.ARGS[0][1]);
		TriangleBumperJUnit.tr2 = TriangleBumperJUnit.FACTORY.createGizmo(TriangleBumperJUnit.BUMPER_NAME, 
				TriangleBumperJUnit.NAMES[1], TriangleBumperJUnit.ARGS[1][0], TriangleBumperJUnit.ARGS[1][1]);				
	}

	/**
	 * Asserts that the triangle Gizmo can rotate and compares it against a pi/2
	 * clockwise rotated IPolygon.  Asserts by checking the shapes points against
	 * one another.
	 */
	@Test
	public void testRotation(){
		TriangleBumperJUnit.tr1.rotate();
		Collection<IGeometry> actual = TriangleBumperJUnit.tr1.getGeometryList();
		IGeometry expected = this.createTriangleRotated(TriangleBumperJUnit.ARGS[0][0],
				TriangleBumperJUnit.ARGS[0][1]);
		assertEquals(1, actual.size());
		Iterator<IGeometry> iter = actual.iterator();
		assertEquals(expected, iter.next());
		assertFalse(iter.hasNext());
	}
	
	private IGeometry createTriangleRotated(final double x, final double y){
		Collection<LogicalPoint> points = new ArrayList<>();
		points.add(new LogicalPoint(x + TriangleBumperJUnit.OFFSET, y));
		points.add(new LogicalPoint(x + TriangleBumperJUnit.OFFSET,
				y + TriangleBumperJUnit.OFFSET));
		points.add(new LogicalPoint(x, y));
		
		IGeometry triangle = new GeometryPolygon(points);
		return triangle;
	}
			
	/**
	 * Asserts the color changes when an action occurs.
	 */
	@Test
	public void testAction(){
		assertEquals(TriangleBumperJUnit.COLOR1, TriangleBumperJUnit.tr1.getColor());
		tr1.doAction();
		assertFalse(TriangleBumperJUnit.tr1.getColor().equals(TriangleBumperJUnit.COLOR1));
		assertEquals(TriangleBumperJUnit.COLOR2, TriangleBumperJUnit.tr1.getColor());
		tr1.doAction();
		assertEquals(TriangleBumperJUnit.COLOR1, TriangleBumperJUnit.tr1.getColor());
	}
	
	/**
	 * Asserts the position changes accordingly for a rotated triangle.
	 */
	@Test
	public void testMove(){
		TriangleBumperJUnit.tr2.move(TriangleBumperJUnit.ARGS[2][0], TriangleBumperJUnit.ARGS[2][1]);
		TriangleBumperJUnit.tr2.rotate();
		Collection<IGeometry> actual = TriangleBumperJUnit.tr2.getGeometryList();
		IGeometry expected =  this.createTriangleRotated(TriangleBumperJUnit.ARGS[2][0], TriangleBumperJUnit.ARGS[2][1]);
		assertEquals(1, actual.size());
		Iterator<IGeometry> iter = actual.iterator();
		assertEquals(expected, iter.next());
		assertFalse(iter.hasNext());
	}
	
	/**
	 * Checks the save string is valid.
	 */
	@Test
	public void testSaveString(){
		StringBuilder builder = new StringBuilder();
		LogicalPoint p = new LogicalPoint(TriangleBumperJUnit.ARGS[0][0], TriangleBumperJUnit.ARGS[0][1]);
		builder.append(TriangleBumperJUnit.BUMPER_NAME + " " +
					TriangleBumperJUnit.NAMES[0] + " " + p.toString() + "\n");
		assertEquals(builder.toString(), TriangleBumperJUnit.tr1.getSaveString());
	}
	
	@Test
	public void testEquals() throws Exception{
		testObjectProperties();
		testExamples();
	}
	
	private void testObjectProperties() throws Exception{
		//Test valid object is not null.
		assertFalse("Error: Object of " + TriangleBumperJUnit.tr1.getClass().toString() + " equal to null.",
						TriangleBumperJUnit.tr1.equals(null));
		//Test reflexive property.
		assertTrue("Error: "+ TriangleBumperJUnit.tr1.getClass().toString()+" is not reflexive.",
				 TriangleBumperJUnit.tr1.equals( TriangleBumperJUnit.tr1));
		//Test symmetric.
		IGizmo temp = TriangleBumperJUnit.FACTORY.createGizmo(TriangleBumperJUnit.BUMPER_NAME,
				TriangleBumperJUnit.NAMES[0], TriangleBumperJUnit.ARGS[0][0], TriangleBumperJUnit.ARGS[0][1]);
		assertEquals("Error: "+ TriangleBumperJUnit.tr1.getClass().toString()+" is not symmetric.", TriangleBumperJUnit.tr1, temp);
		//Test transitive.
		IGizmo temp2 = TriangleBumperJUnit.FACTORY.createGizmo(TriangleBumperJUnit.BUMPER_NAME,
				TriangleBumperJUnit.NAMES[0], TriangleBumperJUnit.ARGS[0][0], TriangleBumperJUnit.ARGS[0][1]);
		if(TriangleBumperJUnit.tr1.equals(temp)&temp.equals(temp2))
					assertEquals("Error: "+ TriangleBumperJUnit.tr1.getClass().toString()+" is not transitive.",
							 TriangleBumperJUnit.tr1,temp2);
		//Test consistency.
		assertTrue("Error: "+ TriangleBumperJUnit.tr1.getClass().toString()+" is not consistent.",
				 TriangleBumperJUnit.tr1.equals(temp)== TriangleBumperJUnit.tr1.equals(temp));
	}
	
	private void testExamples() throws Exception{
		// Check equality; for gizmos equality should be dependant only upon the name!
		IGizmo eq = TriangleBumperJUnit.FACTORY.createGizmo(TriangleBumperJUnit.BUMPER_NAME,
				TriangleBumperJUnit.NAMES[0], TriangleBumperJUnit.ARGS[0][0], TriangleBumperJUnit.ARGS[0][1]);
		assertEquals(eq, TriangleBumperJUnit.tr1);
		IGizmo eq2 = TriangleBumperJUnit.FACTORY.createGizmo(TriangleBumperJUnit.BUMPER_NAME,
				TriangleBumperJUnit.NAMES[0], TriangleBumperJUnit.ARGS[1][0], TriangleBumperJUnit.ARGS[1][1]);
		assertEquals(eq2, TriangleBumperJUnit.tr1);
		IGizmo neq = TriangleBumperJUnit.FACTORY.createGizmo(TriangleBumperJUnit.BUMPER_NAME,
				TriangleBumperJUnit.NAMES[1], TriangleBumperJUnit.ARGS[0][0], TriangleBumperJUnit.ARGS[0][1]);
		assertFalse(TriangleBumperJUnit.tr1.equals(neq));
	}
}
