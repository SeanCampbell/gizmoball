package testing;

import static org.junit.Assert.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import model.GizmoFactory;
import model.IGizmoFactory;
import model.gizmos.Absorber;
import model.gizmos.IGizmo;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import shapes.GeometryPolygon;
import shapes.IGeometry;
import shapes.LogicalPoint;

public class AbsorberJUnit {

	private static final int[][] ARGS = {{2,2,6,5}, {1,1,4,5}};
	private static final String[] NAMES = {"A1", "A2"};
	private static final IGizmoFactory FACTORY = GizmoFactory.getInstance();

	private static String NAME;
	private static double WIDTH;
	private static double HEIGHT;

	private static final double DELTA = 1e-15;

	private static IGizmo absorber;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		NAME = (String)PrivateAccessor.getPrivateField(Absorber.class, "NAME");

		absorber = FACTORY.createGizmo(NAME, NAMES[0], ARGS[0][0], ARGS[0][1], ARGS[0][2], ARGS[0][3]);
		WIDTH = absorber.getWidth();
		HEIGHT = absorber.getLength();
		
	}

	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Asserts that the width of the absorber is calculated correctly.
	 */
	@Test
	public void testWidth() {
		assertEquals(WIDTH, 4.0, DELTA);
	}

	/**
	 * Asserts that the height of the absorber is calculated correctly.
	 */
	@Test
	public void testHeight() {
		assertEquals(HEIGHT, 3.0, DELTA);
	}

	/**
	 * Asserts that the shapes are created correctly for an absorber -
	 * the Collection of shapes should just be one polygon shape.
	 */
	@Test
	public void testAbsorberShape() {
		Collection<IGeometry> generatedShapes = absorber.getGeometryList();
		IGeometry expected = this.createAbsorber(ARGS[0][0], ARGS[0][1]);
		assertEquals(1, generatedShapes.size());
		Iterator<IGeometry> iterator = generatedShapes.iterator();
		assertEquals(expected, iterator.next());
		assertFalse(iterator.hasNext());
	}

	private IGeometry createAbsorber(int tlx, int tly) {
		Collection<LogicalPoint> points = new ArrayList<>();
		points.add(new LogicalPoint(tlx, tly));
		points.add(new LogicalPoint(tlx + WIDTH, tly));
		points.add(new LogicalPoint(tlx + WIDTH, tly + HEIGHT));
		points.add(new LogicalPoint(tlx, tly + HEIGHT));
		return new GeometryPolygon(points);
	}

	@Test
	public void testMove() {
		Collection<IGeometry> generatedShapes = absorber.getGeometryList();
		absorber.move(12, 12);
		generatedShapes = absorber.getGeometryList();
		IGeometry expected = this.createAbsorber(12, 12);
		assertEquals(1, generatedShapes.size());
		Iterator<IGeometry> iterator = generatedShapes.iterator();
		assertEquals(expected, iterator.next());
		assertFalse(iterator.hasNext());
		
		absorber.move(ARGS[0][0], ARGS[0][1]);
		//TODO test fails here using absorber2 and gives this message. 
		//java.lang.AssertionError: expected: shapes.GeometryPolygon<4 5> 
		//but was: shapes.GeometryPolygon<4 5>
//		absorber2.move(4, 5);
//		Collection<IGeometry> actual = absorber2.getGeometryList();
//		IGeometry expected = this.createAbsorber(4,5);
//		assertEquals(1, actual.size());
//		Iterator<IGeometry> iterator = actual.iterator();
//		IGeometry itNext = iterator.next();
//		assertEquals(expected, itNext);
////		assertTrue("expected: " + expected + ", actual: " + itNext, expected.equals(itNext));
////		assertFalse(iterator.hasNext());
	}
	/**
	 * Asserts that an absorber will throw an UnsupportedOperationException
	 * upon rotation.
	 */
	@Test(expected = UnsupportedOperationException.class)
	public void testRotate() {
		absorber.rotate();
	}

	/**
	 * Asserts that the save string is as expected.
	 */
	@Test
	public void testSaveString() {
		String actual = absorber.getSaveString();
		StringBuilder stringBuild = new StringBuilder();
		LogicalPoint origin = new LogicalPoint(ARGS[0][0], ARGS[0][1]);
		LogicalPoint end = new LogicalPoint(ARGS[0][2], ARGS[0][3]);
		stringBuild.append(NAME + " " + NAMES[0] + " " + origin.toString() + " "
				+ end.toString() + "\n");
		assertEquals(actual, stringBuild.toString());

	}

	/**
	 * Test the equality of absorbers.
	 */
	@Test
	public void testEquals() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		//Test not null
		assertFalse(absorber.equals(null));

		//Test random object
		assertFalse(absorber.equals("Absorber String"));
		
		//Test Reflexive
		assertTrue(absorber.equals(absorber));

		//Test Symmetric
		IGizmo absorberTemp = FACTORY.createGizmo(NAME, NAMES[0], ARGS[0][0], ARGS[0][1], ARGS[0][2], ARGS[0][3]);
		assertTrue(absorber.equals(absorberTemp));

		//Test Transitive
		IGizmo absorberTemp2 = FACTORY.createGizmo(NAME, NAMES[0], ARGS[0][0], ARGS[0][1], ARGS[0][2], ARGS[0][3]);
		assertTrue(absorberTemp.equals(absorberTemp2));
		assertTrue(absorber.equals(absorberTemp2));

		//Test consistency
		assertTrue(absorber.equals(absorberTemp) == absorber.equals(absorberTemp));

		//Test name dependancy - ie. gizmos are equal according to their name.
		IGizmo a1 = FACTORY.createGizmo(NAME, NAMES[0], ARGS[0][0], ARGS[0][1], ARGS[0][2], ARGS[0][3]);
		IGizmo a2 = FACTORY.createGizmo(NAME, NAMES[0], ARGS[1][0], ARGS[1][1], ARGS[1][2], ARGS[1][3]);
		IGizmo a3 = FACTORY.createGizmo(NAME, NAMES[1], ARGS[1][0], ARGS[1][1], ARGS[1][2], ARGS[1][3]);

		assertTrue(a1.equals(a2));
		assertFalse(a1.equals(a3));
		assertFalse(a2.equals(a3));
	}


}
