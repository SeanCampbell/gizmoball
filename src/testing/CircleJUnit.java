package testing;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import shapes.GeometryCircle;
import shapes.ICircle;
import shapes.LogicalPoint;

public class CircleJUnit {
	private static final double[] ARGS = {3.5, 1.5};
	private static final double RADIUS = 0.5;
	private static ICircle circle;
	private static LogicalPoint point;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		point = new LogicalPoint(CircleJUnit.ARGS[0], CircleJUnit.ARGS[1]);
		circle = new GeometryCircle(point, CircleJUnit.RADIUS);
	}

	/**
	 * Asserts that an circle is not a polygon.  Ensures circle is non null object.
	 */
	@Test
	public void nullObjectTest(){
		assertNull(circle.getPolygon());
		assertNotNull(circle.getCircle());
	}
	
	/**
	 * Check the input parameters and get methods are consistent
	 */
	@Test
	public void parameterTest() {
		final double offBy = 0.00001;
		assertEquals(circle.getCentre(), point);
		assertEquals(circle.getRadius(),CircleJUnit.RADIUS, offBy);
	}
	
	@Test
	public void testEquals() throws Exception{
		testObjectProperties();
		testExamples();
	}
	
	private void testObjectProperties() throws Exception{
		//Test valid object is not null.
		assertFalse("Error: Object of " + CircleJUnit.circle.getClass().toString() + " equal to null.",
						CircleJUnit.circle.equals(null));
		//Test reflexive property.
		assertTrue("Error: "+ CircleJUnit.circle.getClass().toString()+" is not reflexive.",
				 CircleJUnit.circle.equals( CircleJUnit.circle));
		//Test symmetric.
		ICircle temp = new GeometryCircle(CircleJUnit.point, CircleJUnit.RADIUS);
		assertEquals("Error: "+ CircleJUnit.circle.getClass().toString()+" is not symmetric.", CircleJUnit.circle, temp);
		//Test transitive.
		ICircle temp2 = new GeometryCircle(CircleJUnit.point, CircleJUnit.RADIUS);
		if(CircleJUnit.circle.equals(temp)&temp.equals(temp2))
					assertEquals("Error: "+ CircleJUnit.circle.getClass().toString()+" is not transitive.",
							 CircleJUnit.circle,temp2);
		//Test consistency.
		assertTrue("Error: "+ CircleJUnit.circle.getClass().toString()+" is not consistent.",
				 CircleJUnit.circle.equals(temp)== CircleJUnit.circle.equals(temp));
	}
	
	private void testExamples() throws Exception{
		// Check equality
		GeometryCircle eq = new GeometryCircle(CircleJUnit.point, CircleJUnit.RADIUS);
		assertEquals(eq, CircleJUnit.circle);
		// Check inequality if different radii
		GeometryCircle neq = new GeometryCircle(CircleJUnit.point, CircleJUnit.RADIUS + 1.0);
		assertFalse(CircleJUnit.circle.equals(neq));
		// Check inequality if different centre
		LogicalPoint tempPoint = new LogicalPoint(CircleJUnit.ARGS[0] + 1.0, CircleJUnit.ARGS[1]);
		GeometryCircle neq2 = new GeometryCircle(tempPoint, CircleJUnit.RADIUS); 
		assertFalse(CircleJUnit.circle.equals(neq2));
	}	
}
